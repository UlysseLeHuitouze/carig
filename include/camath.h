//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAMATH_H
#define CARIG_CAMATH_H

#include <stdint.h>
#include <assert.h>
#include <limits.h>
#include <math.h>
#include "camacro.h"


// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to vector operations, which break down into eight parts:
//
// # Vectors of 2 32-bit integers
//
// A platform-specific int32x2 vector abstraction.
//
// Related to int32x2 vectors in this file are:
//
// - type ivec2_t
// - macro ivec2 :: int32_t -> int32_t -> ivec2_t
// - macro cavecdot :: ivec2_t -> ivec2_t -> int32_t
// - macro cavecsum :: ivec2_t -> int32_t
// - macro cavecmix :: ivec2_t -> ivec2_t -> ivec2_t -> ivec2_t
//
//
// # Vectors of 4 32-bit integers
//
// A platform-specific int32x4 vector abstraction.
//
// Related to int32x4 vectors in this file are:
//
// - type ivec4_t
// - macro ivec4 :: int32_t -> int32_t -> int32_t -> int32_t -> ivec4_t
// - macro ivec3 :: int32_t -> int32_t -> int32_t -> ivec4_t
// - macro cavecdot :: ivec4_t -> ivec4_t -> int32_t
// - macro cavecsum :: ivec4_t -> int32_t
// - macro caveccross :: ivec4_t -> ivec4_t -> ivec4_t
// - macro cavecmix :: ivec4_t -> ivec4_t -> ivec4_t -> ivec4_t
//
//
// # Vectors of 2 64-bit integers
//
// A platform-specific int64x2 vector abstraction.
//
// Related to int64x2 vectors in this file are:
//
// - type lvec2_t
// - macro lvec2 :: int64_t -> int64_t -> lvec2_t
// - macro cavecdot :: lvec2_t -> lvec2_t -> int64_t
// - macro cavecsum :: lvec2_t -> int64_t
// - macro cavecmix :: lvec2_t -> lvec2_t -> lvec2_t -> lvec2_t
//
//
// # Vectors of 4 64-bit integers
//
// A platform-specific int64x4 vector abstraction.
//
// Related to int64x4 vectors in this file are:
//
// - type lvec4_t
// - macro lvec4 :: int64_t -> int64_t -> int64_t -> int64_t -> lvec4_t
// - macro lvec3 :: int64_t -> int64_t -> int64_t -> lvec4_t
// - macro cavecdot :: lvec4_t -> lvec4_t -> int64_t
// - macro cavecsum :: lvec4_t -> int64_t
// - macro caveccross :: lvec4_t -> lvec4_t -> lvec4_t
// - macro cavecmix :: lvec4_t -> lvec4_t -> lvec4_t -> lvec4_t
//
//
// # Vectors of 2 32-bit floats
//
// A platform-specific float32x2 vector abstraction.
//
// Related to float32x2 vectors in this file are:
//
// - type vec2_t
// - macro vec2 :: float -> float -> vec2_t
// - macro cavecnormalize :: vec2_t -> vec2_t
// - macro cavecdot :: vec2_t -> vec2_t -> float
// - macro cavecsum :: vec2_t -> float
// - macro cavecmix :: vec2_t -> vec2_t -> vec2_t -> vec2_t
//
//
// # Vectors of 4 32-bit floats
//
// A platform-specific float32x4 vector abstraction.
//
// Related to float32x4 vectors in this file are:
//
// - type vec4_t
// - macro vec4 :: float -> float -> float -> float -> vec4_t
// - macro vec3 :: float -> float -> float -> vec4_t
// - macro cavecnormalize :: vec4_t -> vec4_t
// - macro cavecdot :: vec4_t -> vec4_t -> float
// - macro cavecsum :: vec4_t -> float
// - macro caveccross :: vec4_t -> vec4_t -> vec4_t
// - macro cavecmix :: vec4_t -> vec4_t -> vec4_t -> vec4_t
//
//
// # Vectors of 2 64-bit floats
//
// A platform-specific float64x2 vector abstraction.
//
// Related to float64x2 vectors in this file are:
//
// - type dvec2_t
// - macro dvec2 :: double -> double -> dvec2_t
// - macro cavecnormalize :: dvec2_t -> dvec2_t
// - macro cavecdot :: dvec2_t -> dvec2_t -> double
// - macro cavecsum :: dvec2_t -> double
// - macro cavecmix :: dvec2_t -> dvec2_t -> dvec2_t -> dvec2_t
//
//
// # Vectors of 4 64-bit floats
//
// A platform-specific float64x4 vector abstraction.
//
// Related to float64x4 vectors in this file are:
//
// - type dvec4_t
// - macro dvec4 :: double -> double -> double -> double -> dvec4_t
// - macro dvec3 :: double -> double -> double -> dvec4_t
// - macro cavecnormalize :: dvec4_t -> dvec4_t
// - macro cavecdot :: dvec4_t -> dvec4_t -> double
// - macro cavecsum :: dvec4_t -> double
// - macro caveccross :: dvec4_t -> dvec4_t -> dvec4_t
// - macro cavecmix :: dvec4_t -> dvec4_t -> dvec4_t -> dvec4_t
//
//
// # Matrices of 16 32-bit floats
//
// A platform-specific float32 4x4 matrix abstraction.
//
// Related to float32 4x4 matrices in this file are:
//
// - type mat4_t
// - macro mat4 :: float -> mat4_t
// - func caperspective_lh :: mat4_t -> double -> uint32_t -> uint32_t -> double -> double -> void
// - func caperspective_rh :: mat4_t -> double -> uint32_t -> uint32_t -> double -> double -> void
// - func caperspective_unsafe :: mat4_t -> double -> double -> void
// - func calook_at_lh :: mat4_t -> vec4_t -> vec4_t -> vec4_t -> void
// - func calook_at_lh_unsafe :: mat4_t -> vec4_t -> vec4_t -> vec4_t -> void
// - func calook_at_rh :: mat4_t -> vec4_t -> vec4_t -> vec4_t -> void
// - func calook_at_rh_unsafe :: mat4_t -> vec4_t -> vec4_t -> vec4_t -> void
//
//
// # Matrices of 16 64-bit floats
//
// A platform-specific float64 4x4 matrix abstraction.
//
// Related to float64 4x4 matrices in this file are:
//
// - type dmat4_t
// - macro dmat4 :: double -> dmat4_t
//
//
// ==================================================


#if !__clang__
#   undef __clang__
#endif
#if !__ARM_NEON
#   undef __ARM_NEON
#endif
#if !__SSE__
#   undef __SSE__
#endif
#if !__SSE2__
#   undef __SSE2__
#endif
#if !__SSE3__
#   undef __SSE3__
#endif
#if !__SSE4_1__
#   undef __SSE4_1__
#endif
#if !__AVX__
#   undef __AVX__
#endif
#if! __FMA__
#   undef __FMA__
#endif
#if !__AVX2__
#   undef __AVX2__
#endif
#if !__AVX512F__
#   undef __AVX512F__
#endif
#if !__AVX512VL__
#   undef __AVX512VL__
#endif


#if defined(CAMATH_ENABLE_INTRINSICS) || defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE)
#   ifdef __ARM_NEON
#       include <arm_neon.h>
#   else
#       include <immintrin.h>
#   endif
#endif


#ifdef CAMATH_ENABLE_INTRINSICS
static_assert(CHAR_BIT == 8, "Intrinsics-incompatible architecture detected");
static_assert(sizeof(int32_t) == 4, "Intrinsics-incompatible architecture detected");
static_assert(sizeof(int64_t) == 8, "Intrinsics-incompatible architecture detected");
static_assert(sizeof(float) == 4, "Intrinsics-incompatible architecture detected");
static_assert(sizeof(double) == 8, "Intrinsics-incompatible architecture detected");
#endif
#if __clang__
#   if !__has_builtin(__builtin_shufflevector)
#       error "__builtin_shufflevector required but is not supported"
#   endif
#else
#   if !__has_builtin(__builtin_shuffle)
#       error "__builtin_shuffle required but is not supported"
#   endif
#endif






/**
 * Computes the equivalent in radians of <code>degree</code>.
 * @param degree[in] [double] the angle in degree to convert in radians
 * @return [double] <code>degree * (3.14159265358979323846 / 180.0)</code>
 */
#define caradians(degree) (degree * 0.01745329251994329576922222\
222222222222222222222222222222222222222222222222\
222222222222222222222222222222222222222222222222\
222222222222222222222222222222222222222222222222\
2222222222222222222222222222222)











/**
 * Represents a vector of 2 32-bit integers.<br>
 * Its first and second components can be accessed respectively with
 * <code>myVec[0]</code> and <code>myVec[1]</code>.<br>
 * Since <code>__m64</code> is not supported on 64-bit architectures, this type's use is much more limited
 * than the rest in this file.
 */
typedef int32_t ivec2_t __attribute__((vector_size(2 * sizeof(int32_t))));





/**
 * Constructs a <code>ivec2_t</code> vector from two integers.
 * Essentially <code>(ivec2_t){x, y}</code>.
 * @param x[in] [int32_t] the first component of the vector to construct
 * @param y[in] [int32_t] the second component of the vector to construct
 * @return [ivec2_t] the resulting <code>ivec2_t</code> vector.
 */
#define ivec2(x, y) ((ivec2_t){x, y})











/**
 * Represents a vector of 4 32-bit integers.<br>
 * Its first, second, third and fourth components can be accessed respectively with
 * <code>myVec[0]</code>, <code>myVec[1]</code>, <code>myVec[2]</code> and <code>myVec[3]</code>.<br>
 * Can be casted safely to <code>__m128i</code>.
 */
typedef int32_t ivec4_t __attribute__((vector_size(4 * sizeof(int32_t))));





/**
 * Constructs a <code>ivec4_t</code> vector from four integers.
 * Essentially <code>(ivec4_t){x, y, z, w}</code>.
 * @param x[in] [int32_t] the first component of the vector to construct
 * @param y[in] [int32_t] the second component of the vector to construct
 * @param z[in] [int32_t] the third component of the vector to construct
 * @param w[in] [int32_t] the fourth component of the vector to construct
 * @return [ivec4_t] the resulting <code>ivec4_t</code> vector.
 */
#define ivec4(x, y, z, w) ((ivec4_t){x, y, z, w})





/**
 * Constructs a <code>ivec4_t</code> vector from three integers.
 * Essentially <code>(ivec4_t){x, y, z, 0}</code>.
 * @param x[in] [int32_t] the first component of the vector to construct
 * @param y[in] [int32_t] the second component of the vector to construct
 * @param z[in] [int32_t] the third component of the vector to construct
 * @return [ivec4_t] the resulting <code>ivec4_t</code> vector.
 */
#define ivec3(x, y, z) ((ivec4_t){x, y, z, 0})











/**
 * Represents a vector of 2 64-bit integers.<br>
 * Its first and second components can be accessed respectively with
 * <code>myVec[0]</code> and <code>myVec[1]</code>.<br>
 * Can be casted safely to <code>__m128i</code>.
 */
typedef int64_t lvec2_t __attribute__((vector_size(2 * sizeof(int64_t))));





/**
 * Constructs a <code>lvec2_t</code> vector from two integers.
 * Essentially <code>(lvec2_t){x, y}</code>.
 * @param x[in] [int64_t] the first component of the vector to construct
 * @param y[in] [int64_t] the second component of the vector to construct
 * @return [lvec2_t] the resulting <code>lvec2_t</code> vector.
 */
#define lvec2(x, y) ((lvec2_t){x, y})











/**
 * Represents a vector of 4 64-bit integers.<br>
 * Its first, second, third and fourth components can be accessed respectively with
 * <code>myVec[0]</code>, <code>myVec[1]</code>, <code>myVec[2]</code> and <code>myVec[3]</code>.<br>
 * Can be casted safely to <code>__m256i</code>.
 */
typedef int64_t lvec4_t __attribute__((vector_size(4 * sizeof(int64_t))));





/**
 * Constructs a <code>lvec4_t</code> vector from four integers.
 * Essentially <code>(lvec4_t){x, y, z, w}</code>.
 * @param x[in] [int64_t] the first component of the vector to construct
 * @param y[in] [int64_t] the second component of the vector to construct
 * @param z[in] [int64_t] the third component of the vector to construct
 * @param w[in] [int64_t] the fourth component of the vector to construct
 * @return [lvec4_t] the resulting <code>lvec4_t</code> vector.
 */
#define lvec4(x, y, z, w) ((lvec4_t){x, y, z, w})





/**
 * Constructs a <code>lvec4_t</code> vector from three integers.
 * Essentially <code>(lvec4_t){x, y, z, 0}</code>.
 * @param x[in] [int64_t] the first component of the vector to construct
 * @param y[in] [int64_t] the second component of the vector to construct
 * @param z[in] [int64_t] the third component of the vector to construct
 * @return [lvec4_t] the resulting <code>lvec4_t</code> vector.
 */
#define lvec3(x, y, z) ((lvec4_t){x, y, z, 0})











/**
 * Represents a vector of 2 32-bit floats.<br>
 * Its first and second components can be accessed respectively with
 * <code>myVec[0]</code> and <code>myVec[1]</code>.<br>
 * Since <code>__m64</code> is not supported on 64-bit architectures, this type's use is much more limited
 * than the rest in this file.
 */
typedef float vec2_t __attribute__((vector_size(2 * sizeof(float))));





/**
 * Constructs a <code>vec2_t</code> vector from two floats.
 * Essentially <code>(vec2_t){x, y}</code>.
 * @param x[in] [float] the first component of the vector to construct
 * @param y[in] [float] the second component of the vector to construct
 * @return [vec2_t] the resulting <code>vec2_t</code> vector.
 */
#define vec2(x, y) ((vec2_t){x, y})











/**
 * Represents a vector of 4 32-bit floats.<br>
 * Its first, second, third and fourth components can be accessed respectively with
 * <code>myVec[0]</code>, <code>myVec[1]</code>, <code>myVec[2]</code> and <code>myVec[3]</code>.<br>
 * Can be casted safely to <code>__m128</code>.
 */
typedef float vec4_t __attribute__((vector_size(4 * sizeof(float))));





/**
 * Constructs a <code>vec4_t</code> vector from four floats.
 * Essentially <code>(vec4_t){x, y, z, w}</code>.
 * @param x[in] [float] the first component of the vector to construct
 * @param y[in] [float] the second component of the vector to construct
 * @param z[in] [float] the third component of the vector to construct
 * @param w[in] [float] the fourth component of the vector to construct
 * @return [vec4_t] the resulting <code>vec4_t</code> vector.
 */
#define vec4(x, y, z, w) ((vec4_t){x, y, z, w})





/**
 * Constructs a <code>vec4_t</code> vector from three floats.
 * Essentially <code>(vec4_t){x, y, z, 0}</code>.
 * @param x[in] [float] the first component of the vector to construct
 * @param y[in] [float] the second component of the vector to construct
 * @param z[in] [float] the third component of the vector to construct
 * @return [vec4_t] the resulting <code>vec4_t</code> vector.
 */
#define vec3(x, y, z) ((vec4_t){x, y, z, 0})











/**
 * Represents a vector of 2 64-bit doubles.<br>
 * Its first and second components can be accessed respectively with
 * <code>myVec[0]</code> and <code>myVec[1]</code>.<br>
 * Can be casted safely to <code>__m128d</code>.
 */
typedef double dvec2_t __attribute__((vector_size(2 * sizeof(double))));





/**
 * Constructs a <code>dvec2_t</code> vector from two doubles.
 * Essentially <code>(dvec2_t){x, y}</code>.
 * @param x[in] [double] the first component of the vector to construct
 * @param y[in] [double] the second component of the vector to construct
 * @return [dvec2_t] the resulting <code>dvec2_t</code> vector.
 */
#define dvec2(x, y) ((dvec2_t){x, y})











/**
 * Represents a vector of 4 64-bit doubles.<br>
 * Its first, second, third and fourth components can be accessed respectively with
 * <code>myVec[0]</code>, <code>myVec[1]</code>, <code>myVec[2]</code> and <code>myVec[3]</code>.<br>
 * Can be casted safely to <code>__m256d</code>.
 */
typedef double dvec4_t __attribute__((vector_size(4 * sizeof(double))));





/**
 * Constructs a <code>dvec4_t</code> vector from four doubles.
 * Essentially <code>(dvec4_t){x, y, z, w}</code>.
 * @param x[in] [double] the first component of the vector to construct
 * @param y[in] [double] the second component of the vector to construct
 * @param z[in] [double] the third component of the vector to construct
 * @param w[in] [double] the fourth component of the vector to construct
 * @return [dvec4_t] the resulting <code>dvec4_t</code> vector.
 */
#define dvec4(x, y, z, w) ((dvec4_t){x, y, z, w})





/**
 * Constructs a <code>dvec4_t</code> vector from three doubles.
 * Essentially <code>(dvec4_t){x, y, z, 0}</code>.
 * @param x[in] [double] the first component of the vector to construct
 * @param y[in] [double] the second component of the vector to construct
 * @param z[in] [double] the third component of the vector to construct
 * @return [dvec4_t] the resulting <code>dvec4_t</code> vector.
 */
#define dvec3(x, y, z) ((dvec4_t){x, y, z, 0})











/**
 * <p>Represents a matrix of 16 32-bit floats.</p>
 * <p>Its first, second, third and fourth columns can be accessed respectively with
 * <code>myMatrix[0]</code>, <code>myMatrix[1]</code>, <code>myMatrix[2]</code> and <code>myMatrix[3]</code>.</p>
 * <p>Columns can be further indexed, such that <code>myMatrix[j][i]</code> corresponds to the element
 * at row #i and column #j of <code>myMatrix</code> (See #vec4_t).</p>
 */
typedef vec4_t mat4_t[4];





#define private_maT4(x, SEED) ({ \
    __auto_type CA_UNIQUE_NAME(x1, SEED) = (x); \
    (mat4_t){                    \
        vec4(                    \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED)            \
        ),                       \
        vec4(                    \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED)            \
        ),                       \
        vec4(                    \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED)            \
        ),                       \
        vec4(                    \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED),           \
            CA_UNIQUE_NAME(x1, SEED)            \
        )                        \
    };                           \
})
/**
 * Constructs a <code>mat4_t</code> matrix from one float.
 * Essentially
 * @code
 *  {
 *      {x, x, x, x},
 *      {x, x, x, x},
 *      {x, x, x, x},
 *      {x, x, x, x}
 *  }
 * @endcode
 * @param x[in] [float] the element to fill the matrix with
 * @return [mat4_t] the resulting <code>mat4_t</code> matrix
 */
#define mat4(x) private_maT4(x, __COUNTER__)











/**
 * <p>Represents a matrix of 16 64-bit doubles.</p>
 * <p>Its first, second, third and fourth columns can be accessed respectively with
 * <code>myMatrix[0]</code>, <code>myMatrix[1]</code>, <code>myMatrix[2]</code> and <code>myMatrix[3]</code>.</p>
 * <p>Columns can be further indexed, such that <code>myMatrix[j][i]</code> corresponds to the element
 * at row #i and column #j of <code>myMatrix</code> (See #dvec4_t).</p>
 */
typedef dvec4_t dmat4_t[4];





/**
 * Constructs a <code>dmat4_t</code> matrix from one double.
 * Essentially
 * @code
 *  {
 *      {x, x, x, x},
 *      {x, x, x, x},
 *      {x, x, x, x},
 *      {x, x, x, x}
 *  }
 * @endcode
 * @param x[in] [double] the element to fill the matrix with
 * @return [dmat4_t] the resulting <code>dmat4_t</code> matrix
 */
#define dmat4(x) mat4(x)












#define private_identitY(type, x) _Generic((x), type: (x), default: (type){})
/**
 * Converts a vector <code>u</code> to a vector of type <code>newtype</code>.<br>
 * Example usage:
 * @code
 *  vec4_t u = vec4(x, y, z, w);
 *  dvec4_t v = cavecconvert(u, dvec4_t);
 *  // equivalent to
 *  // v = {(double)u[0], (double)u[1], (double)u[2], (double)u[3]};
 * @endcode
 * @param u[in] [ivec2_t | vec2_t | dvec2_t | ivec4_t | vec4_t | dvec4_t] the vector to convert
 * @param newtype[in] [type] One of
 * @code
 *  ivec2_t
 *   vec2_t
 *  dvec2_t
 *  ivec4_t
 *   vec4_t
 *  dvec4_t
 * @endcode
 * @return [newtype] the resulting converted vector
 */
#define cavecconvert(u, newtype) __builtin_convertvector(u, newtype)




#if (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && \
defined(__SSE__) && defined(__SSE4_1__)

#define private_normalizE_f2(u, SEED) ({                     \
    vec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    __m128 CA_UNIQUE_NAME(u128, SEED) = (__m128)vec4(        \
        CA_UNIQUE_NAME(u1, SEED)[0],                         \
        CA_UNIQUE_NAME(u1, SEED)[1],                         \
        0.f,                                                 \
        0.f                                                  \
    );                                                       \
    __m128 CA_UNIQUE_NAME(n, SEED) = _mm_rsqrt_ps(_mm_dp_ps( \
        CA_UNIQUE_NAME(u128, SEED),                          \
        CA_UNIQUE_NAME(u128, SEED),                          \
        0xff                                                 \
    ));                                                      \
    vec4_t CA_UNIQUE_NAME(r, SEED) = (vec4_t)_mm_mul_ps(     \
        CA_UNIQUE_NAME(u128, SEED),                          \
        CA_UNIQUE_NAME(n, SEED)                              \
    );                                                       \
    vec2(                                                    \
        CA_UNIQUE_NAME(r, SEED)[0],                          \
        CA_UNIQUE_NAME(r, SEED)[1]                           \
    );                                                       \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && defined(__SSE__)

#define private_normalizE_f2(u, SEED) ({                     \
    vec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    __m128 CA_UNIQUE_NAME(u128, SEED) = (__m128)vec4(        \
        CA_UNIQUE_NAME(u1, SEED)[0],                         \
        CA_UNIQUE_NAME(u1, SEED)[1],                         \
        0.f,                                                 \
        0.f                                                  \
    );                                                       \
                                                             \
    __m128 CA_UNIQUE_NAME(p, SEED) = _mm_mul_ps(             \
        CA_UNIQUE_NAME(u128, SEED),                          \
        CA_UNIQUE_NAME(u128, SEED)                           \
    );                                                       \
                                                             \
    __m128 CA_UNIQUE_NAME(n, SEED) = _mm_rsqrt_ps(_mm_add_ps(\
        CA_UNIQUE_NAME(p, SEED),                             \
        _mm_shuffle_ps(                                      \
            CA_UNIQUE_NAME(p, SEED),                         \
            CA_UNIQUE_NAME(p, SEED),                         \
            225 /* 0b11100001 */                             \
        )                                                    \
    ));                                                      \
                                                             \
    vec4_t CA_UNIQUE_NAME(r, SEED) = (vec4_t)_mm_mul_ps(     \
        CA_UNIQUE_NAME(u128, SEED),                          \
        CA_UNIQUE_NAME(n, SEED)                              \
    );                                                       \
    vec2(                                                    \
        CA_UNIQUE_NAME(r, SEED)[0],                          \
        CA_UNIQUE_NAME(r, SEED)[1]                           \
    );                                                       \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && defined(__ARM_NEON)

#define private_normalizE_f2(u, SEED) ({                         \
    vec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                       \
    CA_UNIQUE_NAME(u1, SEED) * vrsqrtes_f32(vaddv_f32(vmulx_f32( \
        CA_UNIQUE_NAME(u1, SEED),                                \
        CA_UNIQUE_NAME(u1, SEED)                                 \
    )));                                                         \
})

#else

#define private_normalizE_f2(u, SEED) ({             \
    vec2_t CA_UNIQUE_NAME(u1, SEED) = (u);           \
    CA_UNIQUE_NAME(u1, SEED) / sqrtf(private_doT_f2( \
        CA_UNIQUE_NAME(u1, SEED),                    \
        CA_UNIQUE_NAME(u1, SEED),                    \
        __COUNTER__                                  \
    ));                                              \
})

#endif

#if (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && \
defined(__SSE4_1__) && defined(__SSE__)

#define private_normalizE_f4(u, SEED) ({                                   \
    __m128 CA_UNIQUE_NAME(u1, SEED) = (__m128)(u);                         \
    __m128 CA_UNIQUE_NAME(n, SEED) = _mm_rsqrt_ps(_mm_dp_ps(               \
        CA_UNIQUE_NAME(u1, SEED),                                          \
        CA_UNIQUE_NAME(u1, SEED),                                          \
        0xff                                                               \
    ));                                                                    \
    (vec4_t)_mm_mul_ps(CA_UNIQUE_NAME(u1, SEED), CA_UNIQUE_NAME(n, SEED)); \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && defined(__SSE__)

#define private_normalizE_f4(u, SEED) ({                      \
    __m128 CA_UNIQUE_NAME(u1, SEED) = (__m128)(u);            \
    __m128 CA_UNIQUE_NAME(p, SEED) = _mm_mul_ps(              \
            CA_UNIQUE_NAME(u1, SEED),                         \
            CA_UNIQUE_NAME(u1, SEED)                          \
    );                                                        \
                                                              \
    __m128 CA_UNIQUE_NAME(a, SEED) = _mm_shuffle_ps(          \
            CA_UNIQUE_NAME(p, SEED),                          \
            CA_UNIQUE_NAME(p, SEED),                          \
            177 /* 0b10110001 */                              \
    );                                                        \
    __m128 CA_UNIQUE_NAME(b, SEED) = _mm_add_ps(              \
            CA_UNIQUE_NAME(p, SEED),                          \
            CA_UNIQUE_NAME(a, SEED)                           \
    );                                                        \
    CA_UNIQUE_NAME(a, SEED) = _mm_shuffle_ps(                 \
            CA_UNIQUE_NAME(b, SEED),                          \
            CA_UNIQUE_NAME(b, SEED),                          \
            15 /* 0b00001111 */                               \
    );                                                        \
                                                              \
    __m128 CA_UNIQUE_NAME(n, SEED) = _mm_rsqrt_ps(_mm_add_ps( \
            CA_UNIQUE_NAME(b, SEED),                          \
            CA_UNIQUE_NAME(a, SEED)                           \
    ));                                                       \
    (vec4_t)_mm_mul_ps(                                       \
            CA_UNIQUE_NAME(u1, SEED),                         \
            CA_UNIQUE_NAME(n, SEED)                           \
    );                                                        \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && defined(__ARM_NEON)

#define private_normalizE_f4(u, SEED) ({                          \
    vec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                        \
    CA_UNIQUE_NAME(u1, SEED) * vrsqrtes_f32(vaddvq_f32(vmulq_f32( \
            CA_UNIQUE_NAME(u1, SEED),                             \
            CA_UNIQUE_NAME(u1, SEED)                              \
    )));                                                          \
})

#else

#define private_normalizE_f4(u, SEED) ({       \
    vec4_t CA_UNIQUE_NAME(u1, SEED) = (u);     \
    CA_UNIQUE_NAME(u1, SEED) / sqrtf(private_doT_f4( \
            CA_UNIQUE_NAME(u1, SEED),          \
            CA_UNIQUE_NAME(u1, SEED),          \
            __COUNTER__                        \
    ));                                        \
})

#endif


#if (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && \
defined(__SSE4_1__) && defined(__SSE2__) && defined(__AVX512F__) && defined(__AVX512VL__)

#define private_normalizE_d2(u, SEED) ({                        \
    __m128d CA_UNIQUE_NAME(u1, SEED) = (__m128d)(u);            \
    __m128d CA_UNIQUE_NAME(n, SEED) = _mm_rsqrt14_pd(_mm_dp_pd( \
            CA_UNIQUE_NAME(u1, SEED),                           \
            CA_UNIQUE_NAME(u1, SEED),                           \
            0xff                                                \
    ));                                                         \
    (dvec2_t)_mm_mul_pd(                                        \
            CA_UNIQUE_NAME(u1, SEED),                           \
            CA_UNIQUE_NAME(n, SEED)                             \
    );                                                          \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && defined(__ARM_NEON)

#define private_normalizE_d2(u, SEED) ({                          \
    dvec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                       \
    CA_UNIQUE_NAME(u1, SEED) * vrsqrted_f64(vaddvq_f64(vmulq_f64( \
            CA_UNIQUE_NAME(u1, SEED),                             \
            CA_UNIQUE_NAME(u1, SEED)                              \
    )));                                                          \
})
#else

#define private_normalizE_d2(u, SEED) ({      \
    dvec2_t CA_UNIQUE_NAME(u1, SEED) = (u);   \
    CA_UNIQUE_NAME(u1, SEED) / sqrt(private_doT_d2( \
            CA_UNIQUE_NAME(u1, SEED),         \
            CA_UNIQUE_NAME(u1, SEED),         \
            __COUNTER__\
    ));                                       \
})

#endif

#if (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && \
defined(__SSE4_1__) && defined(__SSE2__) && defined(__AVX__) && defined(__AVX512F__) && defined(__AVX512VL__)

#define private_normalizE_d4(u, SEED) ({                                                  \
    __m256d CA_UNIQUE_NAME(u1, SEED) = (__m256d)(u);                                      \
    __m128d CA_UNIQUE_NAME(l, SEED) = _mm256_castpd256_pd128(CA_UNIQUE_NAME(u1, SEED));   \
    __m128d CA_UNIQUE_NAME(h, SEED) = _mm256_extractf128_pd(CA_UNIQUE_NAME(u1, SEED), 1); \
    CA_UNIQUE_NAME(l, SEED) = _mm_mul_pd(                                                 \
            CA_UNIQUE_NAME(l, SEED),                                                      \
            CA_UNIQUE_NAME(l, SEED)                                                       \
    );                                                                                    \
    CA_UNIQUE_NAME(h, SEED) = _mm_mul_pd(                                                 \
            CA_UNIQUE_NAME(h, SEED),                                                      \
            CA_UNIQUE_NAME(h, SEED)                                                       \
    );                                                                                    \
    CA_UNIQUE_NAME(l, SEED) = _mm_add_pd(                                                 \
            CA_UNIQUE_NAME(l, SEED),                                                      \
            CA_UNIQUE_NAME(h, SEED)                                                       \
    );                                                                                    \
    CA_UNIQUE_NAME(l, SEED) = _mm_add_pd(CA_UNIQUE_NAME(l, SEED), _mm_shuffle_pd(         \
            CA_UNIQUE_NAME(l, SEED),                                                      \
            CA_UNIQUE_NAME(l, SEED),                                                      \
            1                                                                             \
    ));                                                                                   \
    CA_UNIQUE_NAME(l, SEED) = _mm_rsqrt14_pd(CA_UNIQUE_NAME(l, SEED));                    \
                                                                                          \
    (dvec4_t)_mm256_mul_pd(                                                               \
            CA_UNIQUE_NAME(u1, SEED),                                                     \
            _mm256_broadcastsd_pd(CA_UNIQUE_NAME(l, SEED))                                \
    );                                                                                    \
})

#elif (defined(CAMATH_ENABLE_INTRINSICS_VECNORMALIZE) || defined(CAMATH_ENABLE_INTRINSICS)) && \
defined(__ARM_NEON) && defined(__clang__)

#define private_normalizE_d4(u, SEED) ({                           \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                        \
    float64x2_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                              \
            CA_UNIQUE_NAME(u1, SEED),                              \
            0, 1                                                   \
    );                                                             \
    float64x2_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                              \
            CA_UNIQUE_NAME(u1, SEED),                              \
            2, 3                                                   \
    );                                                             \
    CA_UNIQUE_NAME(a, SEED) = vmulq_f64(                           \
            CA_UNIQUE_NAME(a, SEED),                               \
            CA_UNIQUE_NAME(a, SEED)                                \
    );                                                             \
    CA_UNIQUE_NAME(b, SEED) = vmulq_f64(                           \
            CA_UNIQUE_NAME(b, SEED),                               \
            CA_UNIQUE_NAME(b, SEED)                                \
    );                                                             \
                                                                   \
    CA_UNIQUE_NAME(u1, SEED) * vrsqrted_f64(                       \
            vaddvq_f64(CA_UNIQUE_NAME(a, SEED)) +                  \
            vaddvq_f64(CA_UNIQUE_NAME(b, SEED))                    \
    );                                                             \
})

#else

#define private_normalizE_d4(u, SEED) ({      \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);   \
    CA_UNIQUE_NAME(u1, SEED) / sqrt(private_doT_d4( \
            CA_UNIQUE_NAME(u1, SEED),         \
            CA_UNIQUE_NAME(u1, SEED),         \
            __COUNTER__                       \
    ));                                       \
})

#endif

/**
 * Computes the normalized version of <code>u</code>.
 * Essentially <code>u / sqrt(u . u)</code>.
 * @pre <code>u ≠ 0</code>
 * @post <code>cavecdot(result) ≈ 1</code>
 * @param u[in] [vec2_t | dvec2_t | vec4_t | dvec4_t] the vector to normalize
 * @return [<code>u</code>'s type] A copy of <code>u</code> after normalization
 */
#define cavecnormalize(u) (_Generic((u), \
     vec2_t: private_normalizE_f2(       \
        private_identitY(vec2_t, (u)),   \
        __COUNTER__                      \
     ),                                  \
     vec4_t: private_normalizE_f4(       \
        private_identitY(vec4_t, (u)),   \
        __COUNTER__                      \
     ),                                  \
    dvec2_t: private_normalizE_d2(       \
        private_identitY(dvec2_t, (u)),  \
        __COUNTER__                      \
    ),                                   \
    dvec4_t: private_normalizE_d4(       \
        private_identitY(dvec4_t, (u)),  \
        __COUNTER__                      \
    )                                    \
))




#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_doT_i2(u, v, SEED) vaddv_s32(vmul_s32((u), (v)))

#else

#   define private_doT_i2(u, v, SEED) private_suM_i2((u) * (v), __COUNTER__)

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_doT_i4(u, v, SEED) vaddvq_s32(vmulq_s32((u), (v)))

#else

#   define private_doT_i4(u, v, SEED) private_suM_i4((u) * (v), __COUNTER__)

#endif

#define private_doT_l2(u, v, SEED) private_suM_l2((u) * (v), __COUNTER__)
#define private_doT_l4(u, v, SEED) private_suM_l4((u) * (v), __COUNTER__)

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_doT_f2(u, v, SEED) vaddv_f32(vmul_f32((u), (v)))

#else

#   define private_doT_f2(u, v, SEED) private_suM_f2((u) * (v), __COUNTER__)

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__SSE4_1__) && defined(__SSE__)

#   define private_doT_f4(u, v, SEED) _mm_cvtss_f32(_mm_dp_ps((__m128)(u), (__m128)(v), 0xff))

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_doT_f4(u, v, SEED) vaddvq_f32(vmulq_f32((u), (v)))

#else

#   define private_doT_f4(u, v, SEED) private_suM_f4((u) * (v), __COUNTER__)

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__SSE4_1__) && defined(__SSE2__)

#   define private_doT_d2(u, v, SEED) _mm_cvtsd_f64(_mm_dp_pd((__m128d)(u), (__m128d)(v), 0xff))

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_doT_d2(u, v, SEED) vaddvq_f64(vmulq_f64((u), (v)))

#else

#   define private_doT_d2(u, v, SEED) private_suM_d2((u) * (v), __COUNTER__)

#endif

#define private_doT_d4(u, v, SEED) private_suM_d4((u) * (v), __COUNTER__)
/**
 * Computes the dot product of <code>u</code> with <code>v</code>.
 * Essentially <code>u . v</code>.
 * @param u[in] [ivec2_t | lvec2_t | vec2_t | dvec2_t | ivec4_t | lvec4_t | vec4_t | dvec4_t] the first vector
 * @param v[in] [<code>u</code>'s type] the second vector
 * @return [int32_t | int64_t | float | double] the dot product of <code>u</code> and <code>v</code>
 */
#define cavecdot(u, v) (_Generic( (( void (*)(typeof(u), typeof(v)) ) 0), \
    void (*)(ivec2_t, ivec2_t): private_doT_i2(                           \
        private_identitY(ivec2_t, (u)),                                   \
        private_identitY(ivec2_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)(ivec4_t, ivec4_t): private_doT_i4(                           \
        private_identitY(ivec4_t, (u)),                                   \
        private_identitY(ivec4_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)(lvec2_t, lvec2_t): private_doT_l2(                           \
        private_identitY(lvec2_t, (u)),                                   \
        private_identitY(lvec2_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)(lvec4_t, lvec4_t): private_doT_l4(                           \
        private_identitY(lvec4_t, (u)),                                   \
        private_identitY(lvec4_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)( vec2_t,  vec2_t): private_doT_f2(                           \
        private_identitY( vec2_t, (u)),                                   \
        private_identitY( vec2_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)( vec4_t,  vec4_t): private_doT_f4(                           \
        private_identitY( vec4_t, (u)),                                   \
        private_identitY( vec4_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)(dvec2_t, dvec2_t): private_doT_d2(                           \
        private_identitY(dvec2_t, (u)),                                   \
        private_identitY(dvec2_t, (v)), __COUNTER__                       \
    ),                                                                    \
    void (*)(dvec4_t, dvec4_t): private_doT_d4(                           \
        private_identitY(dvec4_t, (u)),                                   \
        private_identitY(dvec4_t, (v)), __COUNTER__                       \
    )                                                                     \
))




#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_i2(u, SEED) vaddv_s32((u))

#else

#   define private_suM_i2(u, SEED) ({       \
    ivec2_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1];            \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__SSE2__)

#   define private_suM_i4(u, SEED) ({                     \
    ivec4_t CA_UNIQUE_NAME(u1, SEED) = (u);               \
    __m128i CA_UNIQUE_NAME(v, SEED) = _mm_unpackhi_epi64( \
            (__m128i)CA_UNIQUE_NAME(u1, SEED),            \
            (__m128i)CA_UNIQUE_NAME(u1, SEED)             \
    );                                                    \
    CA_UNIQUE_NAME(v, SEED) = _mm_add_epi32(              \
            CA_UNIQUE_NAME(v, SEED),                      \
            (__m128i)CA_UNIQUE_NAME(u1, SEED)             \
    );                                                    \
    CA_UNIQUE_NAME(v, SEED)  = _mm_add_epi32(             \
            CA_UNIQUE_NAME(v, SEED),                      \
            _mm_shuffle_epi32(CA_UNIQUE_NAME(v, SEED), 1) \
    );                                                    \
    _mm_cvtsi128_si32(CA_UNIQUE_NAME(v, SEED));           \
})

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_i4(u, SEED) vaddvq_s32((u))

#else

#   define private_suM_i4(u, SEED) ({       \
    ivec4_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1] +           \
    CA_UNIQUE_NAME(u1, SEED)[2] +           \
    CA_UNIQUE_NAME(u1, SEED)[3];            \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_l2(u, SEED) vaddvq_s64((u))

#else

#   define private_suM_l2(u, SEED) ({       \
    lvec2_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1];            \
})

#endif

#if defined(__AVX2__)
#   define private_EXTRACT128i(u, im) _mm256_extracti128_si256((u), im)
#elif defined(__AVX__)
#   define private_EXTRACT128i(u, im) _mm256_extractf128_si256((u), im)
#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__SSE2__) && defined(__AVX__)

#   define private_suM_l4(u, SEED) ({                                                          \
    __m256i CA_UNIQUE_NAME(u1, SEED) = (__m256i)(u);                                           \
    __m128i CA_UNIQUE_NAME(l, SEED) = _mm256_castsi256_si128(CA_UNIQUE_NAME(u1, SEED));        \
    __m128i CA_UNIQUE_NAME(h, SEED) = private_EXTRACT128i(CA_UNIQUE_NAME(u1, SEED), 1);        \
    CA_UNIQUE_NAME(l, SEED) = _mm_add_epi64(CA_UNIQUE_NAME(l, SEED), CA_UNIQUE_NAME(h, SEED)); \
    (uint64_t)_mm_cvtsi128_si64(_mm_add_epi64(                                                 \
        CA_UNIQUE_NAME(l, SEED),                                                               \
        _mm_unpackhi_epi64(CA_UNIQUE_NAME(l, SEED), CA_UNIQUE_NAME(l, SEED))                   \
    ));                                                                                        \
})

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON) && defined(__clang__)

#   define private_suM_l4(u, SEED) ({                            \
    lvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                      \
    int64x2_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                            \
            CA_UNIQUE_NAME(u1, SEED),                            \
            0, 1                                                 \
    );                                                           \
    int64x2_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                            \
            CA_UNIQUE_NAME(u1, SEED),                            \
            2, 3                                                 \
    );                                                           \
                                                                 \
    vaddvq_s64(CA_UNIQUE_NAME(a, SEED)) +                        \
    vaddvq_s64(CA_UNIQUE_NAME(b, SEED));                         \
})

#else

#   define private_suM_l4(u, SEED) ({       \
    lvec4_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1] +           \
    CA_UNIQUE_NAME(u1, SEED)[2] +           \
    CA_UNIQUE_NAME(u1, SEED)[3];            \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_f2(u, SEED) vaddv_f32(u)

#else

#   define private_suM_f2(u, SEED) ({       \
     vec2_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1];            \
})

#endif

#   ifdef __SSE3__
#   define private_MOVEHDUPPS(u) _mm_movehdup_ps(u);
#   else
#   define private_MOVEHDUPPS(u) _mm_shuffle_ps(u, u, 245); // 0b11110101
#   endif
#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__SSE__)

#   define private_suM_f4(u, SEED) ({                                              \
    __m128 CA_UNIQUE_NAME(u1, SEED) = (__m128)(u);                                 \
    __m128 CA_UNIQUE_NAME(a, SEED) = private_MOVEHDUPPS(CA_UNIQUE_NAME(u1, SEED)); \
                                                                                   \
    __m128 CA_UNIQUE_NAME(b, SEED) = _mm_add_ps(                                   \
            CA_UNIQUE_NAME(u1, SEED),                                              \
            CA_UNIQUE_NAME(a, SEED)                                                \
    );                                                                             \
    CA_UNIQUE_NAME(a, SEED) = _mm_movehl_ps(                                       \
            CA_UNIQUE_NAME(a, SEED),                                               \
            CA_UNIQUE_NAME(b, SEED)                                                \
    );                                                                             \
                                                                                   \
    _mm_cvtss_f32(_mm_add_ss(                                                      \
            CA_UNIQUE_NAME(b, SEED),                                               \
            CA_UNIQUE_NAME(a, SEED))                                               \
    );                                                                             \
})

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_f4(u, SEED) vaddvq_f32((u))

#else

#   define private_suM_f4(u, SEED) ({       \
     vec4_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1] +           \
    CA_UNIQUE_NAME(u1, SEED)[2] +           \
    CA_UNIQUE_NAME(u1, SEED)[3];            \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON)

#   define private_suM_d2(u, SEED) vaddvq_f64((u))

#else

#   define private_suM_d2(u, SEED) ({       \
    dvec2_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1];            \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__AVX__) && defined(__SSE2__)

#   define private_suM_d4(u, SEED) ({                         \
    __m256d CA_UNIQUE_NAME(u1, SEED) = (__m256d)(u);          \
    __m128d CA_UNIQUE_NAME(l, SEED) = _mm256_castpd256_pd128( \
            CA_UNIQUE_NAME(u1, SEED)                          \
    );                                                        \
    __m128d CA_UNIQUE_NAME(h, SEED) = _mm256_extractf128_pd(  \
            CA_UNIQUE_NAME(u1, SEED),                         \
            1                                                 \
    );                                                        \
    CA_UNIQUE_NAME(l, SEED) = _mm_add_pd(                     \
            CA_UNIQUE_NAME(l, SEED),                          \
            CA_UNIQUE_NAME(h, SEED)                           \
    );                                                        \
                                                              \
    _mm_cvtsd_f64(_mm_add_sd(                                 \
            CA_UNIQUE_NAME(l, SEED),                          \
            _mm_unpackhi_pd(                                  \
                    CA_UNIQUE_NAME(l, SEED),                  \
                    CA_UNIQUE_NAME(l, SEED)                   \
            )                                                 \
    ));                                                       \
})

#elif defined(CAMATH_ENABLE_INTRINSICS) && defined(__ARM_NEON) && defined(__clang__)

#   define private_suM_d4(u, SEED) ({                              \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                        \
    float64x2_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                              \
            CA_UNIQUE_NAME(u1, SEED),                              \
            0, 1                                                   \
    );                                                             \
    float64x2_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                              \
            CA_UNIQUE_NAME(u1, SEED),                              \
            2, 3                                                   \
    );                                                             \
                                                                   \
    vaddvq_f64(CA_UNIQUE_NAME(a, SEED)) +                          \
    vaddvq_f64(CA_UNIQUE_NAME(b, SEED));                           \
})

#else

#   define private_suM_d4(u, SEED) ({       \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u); \
    CA_UNIQUE_NAME(u1, SEED)[0] +           \
    CA_UNIQUE_NAME(u1, SEED)[1] +           \
    CA_UNIQUE_NAME(u1, SEED)[2] +           \
    CA_UNIQUE_NAME(u1, SEED)[3];            \
})

#endif
/**
 * Computes the sum of all <code>u</code>'s components.
 * @param u[in] [ivec2_t | lvec2_t | vec2_t | dvec2_t | ivec4_t | lvec4_t | vec4_t | dvec4_t]
 * the vector to take components from and sum them
 * @return [int32_t | int64_t | float | double] the sum of all <code>u</code>'s components
 */
#define cavecsum(u) (_Generic((u),                                        \
    ivec2_t: private_suM_i2(private_identitY(ivec2_t, (u)), __COUNTER__), \
    ivec4_t: private_suM_i4(private_identitY(ivec4_t, (u)), __COUNTER__), \
    lvec2_t: private_suM_l2(private_identitY(lvec2_t, (u)), __COUNTER__), \
    lvec4_t: private_suM_l4(private_identitY(lvec4_t, (u)), __COUNTER__), \
     vec2_t: private_suM_f2(private_identitY( vec2_t, (u)), __COUNTER__), \
     vec4_t: private_suM_f4(private_identitY( vec4_t, (u)), __COUNTER__), \
    dvec2_t: private_suM_d2(private_identitY(dvec2_t, (u)), __COUNTER__), \
    dvec4_t: private_suM_d4(private_identitY(dvec4_t, (u)), __COUNTER__)  \
))


#ifdef __clang__

#   define private_crosS_i(u, v, SEED) ({                      \
    ivec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                    \
    ivec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                    \
    ivec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                          \
            CA_UNIQUE_NAME(u1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    ivec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(v1, SEED),                          \
            CA_UNIQUE_NAME(v1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    ivec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shufflevector(                                   \
            CA_UNIQUE_NAME(r, SEED),                           \
            CA_UNIQUE_NAME(r, SEED),                           \
            1, 2, 0, 3                                         \
    );                                                         \
})

#   define private_crosS_l(u, v, SEED) ({                      \
    lvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                    \
    lvec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                    \
    lvec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                          \
            CA_UNIQUE_NAME(u1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    lvec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(v1, SEED),                          \
            CA_UNIQUE_NAME(v1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    lvec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shufflevector(                                   \
            CA_UNIQUE_NAME(r, SEED),                           \
            CA_UNIQUE_NAME(r, SEED),                           \
            1, 2, 0, 3                                         \
    );                                                         \
})

#   define private_crosS_f(u, v, SEED) ({                      \
    vec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                     \
    vec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                     \
    vec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector(  \
            CA_UNIQUE_NAME(u1, SEED),                          \
            CA_UNIQUE_NAME(u1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    vec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector(  \
            CA_UNIQUE_NAME(v1, SEED),                          \
            CA_UNIQUE_NAME(v1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    vec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED); \
    __builtin_shufflevector(                                   \
            CA_UNIQUE_NAME(r, SEED),                           \
            CA_UNIQUE_NAME(r, SEED),                           \
            1, 2, 0, 3                                         \
    );                                                         \
})

#   define private_crosS_d(u, v, SEED) ({                      \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                    \
    dvec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                    \
    dvec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(u1, SEED),                          \
            CA_UNIQUE_NAME(u1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    dvec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shufflevector( \
            CA_UNIQUE_NAME(v1, SEED),                          \
            CA_UNIQUE_NAME(v1, SEED),                          \
            1, 2, 0, 3                                         \
    );                                                         \
    dvec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shufflevector(                                   \
            CA_UNIQUE_NAME(r, SEED),                           \
            CA_UNIQUE_NAME(r, SEED),                           \
            1, 2, 0, 3                                         \
    );                                                         \
})

#else

#   define private_crosS_i(u, v, SEED) ({                   \
    ivec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                 \
    ivec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                 \
    ivec4_t CA_UNIQUE_NAME(mask, SEED) = ivec4(1, 2, 0, 3); \
    ivec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(u1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    ivec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(v1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    ivec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shuffle(                                      \
            CA_UNIQUE_NAME(r, SEED),                        \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
})

#   define private_crosS_l(u, v, SEED) ({                   \
    lvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                 \
    lvec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                 \
    lvec4_t CA_UNIQUE_NAME(mask, SEED) = lvec4(1, 2, 0, 3); \
    lvec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(u1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    lvec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(v1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    lvec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shuffle(                                      \
            CA_UNIQUE_NAME(r, SEED),                        \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
})

#   define private_crosS_f(u, v, SEED) ({                   \
    vec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                  \
    vec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                  \
    ivec4_t CA_UNIQUE_NAME(mask, SEED) = ivec4(1, 2, 0, 3); \
    vec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shuffle(     \
            CA_UNIQUE_NAME(u1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    vec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shuffle(     \
            CA_UNIQUE_NAME(v1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    vec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED); \
    __builtin_shuffle(                                      \
            CA_UNIQUE_NAME(r, SEED),                        \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
})

#   define private_crosS_d(u, v, SEED) ({                   \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                 \
    dvec4_t CA_UNIQUE_NAME(v1, SEED) = (v);                 \
    lvec4_t CA_UNIQUE_NAME(mask, SEED) = lvec4(1, 2, 0, 3); \
    dvec4_t CA_UNIQUE_NAME(a, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(u1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    dvec4_t CA_UNIQUE_NAME(b, SEED) = __builtin_shuffle(    \
            CA_UNIQUE_NAME(v1, SEED),                       \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
    dvec4_t CA_UNIQUE_NAME(r, SEED) = CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(b, SEED) - \
                                      CA_UNIQUE_NAME(v1, SEED) * CA_UNIQUE_NAME(a, SEED);  \
    __builtin_shuffle(                                      \
            CA_UNIQUE_NAME(r, SEED),                        \
            CA_UNIQUE_NAME(mask, SEED)                      \
    );                                                      \
})

#endif
/**
 * Computes the cross product of <code>u</code> with <code>v</code>.
 * Essentially <code>u ^ v</code.
 * @param u[in] [ivec4_t | lvec4_t | vec4_t | dvec4_t] the first vector
 * @param v[in] [<code>u</code>'s type] the second vector
 * @return [<code>u</code>'s type] the cross product of <code>u</code> with <code>v</code>
 */
#define caveccross(u, v) (_Generic( (( void (*)(typeof(u), typeof(v)) ) 0), \
    void (*)(ivec4_t, ivec4_t): private_crosS_i(                            \
        private_identitY(ivec4_t, (u)),                                     \
        private_identitY(ivec4_t, (v)), __COUNTER__                         \
    ),                                                                      \
    void (*)(lvec4_t, lvec4_t): private_crosS_l(                            \
        private_identitY(lvec4_t, (u)),                                     \
        private_identitY(lvec4_t, (v)), __COUNTER__                         \
    ),                                                                      \
    void (*)( vec4_t,  vec4_t): private_crosS_f(                            \
        private_identitY( vec4_t, (u)),                                     \
        private_identitY( vec4_t, (v)), __COUNTER__                         \
    ),                                                                      \
    void (*)(dvec4_t, dvec4_t): private_crosS_d(                            \
        private_identitY(dvec4_t, (u)),                                     \
        private_identitY(dvec4_t, (v)), __COUNTER__                         \
    )                                                                       \
))




// u (1 - x) + vx = vx - (ux - u)
#define private_miX_i2(u, v, x, SEED) ({                      \
    ivec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    ivec2_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})
#define private_miX_i4(u, v, x, SEED) ({                      \
    ivec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    ivec4_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})
#define private_miX_l2(u, v, x, SEED) ({                      \
    lvec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    lvec2_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})
#define private_miX_l4(u, v, x, SEED) ({                      \
    lvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    lvec4_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})
#define private_miX_f2(u, v, x, SEED) ({                      \
    vec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                    \
    vec2_t CA_UNIQUE_NAME(x1, SEED) = (x);                    \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__FMA__)

#define private_miX_f4(u, v, x, SEED) ({                      \
    __m128 CA_UNIQUE_NAME(u1, SEED) = (__m128)(u);            \
    __m128 CA_UNIQUE_NAME(x1, SEED) = (__m128)(x);            \
    (vec4_t)_mm_fmsub_ps(                                     \
        (__m128)(v),                                          \
        CA_UNIQUE_NAME(x1, SEED),                             \
        _mm_fmsub_ps(                                         \
            CA_UNIQUE_NAME(u1, SEED),                         \
            CA_UNIQUE_NAME(x1, SEED),                         \
            CA_UNIQUE_NAME(u1, SEED)                          \
        )                                                     \
    );                                                        \
})

#else

#define private_miX_f4(u, v, x, SEED) ({                      \
    vec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                    \
    vec4_t CA_UNIQUE_NAME(x1, SEED) = (x);                    \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__FMA__)

#define private_miX_d2(u, v, x, SEED) ({                      \
    __m128d CA_UNIQUE_NAME(u1, SEED) = (__m128d)(u);          \
    __m128d CA_UNIQUE_NAME(x1, SEED) = (__m128d)(x);          \
    (dvec2_t)_mm_fmsub_pd(                                    \
        (__m128d)(v),                                         \
        CA_UNIQUE_NAME(x1, SEED),                             \
        _mm_fmsub_pd(                                         \
            CA_UNIQUE_NAME(u1, SEED),                         \
            CA_UNIQUE_NAME(x1, SEED),                         \
            CA_UNIQUE_NAME(u1, SEED)                          \
        )                                                     \
    );                                                        \
})

#else

#define private_miX_d2(u, v, x, SEED) ({                      \
    dvec2_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    dvec2_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})

#endif

#if defined(CAMATH_ENABLE_INTRINSICS) && defined(__FMA__)

#define private_miX_d4(u, v, x, SEED) ({                      \
    __m256d CA_UNIQUE_NAME(u1, SEED) = (__m256d)(u);          \
    __m256d CA_UNIQUE_NAME(x1, SEED) = (__m256d)(x);          \
    (dvec4_t)_mm256_fmsub_pd(                                 \
        (__m256d)(v),                                         \
        CA_UNIQUE_NAME(x1, SEED),                             \
        _mm256_fmsub_pd(                                      \
            CA_UNIQUE_NAME(u1, SEED),                         \
            CA_UNIQUE_NAME(x1, SEED),                         \
            CA_UNIQUE_NAME(u1, SEED)                          \
        )                                                     \
    );                                                        \
})

#else

#define private_miX_d4(u, v, x, SEED) ({                      \
    dvec4_t CA_UNIQUE_NAME(u1, SEED) = (u);                   \
    dvec4_t CA_UNIQUE_NAME(x1, SEED) = (x);                   \
    (v) * CA_UNIQUE_NAME(x1, SEED) - (                        \
        CA_UNIQUE_NAME(u1, SEED) * CA_UNIQUE_NAME(x1, SEED) - \
        CA_UNIQUE_NAME(u1, SEED)                              \
    );                                                        \
})

#endif

/**
 * Interpolates <code>x</code> in regards to <code>u</code> and <code>v</code>.
 * @param u[in] [ivec2_t | lvec2_t | vec2_t | dvec2_t | ivec4_t | lvec4_t | vec4_t | dvec4_t]
 * the start of the interpolation range
 * @param v[in] [<code>u</code>'s type] th end of the interpolation range
 * @param x[in] [<code>u</code>'s type] the vector to interpolate
 * @return [<code>u</code>'s type]
 * @code u × (1 - x) + v × x @endcode where × is the multiplication of vectors component-wise.
 */
#define cavecmix(u, v, x) (_Generic( (( void (*)(typeof(u), typeof(v), typeof(x)) ) 0), \
    void (*)(ivec2_t, ivec2_t, ivec2_t): private_miX_i2(                                \
        private_identitY(ivec2_t, (u)),                                                 \
        private_identitY(ivec2_t, (v)),                                                 \
        private_identitY(ivec2_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)(ivec4_t, ivec4_t, ivec4_t): private_miX_i4(                                \
        private_identitY(ivec4_t, (u)),                                                 \
        private_identitY(ivec4_t, (v)),                                                 \
        private_identitY(ivec4_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)(lvec2_t, lvec2_t, lvec2_t): private_miX_l2(                                \
        private_identitY(lvec2_t, (u)),                                                 \
        private_identitY(lvec2_t, (v)),                                                 \
        private_identitY(lvec2_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)(lvec4_t, lvec4_t, lvec4_t): private_miX_l4(                                \
        private_identitY(lvec4_t, (u)),                                                 \
        private_identitY(lvec4_t, (v)),                                                 \
        private_identitY(lvec4_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)( vec2_t,  vec2_t,  vec2_t): private_miX_f2(                                \
        private_identitY( vec2_t, (u)),                                                 \
        private_identitY( vec2_t, (v)),                                                 \
        private_identitY( vec2_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)( vec4_t,  vec4_t,  vec4_t): private_miX_f4(                                \
        private_identitY( vec4_t, (u)),                                                 \
        private_identitY( vec4_t, (v)),                                                 \
        private_identitY( vec4_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)(dvec2_t, dvec2_t, dvec2_t): private_miX_d2(                                \
        private_identitY(dvec2_t, (u)),                                                 \
        private_identitY(dvec2_t, (v)),                                                 \
        private_identitY(dvec2_t, (x)), __COUNTER__                                     \
    ),                                                                                  \
    void (*)(dvec4_t, dvec4_t, dvec4_t): private_miX_d4(                                \
        private_identitY(dvec4_t, (u)),                                                 \
        private_identitY(dvec4_t, (v)),                                                 \
        private_identitY(dvec4_t, (x)), __COUNTER__                                     \
    )                                                                                   \
))


/**
 * Completely overwrites a matrix to turn it into a perspective projection matrix for
 * left-handed coordinate systems.
 * @param dst[in,out] the destination matrix
 * @param fov[in] the field of vision in degrees
 * @param width[in] window's width, in pixels
 * @param height[in] window's height, in pixels
 * @param z_near[in] z_near
 * @param z_far[in] z_far
 */
void caperspective_lh(mat4_t dst, double fov, uint32_t width, uint32_t height, double z_near, double z_far);



/**
 * Completely overwrites a matrix to turn it into a perspective projection matrix for
 * right-handed coordinate systems.
 * @param dst[in,out] the destination matrix
 * @param fov[in] the field of vision in degrees
 * @param width[in] window's width, in pixels
 * @param height[in] window's height, in pixels
 * @param z_near[in] z_near
 * @param z_far[in] z_far
 */
void caperspective_rh(mat4_t dst, double fov, uint32_t width, uint32_t height, double z_near, double z_far);


/**
 * Unsafe version of #caperspective variants that only writes necessary slots
 * when either fov or window dimension changes.
 * Suited for both left-handed and right-handed coordinate systems.
 * @param dst[in,out] the destination
 * @param reverse_tan_half_fov 1 / tan(0.5 * fov)
 * @param reverse_aspect_ratio window height / width ratio
 */
void caperspective_unsafe(mat4_t dst, double reverse_tan_half_fov, double reverse_aspect_ratio);


/**
 * Fills a matrix with the correct look_at build for left-handed coordinate system.
 * @param dst[in,out] the destination matrix
 * @param cam_pos[in] [vec3(...)] the eye/camera position
 * @param cam_look[in] [vec3(...)] what the camera is looking at,
 * such that the looking direction is <code>cam_look - cam_pos</code>
 * @param up_dir[in] [vec3(...)] the "up" axis, usually (0, 1, 0)
 */
void calook_at_lh(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir);


/**
 * Unsafe version of #calook_at_lh that only updates necessary components. Left-handed coordinate systems only.
 * @param dst[in,out] [vec3(...)] the destination matrix
 * @param cam_pos[in] [vec3(...)] the eye/camera position
 * @param cam_look[in] [vec3(...)] what the camera is looking at,
 * such that the looking direction is <code>cam_look - cam_pos</code>
 * @param up_dir[in] [vec3(...)] the "up" axis, usually (0, 1, 0)
 */
void calook_at_lh_unsafe(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir);


/**
 * Fills a matrix with the correct look_at build for right-handed coordinate system.
 * @param dst[in,out] the destination matrix
 * @param cam_pos[in] [vec3(...)] the eye position/the camera position
 * @param cam_look[in] [vec3(...)] what the camera is looking at,
 * such that the looking direction is <code>cam_look - cam_pos</code>
 * @param up_dir[in] [vec3(...)] the "up" axis, usually (0, 1, 0)
 */
void calook_at_rh(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir);


/**
 * Unsafe version of #calook_at_rh that only updates necessary components. Right-handed coordinate systems only.
 * @param dst[in,out] the destination matrix
 * @param cam_pos[in] [vec3(...)] the eye position/the camera position
 * @param cam_look[in] [vec3(...)] what the camera is looking at,
 * such that the looking direction is <code>cam_look - cam_pos</code>
 * @param up_dir[in] [vec3(...)] the "up" axis, usually (0, 1, 0)
 */
void calook_at_rh_unsafe(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir);




#endif //CARIG_CAMATH_H
