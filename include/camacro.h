//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAMACRO_H
#define CARIG_CAMACRO_H


#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif


#define private_CONCAT2_bis2(A, B) A ## B
#define private_CONCAT2_bis(A, B) private_CONCAT2_bis2(A, B)
/**
 * Concatenates two raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @return A . B where '.' is the string concatenation
 */
#define CA_CONCAT2(A, B) private_CONCAT2_bis(A, B)


/**
 * Concatenates three raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @param C the third text to be concatenated
 * @return A . B . C where '.' is the string concatenation
 */
#define CA_CONCAT3(A, B, C) private_CONCAT2_bis( private_CONCAT2_bis(A, B), C )


/**
 * Concatenates four raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @param C the third text to be concatenated
 * @param D the fourth text to be concatenated
 * @return A . B . C . D where '.' is the string concatenation
 */
#define CA_CONCAT4(A, B, C, D) private_CONCAT2_bis(CA_CONCAT3(A, B, C), D)


/**
 * Creates a unique name based on a substring and a unique number.<br>
 * Typical usage:
 * @code
 *  #define __MY_MACRO_IMPL(COUNT, UNIQUE_SEED) ({
 *      int CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED) = some_formula(COUNT);
 *      (MyDataStructure){
 *          .capacity = CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED),
 *          .arr = malloc(sizeof(whatever) * CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED));
 *      };
 *  })
 *
 *  #define MY_MACRO(COUNT) __MY_MACRO_IMPL(COUNT, __COUNTER__)
 * @endcode
 *
 * @param NAME the base name to use
 * @param ID the ID used for making a unique name
 * @return _ . NAME . ID where '.' is the string concatenation
 */
#define CA_UNIQUE_NAME(NAME, ID) CA_CONCAT3(_, NAME, ID)


#define private_STRINGIFY(A) #A
/**
 * Stringifies a macro expression.
 * @param A the expression to stringify
 */
#define STRINGIFY(A) private_STRINGIFY(A)


#define private_smalloc(size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = malloc(size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {               \
        CA_ERROR("Failed malloc");                  \
        exit(1);                                    \
    }                                               \
    CA_UNIQUE_NAME(mem, SEED);                      \
})
/**
 * Safe version of malloc which crashes upon failure.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] always a valid pointer to the allocated memory
 */
#define smalloc(size) private_smalloc(size, __COUNTER__)


#define private_scalloc(el_count, el_size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = calloc(el_count, el_size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {                            \
        CA_ERROR("Failed calloc");                               \
        exit(1);                                                 \
    }                                                            \
    CA_UNIQUE_NAME(mem, SEED);                                   \
})
/**
 * Safe version of calloc which crashes upon failure.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] always a valid pointer to the allocated memory
 */
#define scalloc(el_count, el_size) private_scalloc(el_count, el_size, __COUNTER__)


#define private_srealloc(old_mem, new_size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = realloc(old_mem, new_size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {                             \
        CA_ERROR("Failed realloc");                               \
        free(old_mem);                                            \
        exit(1);                                                  \
    }                                                             \
    CA_UNIQUE_NAME(mem, SEED);                                    \
})
/**
 * Safe version of realloc which automatically <code>old_mem</code> then crashes upon failure.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] always a valid pointer to the reallocated memory
 */
#define srealloc(old_mem, new_size) private_srealloc(old_mem, new_size, __COUNTER__)


#if DEBUG
/**
 * Gets expanded into smalloc (safe malloc which crashes upon failure) if DEBUG, into malloc otherwise.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define nmalloc(size) smalloc(size)


/**
 * Gets expanded into scalloc (safe calloc which crashes upon failure) if DEBUG, into calloc otherwise.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define ncalloc(el_count, el_size) scalloc(el_count, el_size)


/**
 * Gets expanded into srealloc (safe realloc which crashes upon failure) if DEBUG, into realloc otherwise.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] If DEBUG, always a pointer to the reallocated memory, otherwise, NULL upon failure.
 */
#define nrealloc(old_mem, new_size) srealloc(old_mem, new_size)

#else
/**
 * Gets expanded into smalloc (safe malloc which crashes upon failure) if DEBUG, into malloc otherwise.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define nmalloc(size) malloc(size)


/**
 * Gets expanded into scalloc (safe calloc which crashes upon failure) if DEBUG, into calloc otherwise.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define ncalloc(el_count, el_size) calloc(el_count, el_size)


/**
 * Gets expanded into srealloc (safe realloc which crashes upon failure) if DEBUG, into realloc otherwise.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] If DEBUG, always a pointer to the reallocated memory, otherwise, NULL upon failure.
 */
#define nrealloc(old_mem, new_size) realloc(old_mem, new_size)
#endif //DEBUG



#define private_MAX(A, B, SEED) ({                                                             \
    __auto_type CA_UNIQUE_NAME(a, SEED) = (A);                                                    \
    __auto_type CA_UNIQUE_NAME(b, SEED) = (B);                                                    \
    CA_UNIQUE_NAME(a, SEED) > CA_UNIQUE_NAME(b, SEED) ? CA_UNIQUE_NAME(a, SEED) : CA_UNIQUE_NAME(b, SEED); \
})
#define private_MIN(A, B, SEED) ({                                                             \
    __auto_type CA_UNIQUE_NAME(a, SEED) = (A);                                                    \
    __auto_type CA_UNIQUE_NAME(b, SEED) = (B);                                                    \
    CA_UNIQUE_NAME(a, SEED) < CA_UNIQUE_NAME(b, SEED) ? CA_UNIQUE_NAME(a, SEED) : CA_UNIQUE_NAME(b, SEED); \
})
#define MAX(A, B) private_MAX(A, B, __COUNTER__)
#define MIN(A, B) private_MIN(A, B, __COUNTER__)
#define CLAMP(A, B, X) ( MAX(A, MIN(B, X)) )

#if __has_builtin(__builtin_expect)
#define likely(c) __builtin_expect(((bool)(c)), 1)
#define unlikely(c) __builtin_expect(((bool)(c)), 0)
#else
#define likely(c) (c)
#define unlikely(c) (c)
#endif


#endif //CARIG_CAMACRO_H
