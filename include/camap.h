//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAMAP_H
#define CARIG_CAMAP_H

#include <stdint.h>
#include <stddef.h>
#include "camacro.h"




// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to maps, which break down into five parts:
//
//
// # Hash Set
//
// A set abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case.
// Only a fixed-sized variant is implemented as of now.
//
// Related to hash sets in this file are:
//
// - struct BoundHashSet
// - macro ca_bound_hashset_static_of :: integral -> int (void*,void*) -> BoundHashSet
// - func ca_bound_hashset_dynamic_of :: size_t -> int (void*,void*) -> BoundHashSet
// - func ca_bound_hashset_free :: BoundHashSet* -> void
// - func cahsetput :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsethas :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsetrmv :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsetrst :: BoundHashSet* -> void
// - func cahsetarr :: BoundHashSet* -> void** -> void
//
//
// # Int Set
//
// A set abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case,
// specifically tweaked for 32-bit integers.
// Only a fix-sized variant is implemented as of now.
//
// Related to int sets in this file are:
//
// - struct BoundIntSet
// - macro ca_bound_intset_static_of :: integral -> BoundIntSet
// - func ca_bound_intset_dynamic_of :: size_t -> BoundIntSet
// - func ca_bound_intset_free :: BoundIntSet* -> void
// - func caisetput :: BoundIntSet* -> uint32_t -> bool
// - func caisethas :: BoundIntSet* -> uint32_t -> bool
// - func caisetrmv :: BoundIntSet* -> uint32_t -> bool
// - func caisetrst :: BoundIntSet* -> void
// - func caisetarr :: BoundIntSet* -> uint32_t* -> void
//
//
// # Hash Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys.
// Only a fix-sized variant is implemented as of now.
//
// Related to hash maps in this file are:
//
// - struct BoundHashMap
// - macro ca_bound_hashmap_static_of :: integral -> int (void*,void*) -> BoundHashMap
// - func ca_bound_hashmap_dynamic_of :: size_t -> int (void*,void*) -> BoundHashMap
// - func ca_bound_hashmap_free :: BoundHashMap* -> void
// - func cahmapput :: BoundHashMap* -> uint32_t -> void* -> void* -> void*
// - func cahmapget :: BoundHashMap* -> uint32_t -> void* -> void*
// - func cahmaprmv :: BoundHashMap* -> uint32_t -> void* -> void*
// - func cahmaprst :: BoundHashMap* -> void
// - func cahmaparr :: BoundHashMap* -> void** -> void** -> void
//
//
// # Int Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys, specifically tweaked for 32-bit integer keys.
// Only a fix-sized variant is implemented as of now.
//
// Related to int maps in this file are:
//
// - struct BoundIntMap
// - macro ca_bound_intmap_static_of :: integral -> BoundIntMap
// - func ca_bound_intmap_dynamic_of :: size_t -> BoundIntMap
// - func ca_bound_intmap_free :: BoundIntMap* -> void
// - func caimapput :: BoundIntMap* -> uint32_t -> void* -> void*
// - func caimapget :: BoundIntMap* -> uint32_t -> void*
// - func caimaprmv :: BoundIntMap* -> uint32_t -> void*
// - func caimaprst :: BoundIntMap* -> void
// - func caimaparr :: BoundIntMap* -> uint32_t* -> void** -> void
//
//
// # Int-to-Int Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys, specifically tweaked for 32-bit integer keys and values.
// Only a fix-sized variant is implemented as of now.
//
// Related to int-to-int maps in this file are:
//
// - struct BoundIntIntMap
// - macro ca_bound_intintmap_static_of :: integral -> BoundIntIntMap
// - func ca_bound_intintmap_dynamic_of :: size_t -> BoundIntIntMap
// - func ca_bound_intintmap_free :: BoundIntIntMap* -> void
// - func caiimapput :: BoundIntIntMap* -> uint32_t -> uint32_t -> uint32_t
// - func caiimapget :: BoundIntIntMap* -> uint32_t -> uint32_t
// - func caiimaprmv :: BoundIntIntMap* -> uint32_t -> uint32_t
// - func caiimaprst :: BoundIntIntMap* -> void
// - func caiimaparr :: BoundIntIntMap* -> uint32_t* -> uint32_t* -> void
//
// ==================================================




//
// All hash map/hash set related functions that require doing some hashing will ask the user to pass it as parameter
// instead of asking a pointer to a hash function at structure creation time.
// This is deliberate as storing a pointer to a hash function would restrain the hash integral type, possibly making
// the user having to write a 'lambda' for correct conversion.
// It also serves as a reminder for the user not to pass carelessly NULL values to those methods
// (unless the compare function supports NULL entries, which means extra checks, etc.).
//











/**
 * Represents a Hash Set of fixed size, i.e. with no growing capabilities (branching + rehashing required otherwise).
 * <br><br><br>
 * For any valid BoundHashSet <code>s</code>:<br><br>
 * Let <code>S</code> be the set theoretical representation of <code>s</code>.<br>
 * Let <code>s.has/1</code> and <code>s.rmv/1</code> be respectively the theoretical <code>cahsethas</code>
 * and the theoretical <code>cahsetrmv</code<, which both return whether
 *      <code>s</code> contains an element, given the element is valid.<br><br>
 * The following properties hold:
 *
 * <ol>
 *      <li><code>S ⊂ s.elements</code></li>
 *      <li><code>forall e ∈ S; e /= NULL</code></li>
 *      <li><code>forall e ∈ s.elements / S; e = NULL</code></li>
 *      <li><code>forall e; e ∈ S ↔ s.has e ^ e ∈ S ↔ s.rmv e</code></li>
 * </ol>
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>none</li>
 *      <li>the <code>element</code> parameter in a call to <code>cahsetput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #2 and <b>must always calloc</b> the <code>elements</code> array
 *          (see #ca_bound_hashset_dynamic_of)</li>
 *      <li>the <code>element</code> parameter in a call to <code>cahsethas</code> or to <code>cahsetrmv</code>
 *          <b>should not be NULL</b> unless your <code>s.compareFunctionPtr</code> supports
 *          <code>NULL</code> entries</li>
 * </ol>
 */
typedef struct BoundHashSet {
    /**
     * The array of elements of this hash set. Meant to be const.
     */
    void **elements;
    /**
     * The function for comparing this hash set's elements.
     * It must returns zero when both parameters are equals, and a non-zero value otherwise, for the hash set to work.
     * @return 0 if both parameters are equals.
     */
    int (*compareFunctionPtr)(const void*, const void*);
    /**
     * The capacity, i.e. the length of <code>elements</code>, of this hash set. Meant to be const.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>elements</code>, of this hash set.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    size_t count;
} BoundHashSet;





/**
 * Statically (array declaration) creates a set of pointers(void*) able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this integer set will be holding.
 *                  <h3>IMPORTANT:</h3>Must <b>not</b> yield any side-effect, macro expansion oblige.
 * @param compare_func_ptr[in] [int (void*, void*)] a function for comparing two elements of this hash set
 * @return [BoundHashSet] the resulting 32-bit integer set
 */
#define ca_bound_hashset_static_of(element_count, compare_func_ptr) (BoundHashSet) { \
    .elements = (void*[2 * element_count + 1]){},                                 \
    .compareFunctionPtr = (int (*)(const void *, const void *))compare_func_ptr,                   \
    .capacity = 2 * element_count + 1                                                              \
}


/**
 * Creates a set of pointers (void*) able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this set's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_hashset_free when they are done with the set.<br>
 * Typical usage:
 * @code
 *  BoundHashSet my_string_set = ca_bound_hashset_dynamic_of(
 *      A_BIG_NUMBER, strcmp
 *  );
 *  if ( !my_string_set.elements ) {
 *      error("BoundHashSet allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] the maximum number of elements this hash set will be holding
 * @param compare_func_ptr[in] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two elements of the set. <b>Must return 0 upon equality.</b>
 * @return the resulting set of pointers
 */
BoundHashSet ca_bound_hashset_dynamic_of(
        size_t element_count,
        int (*compare_func_ptr)(const void*, const void*)
);


/**
 * Frees a BoundHashSet allocated using #ca_bound_hashset_dynamic_of.
 * @param set[in,out] the set to be freed
 */
void ca_bound_hashset_free(BoundHashSet *set);





/**
 * Puts an element in <code>set</code>, ensuring that future calls to <code>cahsethas(element)</code> will return true
 * and that the next call to <code>cahsetrmv(element)</code> will return true.
 * @param set[in,out] [BoundHashSet*] the hash set to work on
 * @param element_hash[in] [integral type] the element hash
 * @param element[in] the element to put in <code>set</code>
 * @return [bool] whether <code>set</code> already contained <code>element</code> prior to the call
 */
bool cahsetput(BoundHashSet *set, uint32_t element_hash, void *element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not.
 * @param set[in] the hash set to work on
 * @param element_hash[in] the element hash
 * @param element[in] the element to check the presence in <code>set</code>
 * @return whether <code>set</code> does contain <code>element</code>
 */
bool cahsethas(BoundHashSet *set, uint32_t element_hash, const void *element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not,
 * then deletes <code>element</code> from <code>set</code>.
 * @param set[in,out] the hash set to work on
 * @param element_hash[in] the element hash
 * @param element[in] the element to delete in <code>set</code>
 * @return whether <code>set</code> contained <code>element</code> prior to its deletion
 */
bool cahsetrmv(BoundHashSet *set, uint32_t element_hash, const void *element);


/**
 * Resets a set, i.e. zeroes-out its <code>elements</code> array.
 * @param set[in,out] the hash set to work on
 */
void cahsetrst(BoundHashSet *set);


/**
 * Makes an array out of an hash set.
 * @param set[in] the hash set to copy into <code>dst</code>
 * @param dst[in,out] the destination array to copy elements into.
 *      Must be able to hold at least <code>set-&gt;count</code> elements.
 */
void cahsetarr(BoundHashSet *set, void **dst);











/**
 * Integral version of BoundHashSet.
 * See BoundHashSet documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntSet {
    /**
     * The array of elements of this integer set. Meant to be const.
     */
    uint32_t *elements;
    /**
     * The capacity, i.e. the length of <code>elements</code>, of this integer set. Meant to be const.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>elements</code>, of this hash set.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntSet;





/**
 * Statically (array declaration) creates a set of 32-bit integers able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this integer set will be holding.
 *                  <h3>IMPORTANT:</h3><br><br>Should <b>not</b> yield any side-effect, macro expansion oblige.
 * @return [BoundIntSet] the resulting 32-bit integer set
 */
#define ca_bound_intset_static_of(element_count) (BoundIntSet) { \
    .elements = (uint32_t[2 * (element_count) + 1]){},           \
    .capacity = 2 * (element_count) + 1                          \
}


/**
 * Creates a set of 32-bit integers able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this set's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intset_free when they are done with the set.<br>
 * Typical usage:
 * @code
 *  BoundIntSet my_int_set = ca_bound_intset_dynamic_of(
 *      A_BIG_NUMBER
 *  );
 *  if ( !my_int_set.elements ) {
 *      error("BoundIntSet allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] the maximum number of elements this integer set will be holding
 * @return the resulting 32-bit integer set
 */
BoundIntSet ca_bound_intset_dynamic_of(size_t element_count);


/**
 * Frees a BoundIntSet allocated using #ca_bound_intset_dynamic_of.
 * @param set[in,out] the set to be freed
 */
void ca_bound_intset_free(BoundIntSet *set);





/**
 * Puts an element in <code>set</code>, ensuring that future calls to <code>caisethas(element)</code> will return true
 * and that the next call to <code>caisetrmv(element)</code> will return true.
 * @param set[in,out] the hash set to work on
 * @param element[in] the element to put in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> already contained <code>element</code> prior to the call
 */
bool caisetput(BoundIntSet *set, uint32_t element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not.
 * @param set[in] the hash set to work on
 * @param element[in] the element to check the presence in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> does contain <code>element</code>
 */
bool caisethas(BoundIntSet *set, uint32_t element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not,
 * then deletes <code>element</code> from <code>set</code>.
 * @param set[in,out] the hash set to work on
 * @param element[in] the element to delete in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> contained <code>element</code> prior to its deletion
 */
bool caisetrmv(BoundIntSet *set, uint32_t element);


/**
 * Resets a set, i.e. zeroes-out its <code>elements</code> array.
 * @param set[in,out] the int set to work on
 */
void caisetrst(BoundIntSet *set);


/**
 * Makes an array out of a integral set.
 * @param set[in] the int set to copy into <code>dst</code>
 * @param dst[in,out] the destination array to copy elements into.
 *      Must be able to hold at least <code>set-&gt;count</code> integers.
 */
void caisetarr(BoundIntSet *set, uint32_t *dst);











/**
 * Represents a Hash Map of fixed size, i.e. with no growing capabilities (branching + rehashing required otherwise).
 * <br><br><br>
 * For any valid BoundHashMap <code>m</code>:<br><br>
 * Let <code>K</code> be <code>m</code>'s key set and <code>V</code> <code>m</code>'s value set.<br>
 * Let <code>m.get/1</code> and <code>m.rmv/1</code> be respectively the theoretical <code>cahmapget</code>
 * and the theoretical <code>cahmaprmv</code> which both return the value associated to a key
 * given the key the is valid.<br><br>
 * The following properties hold:
 *
 * <ol>
 *      <li><code>K ⊂ m.keys</code></li>
 *      <li><code>V ⊂ m.values</code></li>
 *      <li><code>forall k ∈ K; k /= NULL</code></li>
 *      <li><code>forall k ∈ m.keys / K; k = NULL</code></li>
 *      <li><code>forall v ∈ V; v /= NULL</code></li>
 *      <li><code>forall v ∈ m.values / V; v = NULL</code></li>
 *      <li><code>forall k; k ∈ K ↔ m.get k ∈ V ^ k ∈ K ↔ m.rmv k ∈ V</code></li>
 * </ol>
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>none</li>
 *      <li>none</li>
 *      <li>the <code>key</code> parameter in a call to <code>cahmapput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #3 and <b>must always calloc</b> the <code>keys</code> array
 *          (see #ca_bound_hashmap_dynamic_of)</li>
 *      <li>the <code>value</code> parameter in a call to <code>cahmapput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #5 and <b>must always calloc</b> the <code>values</code> array
 *          (see #ca_bound_hashmap_dynamic_of)</li>
 *      <li>the <code>key</code> parameter in a call to <code>cahmapget</code> or to <code>cahmaprmv</code>
 *          <b>should not be NULL</b> unless your <code>m.compareFunctionPtr</code> supports
 *          <code>NULL</code> entries</li>
 * </ol>
 *
 */
typedef struct BoundHashMap {
    /**
     * The underlying array used to hold keys and values.
     * Virtual arrays <code>keys</code> and <code>values</code> can be built with
     * <code>{mem[2 * k] | k ∈ ℕ}</code> and <code>{mem[2 * k + 1] | k ∈ ℕ}</code> respectively.
     */
    void **mem;
    /**
     * The function for comparing this hash map's keys.
     * It must returns zero when both parameters are equals, and a non-zero value otherwise, for the hash map to work.
     * @return 0 if both parameters are equals.
     */
    int (*compareFunctionPtr)(const void*, const void*);
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this hash map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>keys</code>, of this hash map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    size_t count;
} BoundHashMap;






/**
 * Statically (array declaration) creates a map of pointers (void*) keys and pointers (void*) values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this hash map will be holding
 *                      <h3>IMPORTANT:</h3>Must <b>not</b> yield any side-effect, macro expansion oblige.
 * @param compare_func_ptr[in] [int (void*, void*)] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two <code>key_type</code> keys. <b>Must return 0 upon equality.</b>
 * @return [BoundHashMap] the resulting hash map
 */
#define ca_bound_hashmap_static_of(element_count, compare_func_ptr) ((BoundHashMap) { \
    .mem = (void*[4 * element_count + 2]){},                                          \
    .compareFunctionPtr = (int (*)(const void *, const void *))compare_func_ptr,      \
    .capacity = 2 * element_count + 1                                                 \
})


/**
 * Creates a map of pointers (void*) keys and pointers (void*) values able to hold
 * <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_hashmap_free when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundHashMap my_string_to_struct_ptr_map = ca_bound_hashmap_dynamic_of(
 *      A_BIG_NUMBER, strcmp
 *  );
 *  if ( !my_string_to_struct_ptr_map.keys ) {
 *      error("BoundHashMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] the maximum number of elements this hash map will be holding
 * @param compare_func_ptr[in] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two <code>key_type</code> keys. <b>Must return 0 upon equality.</b>
 * @return the resulting hash map
 */
BoundHashMap ca_bound_hashmap_dynamic_of(
        size_t element_count,
        int (*compare_func_ptr)(const void*, const void*)
);


/**
 * Frees a BoundHashMap allocated using #ca_bound_hashmap_dynamic_of.
 * @param bound_hash_map[in,out] the map to be freed
 */
void ca_bound_hashmap_free(BoundHashMap *map);





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>cahmapget(key)</code> will return
 * <code>value</code> and that the next call to <code>cahmaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to add to <code>map</code>
 * @param value[in] the value associated with <code>key</code>
 * @return[NULLABLE] the old value associated with <code>key</code>, if any
 */
void* cahmapput(BoundHashMap *map, uint32_t key_hash, void *key, void *value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> NULL.
 */
void* cahmapget(BoundHashMap *map, uint32_t key_hash, void *key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> NULL.
 */
void* cahmaprmv(BoundHashMap *map, uint32_t key_hash, void *key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the hash map to work on
 */
void cahmaprst(BoundHashMap *map);


/**
 * Makes two arrays out of an hash map.
 * @param map[in] the hash map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void cahmaparr(BoundHashMap *map, void **key_dst, void **value_dst);











/**
 * Integral-key version of BoundHashMap.
 * See BoundHashMap documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntMap {
    /**
     * The array of keys of this int map.
     */
    uint32_t *keys;
    /**
     * The array of values of this int map.
     */
    void **values;
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this int map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-0 elements in <code>keys</code>, of this int map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntMap;





/**
 * Statically (array declaration) creates a map of 32-bit integer keys and pointers (void*) values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this int map will be holding
 *              <h3>IMPORTANT:</h3>Must <b>not</b> yield any side-effect, macro expansion oblige.
 * @return [BoundIntMap] the resulting int map
 */
#define ca_bound_intmap_static_of(element_count) (BoundIntMap) { \
    .keys = (uint32_t[2 * element_count + 1]){},                 \
    .values = (void*[2 * element_count + 1]){},                  \
    .capacity = 2 * element_count + 1                            \
}


/**
 * Creates a map of 32-bit integer keys and pointers (void*) values able to hold
 * <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intmap_free when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundIntMap my_int_to_struct_ptr_map = ca_bound_intmap_dynamic_of(
 *      A_BIG_NUMBER
 *  );
 *  if ( !my_int_to_struct_ptr_map.keys ) {
 *      error("BoundIntMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] the maximum number of elements this int map will be holding
 * @return the resulting int map
 */
BoundIntMap ca_bound_intmap_dynamic_of(size_t element_count);


/**
 * Frees a BoundIntMap allocated using #ca_bound_intmap_dynamic_of.
 * @param bound_int_map[in,out] the map to be freed
 */
void ca_bound_intmap_free(BoundIntMap *map);





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>caimapget(key)</code> will return
 * <code>value</code> and that the next call to <code>caimaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the int map to work on
 * @param key[in] the key to add to <code>map</code>. Must be different than UINT32_MAX.
 * @param value[in] the value associated with <code>key</code>
 * @return[NULLABLE] the old value associated with <code>key</code>, if any
 */
void* caimapput(BoundIntMap *map, uint32_t key, void *value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the int map to work on
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>. Must be different than UINT32_MAX.
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> NULL.
 */
void* caimapget(BoundIntMap *map, uint32_t key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the int map to work on
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>.
 *                  Must be different than UINT32_MAX.
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> NULL.
 */
void* caimaprmv(BoundIntMap *map, uint32_t key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the int map to work on
 */
void caimaprst(BoundIntMap *map);


/**
 * Makes two arrays out of an int map.
 * @param map[in] the int map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void caimaparr(BoundIntMap *map, uint32_t *key_dst, void **value_dst);











/**
 * Integral-value version of BoundIntMap.
 * See BoundHashMap documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntIntMap {
    /**
     * The underlying array used to hold keys and values.
     * Virtual arrays <code>keys</code> and <code>values</code> can be built with
     * <code>{mem[2 * k] | k ∈ ℕ}</code> and <code>{mem[2 * k + 1] | k ∈ ℕ}</code> respectively.
     */
    uint32_t *mem;
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this int map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-0 elements in <code>keys</code>, of this int map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntIntMap;





/**
 * Statically (array declaration) creates a map of 32-bit integer keys and values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this int-to-int map will be holding
 *              <h3>IMPORTANT:</h3>Must <b>not</b> yield any side-effect, macro expansion oblige.
 * @return [BoundIntIntMap] the resulting int-to-int map
 */
#define ca_bound_intintmap_static_of(element_count) (BoundIntIntMap) { \
    .mem = (uint32_t[4 * element_count + 2]){},                        \
    .capacity = 2 * element_count + 1                                  \
}


/**
 * Creates a map of 32-bit integer keys and values able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intintmap_free when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundIntIntMap my_int_to_int_map = ca_bound_intintmap_dynamic_of(
 *      A_BIG_NUMBER
 *  );
 *  if ( !my_int_to_int_map.keys ) {
 *      error("BoundIntIntMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] the maximum number of elements this int2int map will be holding
 * @return the resulting int map
 */
BoundIntIntMap ca_bound_intintmap_dynamic_of(size_t element_count);


/**
 * Frees a BoundIntIntMap allocated using #ca_bound_intintmap_dynamic_of.
 * @param map[in,out] the map to be freed
 */
void ca_bound_intintmap_free(BoundIntIntMap *map);





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>caiimapget(key)</code> will return
 * <code>value</code> and that the next call to <code>caiimaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the int-to-int map to work on
 * @param key[in] the key to add to <code>map</code>. Must be different than UINT32_MAX.
 * @param value[in] the value associated with <code>key</code>. Must be different than UINT32_MAX.
 * @return the old value associated with <code>key</code>, if any, -1 otherwise
 */
uint32_t caiimapput(BoundIntIntMap *map, uint32_t key, uint32_t value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the int-to-int map to work on
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>. Must be different than UINT32_MAX.
 * @return -1 if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> -1.
 */
uint32_t caiimapget(BoundIntIntMap *map, uint32_t key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the int-to-int map to work on
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>.
 *                  Must be different than UINT32_MAX.
 * @return -1 if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> -1.
 */
uint32_t caiimaprmv(BoundIntIntMap *map, uint32_t key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the int-to-int map to work on
 */
void caiimaprst(BoundIntIntMap *map);


/**
 * Makes two arrays out of an int-to-int map.
 * @param map[in] the int-to-int map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void caiimaparr(BoundIntIntMap *map, uint32_t *key_dst, uint32_t *value_dst);




#endif //CARIG_CAMAP_H
