//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CALOGGING_H
#define CARIG_CALOGGING_H


#include "camacro.h"



// To not abort upon allocation failure but instead return silently (not log the message),
// define CALOGGING_NO_ABORT_ERROR for compilation.


// ==================== ABSTRACT ====================
//
// Here we define an abstraction for logging messages into file sequentially
// – as opposed to deferred parallel logging or "asynchronous logging" for short.
//
// Related to logging in this file are:
//
// - func caloginit :: char* -> bool
// - func calogclose :: bool
// - macro CA_INFO :: string -> ... -> void
// - macro CA_WARN :: string -> ... -> void
// - macro CA_ERROR :: string -> ... -> void
// - func calogwriteraw :: string -> ... -> void
//
// ==================================================








/**
 * Initializes the logging service, by specifying the log file path.
 * @param filename[in] NULL or the path to the log file which future calls to CA_INFO, CA_WARN, CA_ERROR and
 *              calogwriteraw will write into.
 *              If it is NULL, those will write into stdout, stderr, stderr and stdout respectively instead of a log file.
 *
 * @return whether the operation succeeded or not
 */
bool caloginit(const char *filename);


/**
 * Closes the logging service, discarding the log file's handle.
 * CA_INFO, CA_WARN, CA_ERROR must <b>not</b> be called afterwards.
 * @return whether the operation succeeded or not
 */
bool calogclose();


#define CALOG_HEADER STRINGIFY(__FILE__) " line " STRINGIFY(__LINE__) ": "


/**
 * Logs a info message.
 *
 * @param MSG[in] [char*] the info message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_INFO(MSG, ...) private_info(" [INFO] " CALOG_HEADER MSG, ##__VA_ARGS__)


/**
 * Logs a warn message.
 *
 * @param MSG[in] [char*] the warn message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_WARN(MSG, ...) private_error(" [WARN] " CALOG_HEADER MSG, ##__VA_ARGS__)


/**
 * Logs a error message.
 *
 * @param MSG[in] [char*] the error message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_ERROR(MSG, ...) private_error(" [ERROR] " CALOG_HEADER MSG, ##__VA_ARGS__)




/**
 * Logs a message. Internal use only!
 * @param msg the message to log
 * @param ... the formatting arguments
 */
void private_info(const char *msg, ...);


/**
 * Logs a message. Internal use only!
 * @param msg the message to log
 * @param ... the formatting arguments
 */
void private_error(const char *msg, ...);


/**
 * Writes a formatted message to the log file, without adding metadata (such as the time).
 * @param msg[in] the formatting string
 * @param ... the format arguments
 */
void calogwriteraw(const char *msg, ...);






#endif //CARIG_CALOGGING_H
