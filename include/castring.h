//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CASTRING_H
#define CARIG_CASTRING_H


#include <stdint.h>
#include <string.h>
#include "camacro.h"


#ifndef CASTRING_SIZE_MAX
#define CASTRING_SIZE_MAX UINT32_MAX
#endif





// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to strings, which break down into two parts:
//
//
// # String
//
// A string abstraction which wraps a simple pointer to character with its length cached.
//
// Related to strings in this file are:
//
// - struct string
// - macro ca_str_of :: char* -> string
// - macro ca_str_constant_of :: char* -> string
// - func ca_str_to_CStr :: string -> char*
// - func castrhash :: size_t -> string* -> uint32_t
// - func castrhash_unsafe :: size_t -> char* -> uint32_t
// - func castrconcat :: string* -> string* -> string* -> void
// - func castrcmp :: string* -> string* -> int
// - func castrsubstr :: string* -> string* -> char*
//
//
// # String Buffer
//
// A string buffer abstraction, which behaves like a growable array to build strings.
//
// Related to string buffers in this file are:
//
// - struct StringBuffer
// - func ca_string_buffer_of :: size_t -> StringBuffer
// - func ca_string_buffer_free :: StringBuffer* -> void
// - func castrbufpop :: StringBuffer* -> char
// - func castrbufpush :: StringBuffer* -> char -> bool
// - func castrbufpush_all :: StringBuffer* -> string* -> bool
// - func castrbufpush_int :: StringBuffer* -> uint32_t -> bool
// - func castrbufpush_bool :: StringBuffer* -> bool -> bool
// - func castrbufpush_float :: StringBuffer* -> float -> int -> bool
//
// ==================================================






/**
 * Represents a string, by holding both the actual character array and its cached size, i.e. the number of characters
 * excluding the final '\0'.
 */
typedef struct string {
    /**
     * The underlying pointer to character
     */
    char *chars;
    /**
     * The size of the string, i.e. the cached result of <code>strlen(chars)</code>
     */
    size_t len;
} string;





#define private_stRoF(char_pointer, SEED) ({ \
    char *CA_UNIQUE_NAME(str, SEED) = (char_pointer); \
    (string) {                               \
        .chars = CA_UNIQUE_NAME(str, SEED),  \
        .len = strnlen(CA_UNIQUE_NAME(str, SEED), CASTRING_SIZE_MAX)\
    };\
})
/**
 * Creates a <code>string</code> structure on the stack from a pointer to character.
 * @param char_pointer[in] [char*] the original string (must stay valid, no deep copy is done)
 */
#define ca_str_of(char_pointer) private_stRoF(char_pointer, __COUNTER__)


/**
 * Creates a <code>string</code> structure on the stack from a compilation-constant string.
 * @param char_pointer[in] [char*] the constant string
 */
#define ca_str_constant_of(constant_string) (string) { \
    .chars = constant_string,                          \
    .len = (sizeof(constant_string) / sizeof(char)) - 1\
}


/**
 * Returns a raw char* from a <code>string</code>.
 * The string must have been allocated to allow a extra null-character, i.e. <code>s->chars</code>
 * must be able to hold at least <code>s->len + 1</code> characters.
 * @param s[in,out] the string to extract the raw char* from
 * @return the underlying raw char*, null-terminated properly
 */
char *ca_str_to_CStr(string *s);



/**
 * Computes a string's hash value, based on the characters at index <code>[begin, s-&gt;len[</code>
 * in <code>s-&gt;chars</code>.
 *
 * @pre <code>0 &lt;= begin &lt; s-&gt;len</code>
 * @param begin the index in <code>s-&gt;chars</code> to start hashing from
 * @param s the string to hash
 * @return the hash value of <code>s</code>
 */
uint32_t castrhash(size_t begin, string *s);


/**
 * Computes a string's hash value, based on the characters at index <code>[begin, strlen(s)[</code>
 * in <code>s</code>.
 * Unsafe version of castrhash
 *
 * @pre <code>0 &lt;= begin &lt; strlen(s)</code>
 * @param begin the index in <code>s</code> to start hashing from
 * @param s the string to hash
 * @return the hash value of <code>s</code>
 */
uint32_t castrhash_unsafe(size_t begin, const char *s);


/**
 * Concatenates <code>s1</code> and <code>s2</code> together in <code>result</code>,
 * that is
 * <pre><code>
 * forall i, j; (0 &lt;= i &lt; s1->len ∧ 0 &lt;= j &lt; s2->len)
 *      → (result->chars[i] = s1->chars[i] ∧ result->chars[s1->len + j] = s2->chars[j])
 * </code></pre>
 * @param result[out] the resulting string. Must be already allocated to hold
 *      <code>s1->len + s2->len + 1</code> characters.
 * @param s1[in] the first string to concatenate, as in <code>s1 . s2</code>.
 * @param s2[in] the second string to concatenate, as in <code>s1 . s2</code>.
 */
void castrconcat(string *restrict result, string *restrict s1, string *restrict s2);


/**
 * Lexicographically compares <code>s1</code> and <code>s2</code>.
 * Underlying implementation is directly <code>strncmp</code> from <code>"string.h"</code>.
 * @param s1[in] the first string to compare
 * @param s2[in] the second string to compare
 * @return <code>i</code>, as in
 * <pre><code>
 * (s1 = s2) ↔ (i = 0) ∧ (s1 &gt; s2) ↔ (i &gt; 0) ∧ (s1 &lt; s2) ↔ (i &lt; 0)
 * </code></pre>
 */
int castrcmp(string *s1, string *s2);


/**
 * Determines if <code>sub</code> is a substring of <code>s</code>, that is if
 * @code
 * exists i; 0 &lt;= i ∧ i + sub-&gt;len &lt;= s-&gt;len ∧
 *      (forall j; 0 &lt;= j &lt; sub-&gt;len → s-&gt;chars[i + j] = sub-&gt;chars[j])
 * @endcode
 *
 * @param s[in]
 * @param sub[in]
 * @return if <code>sub</code> is indeed a substring of <code>s</code>,
 * a pointer to the character in <code>s</code> that start the <code>sub</code> sequence, <code>NULL</code> otherwise.
 */
const char* castrsubstr(string *restrict s, string *restrict sub);




/**
 * Represents a growable array of characters. Ideal for building strings.
 * Only basic needs are implemented specifically for string buffers in this file.<br>
 */
typedef struct StringBuffer {
    /**
     * The underlying array of elements.
     */
    char *elements;
    /**
     * The capacity, i.e. the total space currently allocated for <code>elements</code>, of this array.
     */
    size_t capacity;
    /**
     * The count, i.e. the number of actually used (valid) elements in <code>elements</code>, of this array.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    size_t count;
} StringBuffer;





/**
 * Creates a string buffer with an initial capacity. Malloc and realloc functions
 * used are the ones configurable with macro (See castring.c).
 * The user should NULL-check this buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_string_buffer_free(buffer) when they are done with the buffer.<br>
 * Typical usage:
 * @code
 *  StringBuffer my_string_buffer = ca_string_buffer_of(n);
 *  if ( !my_string_buffer.elements ) {
 *      error("StringBuffer allocation failed");
 *      return MEM_ERROR;
 *  }
 * @endcode
 *
 * @param initial_capacity[in] [integral type] the initial number of characters the buffer will be able to hold
 * @return [StringBuffer] the resulting string buffer
 */
StringBuffer ca_string_buffer_of(size_t initialCapacity);


/**
 * Frees a string buffer. The free function used is the one configurable with macro (See top of file).
 * @param buffer[in,out] [StringBuffer*] the buffer to free the elements
 */
void ca_string_buffer_free(StringBuffer *buffer);





/**
 * Pops the last character in <code>buffer</code> and returns it.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_string_buffer.count == 0) {
 *      error("Trying to pop from an empty string buffer. That should never happen");
 *      exit(CODE_NEED_FIX);
 *  }
 *  char c = castrbufpop(&my_string_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param array[in,out] [StringBuffer*]
 *          the string buffer to pop a character from. Must be non-empty, i.e. <code>buffer-&gt;count &gt; 0</code>.
 * @return [char] the popped character
 */
char castrbufpop(StringBuffer *buffer);


/**
 * Appends a character <code>c</code> at the end of a string buffer <code>buffer</code>.
 * @param buffer[in,out] the buffer to append a character to
 * @param c[in] the character to append at the end of <code>buffer</code>
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush(StringBuffer *buffer, char c);


/**
 * Appends a string <code>s</code> at the end of a string buffer <code>buffer</code>.
 * @param buffer[in,out] the buffer to append several characters to
 * @param s[in] the string to append at the end of <code>buffer</code>
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_all(StringBuffer *buffer, string *restrict s);


/**
 * Appends a 32-bit integer at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param i[in] the integer to append
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_int(StringBuffer *buffer, uint32_t i);


/**
 * Appends a boolean value at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param b[in] the boolean value to append
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_bool(StringBuffer *buffer, bool b);


/**
 * Appends a float at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param f[in] the float to append
 * @param frac_digits[in] the number of digits of the fractional part of <code>f</code> to show
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_float(StringBuffer *buffer, float f, int frac_digits);


#endif //CARIG_CASTRING_H
