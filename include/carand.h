//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CARAND_H
#define CARIG_CARAND_H

#include <stdint.h>
#include "camacro.h"





// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to pseudo-random number generators.
//
// - struct CARandom
// - func carand_setseed :: CARandom* -> uint64_t -> void
// - func carandi :: CARandom* -> uint32_t
// - func carandi_bound :: CARandom* -> uint32_t -> uint32_t
// - func carandl :: CARandom* -> uint64_t
// - func carandl_bound :: CARandom* -> uint64_t -> uint64_t
// - func carandbool :: CARandom* -> bool
// - func carandf :: CARandom* -> float
// - func carandf_bound :: CARandom* -> float -> float
// - func carandd :: CARandom* -> double
// - func carandd_bound :: CARandom* -> double -> double
// - func carandgaussian :: CARandom* -> double -> double -> double
//
// ==================================================









/**
 * Holds the states of a pseudorandom number generator "instance".<br>
 * <b>Do not access its fields manually.</b><br>
 * Use dedicated functions in this file instead.
 */
typedef struct CARandom {
    uint64_t seed;
    double gaussian_deviate;
} CARandom;





/**
 * Sets the seed of a pseudorandom number generator "instance".<br>
 * @param rand the pseudorandom number generator "instance"
 * @param seed the new seed to work with
 */
void carand_setseed(CARandom *rand, uint64_t seed);


/**
 * Randomly generates an integer within <code>[0, 2<sup>32</sup> - 1[</code> with a pseudorandom
 * number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return an integer within <code>[0, 2<sup>32</sup> - 1[</code>
 */
uint32_t carandi(CARandom *rand);


/**
 * Randomly generates an integer within <code>[0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return an integer within <code>[0, ceiling[</code>
 */
uint32_t carandi_bound(CARandom *rand, uint32_t ceiling);


/**
 * Randomly generates an integer within <code>[0, 2<sup>64</sup> - 1[</code> with a pseudorandom number
 * generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return an integer within <code>[0, 2<sup>64</sup> - 1[</code>
 */
uint64_t carandl(CARandom *rand);


/**
 * Randomly generates an integer within <code>[0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return an integer within <code>[0, ceiling[</code>
 */
uint64_t carandl_bound(CARandom *rand, uint64_t ceiling);


/**
 * Randomly generates a boolean with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return a boolean
 */
bool carandbool(CARandom *rand);


/**
 * Randomly generates a float within <code>[0.0, 1.0[</code>
 * @param rand the pseudorandom number generator "instance"
 * @return a float within <code>[0.0, 1.0[</code>
 */
float carandf(CARandom *rand);


/**
 * Randomly generates a float within <code>[0.0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return a float in <code>[0.0, ceiling[</code>
 */
float carandf_bound(CARandom *rand, float ceiling);


/**
 * Randomly generates a double within <code>[0.0, 1.0[</code>
 * @param rand the pseudorandom number generator "instance"
 * @return a double within <code>[0.0, 1.0[</code>
 */
double carandd(CARandom *rand);


/**
 * Randomly generates a double within <code>[0.0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return a double in <code>[0.0, ceiling[</code>
 */
double carandd_bound(CARandom *rand, double ceiling);


/**
 * Randomly generates a gaussian-distributed double with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param mean the mean of the gaussian distribution
 * @param deviation the standard deviation of the gaussian distribution
 * @return
 */
double carandgaussian(CARandom *rand, double mean, double deviation);




#endif //CARIG_CARAND_H
