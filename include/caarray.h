//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAARRAY_H
#define CARIG_CAARRAY_H

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "camacro.h"




// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to arrays, which break down into four parts:
//
//
// # Array
//
// A simple array abstraction which stores length internally.
//
// Related to arrays in this file are:
//
// - struct array
// - macro ca_array_of :: type -> variadic list -> array
//
//
// # Unbound Array
//
// A growable array abstraction which offers similar behaviour to C++ vectors.
//
// Related to unbound arrays in this file are:
//
// - struct UnboundArray
// - macro ca_unbound_array_of :: type -> integral -> void* (size_t) -> void* (void*,size_t)) -> UnboundArray
// - macro ca_unbound_array_free :: UnboundArray -> void (void*) -> void
// - macro caarrget :: type -> UnboundArray* -> integral -> type instance
// - macro caarrset :: type -> UnboundArray* -> integral -> type instance -> void
// - macro caarrpush :: type -> UnboundArray* -> type instance -> void*
// - macro caarrpush_all :: type -> UnboundArray* -> type instance* -> integral -> void*
// - macro caarrpop :: type -> UnboundArray* -> type instance
// - func caarrrst :: UnboundArray* -> void
//
//
// # Bound Ring buffer
//
// A fixed-size ring buffer abstraction which provides overwriting capabilities.
//
// Related to bound ring buffers in this file are:
//
// - struct BoundRingBuffer
// - macro ca_bound_ringbuffer_static_of :: type -> integral -> BoundRingBuffer
// - macro ca_bound_ringbuffer_dynamic_of :: type -> integral -> void* (size_t) -> BoundRingBuffer
// - macro ca_bound_ringbuffer_free :: BoundRingBuffer -> void (void*) -> void
// - macro cabuffisfull :: BoundRingBuffer* -> bool
// - macro cabuffoffer :: type -> BoundRingBuffer* -> type instance -> bool
// - macro cabuffoffer_unsafe :: type -> BoundRingBuffer* -> type instance -> void
// - macro cabuffoffer_overwrite :: type -> BoundRingBuffer* -> type instance -> void
// - macro cabuffpoll :: type -> BoundRingBuffer* -> type instance
// - macro cabuffpeek :: type -> BoundRingBuffer* -> type instance
// - func cabuffrst :: BoundRingBuffer* -> void
//
//
// # Unbound Ring buffer
//
// A ring buffer abstraction which provides overwriting and growing capabilities.
//
// Related to unbound ring buffers in this file are:
//
// - struct UnboundRingBuffer
// - macro ca_unbound_ringbuffer_of :: type -> integral -> void* (size_t) -> void* (void*,size_t) -> UnboundRingBuffer
// - macro ca_unbound_ringbuffer_free :: UnboundRingBuffer -> void (void*) -> void
// - macro cadbuffisfull :: UnboundRingBuffer* -> bool
// - macro cadbuffoffer :: type -> UnboundRingBuffer* -> type instance -> void*
// - macro cadbuffoffer_overwrite :: type -> UnboundRingBuffer* -> type instance -> void
// - macro cadbuffpoll :: type -> UnboundRingBuffer* -> type instance
// - macro cadbuffpeek :: type -> UnboundRingBuffer* -> type instance
// - func cadbuffrst :: UnboundRingBuffer* -> void
//
// ==================================================




/**
 * Builds a compile-time C array out of a compile-time variadic list.<br>
 * Expansion example:<br><br>
 * <code>ARRAY_OF(char*, "foo", "bar")</code> --&gt;<br><br>
 * <code>(const char*[]){"foo", "bar"}</code>
 * @param type the type of the array
 * @param ... the variadic list
 */
#define ARRAY_OF(type, ...) (const type[]){__VA_ARGS__}





/**
 * Represents a C array with its length cached.
 */
typedef struct array {
    /**
     * The underlying array of elements.
     */
    void *elements;
    /**
     * The length, i.e the number of allocated elements for <code>elements</code>, of this array.
     */
    uint32_t len;
} array;





/**
 * Builds an <code>array</code> structure out of a compile-time variadic list.<br>
 * Expansion example:<br><br>
 * <code>CARRAY_OF(char*, "foo", "bar")</code> --&gt;
 * @code
 *  (array){
 *      .elements = (const char*[]){"foo", "bar"},
 *      .len = 16 / 8
 *  }
 * @endcode
 * @param type the type of the array
 * @param ... the variadic list
 */
#define ca_array_of(type, ...) (array) {                  \
    .elements = (type[]){__VA_ARGS__},                    \
    .len = sizeof( (type[]){__VA_ARGS__} ) / sizeof(type) \
}











/**
 * Represents an array that can grow upon appending elements.
 * However, it cannot shrinks upon popping its elements, i.e. it won't ever free parts of its memory, even unused.
 *
 * <br><br><br>
 * For any valid UnboundArray <code>a</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>a.capacity &gt; 0</code></li>
 *      <li><code>forall i; a.count &lt;= i &lt; a.capacity -&gt; a.elements[i] = undefined</code></li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>While the implementation actually supports 0-sized array, it is strongly advised <b>not</b>
 *      to do so, as it would make the first allocation a joke,
 *      and life harder for the first reallocation afterwards.</li>
 *      <li>Elements outside the boundaries <code>[0, a.count[</code> should never be accessed manually.</li>
 * </ol>
 *
 * <h3>Performance note:</h3>
 *
 * <p>If the UnboundArray is used for a large structure A, e.g. larger than <code>sizeof(void*)</code>,
 * and memory scope won't cause you a headache, e.g. the array is instantiated in a function
 * and all structures are allocated in that function's stack scope,
 * then it's recommended to make the UnboundArray an array
 * of A* rather than A to prevent unnecessary structure copy-pasting.</p>
 *
 * <p>Example:
 * @code
 *  void foo() {
 *      UnboundArray A_array = ca_unbound_array_of(A*, N, malloc, realloc);
 *      caarrpush(A*, A_array, ( &(A){...} ));
 *      ...
 *      bar(&A_array);
 *      ca_unbound_array_free(A_array, free);
 *  }
 *
 *  void bar(UnboundArray *A_array) {
 *      A *a_struct = caarrpop(A*, A_array);
 *      // read (only) from a_struct
 *      ...
 *  }
 * @endcode
 *
 * rather than
 *
 * @code
 *  void foo() {
 *      UnboundArray A_array = ca_unbound_array_of(A, N, malloc, realloc);
 *      caarrpush(A, A_array, ( (A){...} ));
 *      ...
 *      bar(&A_array);
 *      ca_unbound_array_free(A_array, free);
 *  }
 *
 *  void bar(UnboundArray *A_array) {
 *      A a_struct = caarrpop(A, A_array);
 *      // read (only) from a_struct
 *      ...
 *  }
 * @endcode
 *
 * </p>
 */
typedef struct UnboundArray {
    /**
     * The size of one element in <code>elements</code>.
     */
    size_t element_size;
    /**
     * The underlying array of elements.
     */
    void *elements;
    /**
     * A pointer to a function for reallocating memory. It must returns <code>NULL</code> when the reallocation failed,
     * and a non-NULL value when it succeeded.
     * @return[NULLABLE] a pointer to reallocated memory or <code>NULL</code> if it failed
     */
    void* (*reallocFuncPtr)(void*, size_t);
    /**
     * The capacity, i.e. the total space currently allocated for <code>elements</code>, of this array.
     */
    size_t capacity;
    /**
     * The count, i.e. the number of actually used (valid) elements in <code>elements</code>, of this array.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    size_t count;
} UnboundArray;





#define private_unbounDarraYoF(element_type, initial_capacity, alloc_func, realloc_func_ptr, SEED) ({ \
    __auto_type CA_UNIQUE_NAME(cap, SEED) = (initial_capacity);                                       \
    (UnboundArray){                                                                                   \
        .element_size = sizeof(element_type),                                                         \
        .elements = alloc_func(sizeof(element_type) * CA_UNIQUE_NAME(cap, SEED)),                     \
        .reallocFuncPtr = realloc_func_ptr,                                                           \
        .capacity = CA_UNIQUE_NAME(cap, SEED)                                                         \
    };                                                                                                \
})
/**
 * Creates an UnboundArray structure, allocating <code>elements</code> to hold
 * <code>initial_capacity</code> elements.<br>
 * The user should NULL-check this array's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_unbound_array_free(unbound_array, free_func) when they are done with the array.<br>
 * Typical usage:
 * @code
 *  UnboundArray my_struct_array = ca_unbound_array_of(
 *      MyStruct, n, malloc, realloc
 *  );
 *  if ( !my_struct_array.elements ) {
 *      error("UnboundArray allocation failed");
 *      return MEM_ERROR;
 *  }
 * @endcode
 *
 * @param element_type the type of the elements the array will be holding
 * @param initial_capacity[in] [integral type] the initial number of elements the array will be able to hold
 * @param alloc_func a macro/function name for allocating <code>elements</code> at first.
 *                  (realloc is used internally afterwards)
 * @param realloc_func_ptr[in] [void* (void*, size_t)] a pointer to a valid realloc function (no macro!).
 *          Will be stored and used internally.
 *          Must return NULL upon failing to reallocate and a non-NULL pointer to memory upon success.
 * @return [UnboundArray] the resulting unbound array
 */
#define ca_unbound_array_of(element_type, initial_capacity, alloc_func, realloc_func_ptr) \
private_unbounDarraYoF(element_type, initial_capacity, alloc_func, realloc_func_ptr, __COUNTER__)


/**
 * Frees an UnboundArray's elements.
 * @param unbound_array[in] [UnboundArray] the array whose elements are to be freed
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory.
 */
#define ca_unbound_array_free(unbound_array, free_func) free_func((unbound_array).elements)





/**
 * Gets the element at an index in <code>array</code>.<br>
 * Typical usage:
 * @code
 *  if ( DEBUG && !(0 &lt;= idx && idx &lt; my_struct_array.count) ) {
 *      error("Indexing array outside boundaries");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct foo = caarrget(MyStruct, &my_struct_array, idx);
 *  ...
 * @endcode
 *
 * To edit the actual element in <code>array</code> memory, consider referencing the result as follows:
 *
 * @code
 *  MyStruct *foo = &caarrget(MyStruct, &my_struct_array, idx);
 *  ... // edit foo
 * @endcode
 *
 * @pre <code>0 &lt;= index &lt; array-&gt;count</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in] [UnboundArray*] the array to get an element from
 * @param index[in] [integral type] the index in <code>array-&gt;elements</code> to retrieve an element at
 * @return [element_type] the element in <code>array-&gt;elements</code> at <code>index</code>
 */
#define caarrget(element_type, array, index) ((element_type*)(array)->elements)[index]


/**
 * Sets the element in <code>array-&gt;elements</code> at index <code>index</code> to <code>element</code>.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && !(0 &lt;= idx && idx &lt; my_struct_array.count) ) {
 *      error("Indexing array outside boundaries");
 *      exit(CODE_NEED_FIX);
 *  }
 *  caarrset(MyStruct, &my_struct_array, idx, (MyStruct){...});
 *  ...
 * @endcode
 *
 * Note that referencing the result of <code>caarrget</code> and editing with that pointer
 * instead of calling this function might be more efficient for small changes, e.g.:
 * @code
 *  // only care to set dead to true
 *  caarrset(MyStruct, &my_struct_array, idx, ( (MyStruct){.dead = true} ));
 * @endcode
 * vs
 * @code
 *  MyStruct *ref = &caarrget(MyStruct, &my_struct_array, idx);
 *  // only care to set dead to true
 *  ref-&gt;dead = true;
 * @endcode
 *
 * @pre <code>0 &lt;= index &lt; array-&gt;count</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*] the array to be modified
 * @param index[in] [integral type] the index in <code>array-&gt;elements</code> to set <code>element</code> at
 * @param element[in] [element_type] the element to set at index <code>index</code> in <code>array</code>
 */
#define caarrset(element_type, array, index, element) caarrget(element_type, array, index) = element


#define private_arRpusH(element_type, array, element, SEED) ({                                              \
    __auto_type CA_UNIQUE_NAME(arr, SEED) = (array);                                                        \
    void *CA_UNIQUE_NAME(mem, SEED) = NULL;                                                                 \
    if (CA_UNIQUE_NAME(arr, SEED)->count != CA_UNIQUE_NAME(arr, SEED)->capacity ||                          \
        !(CA_UNIQUE_NAME(mem, SEED) = private_groWunbounDarraY(CA_UNIQUE_NAME(arr, SEED))))                 \
        ((element_type*)CA_UNIQUE_NAME(arr, SEED)->elements)[CA_UNIQUE_NAME(arr, SEED)->count++] = element; \
                                                                                                            \
    CA_UNIQUE_NAME(mem, SEED);                                                                              \
})
/**
 * Pushes <code>element</code> at the end of <code>array</code>, possibly making <code>array</code> grow.<br>
 * Typical usage:
 * @code
 *  void *mem = caarrpush(MyStruct, &my_struct_array, ( (MyStruct){...} ));
 *  if (mem) {
 *      free(mem);
 *      error("Realloc failed. my_string_array may be corrupted");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*] the array to append to
 * @param element[in] [element_type] the element to append at the end of <code>array</code>
 * @return [NULLABLE] [void*] the memory that must be freed in case reallocation failed.<br>
 *  It is advised to drop completely <code>array</code> when such a non-NULL pointer is returned.
 */
#define caarrpush(element_type, array, element) private_arRpusH(element_type, array, element, __COUNTER__)


#define private_arRpusH_alL(element_type, array, new_elements, element_count, SEED) ({                          \
    __auto_type CA_UNIQUE_NAME(arr, SEED) = (array);                                                            \
    __auto_type CA_UNIQUE_NAME(count, SEED) = (element_count);                                                  \
    void *CA_UNIQUE_NAME(mem, SEED) = NULL;                                                                     \
    if (CA_UNIQUE_NAME(arr, SEED)->count + CA_UNIQUE_NAME(count, SEED) < CA_UNIQUE_NAME(arr, SEED)->capacity || \
        !(CA_UNIQUE_NAME(mem, SEED) = private_groWunbounDarraYamounT(                                           \
                                        CA_UNIQUE_NAME(arr, SEED),                                              \
                                        CA_UNIQUE_NAME(count, SEED))                                            \
                                        )) {                                                                    \
        memcpy((element_type*)CA_UNIQUE_NAME(arr, SEED)->elements + CA_UNIQUE_NAME(arr, SEED)->count,           \
                    (new_elements), CA_UNIQUE_NAME(count, SEED) * sizeof(element_type));                        \
        CA_UNIQUE_NAME(arr, SEED)->count += CA_UNIQUE_NAME(count, SEED);                                        \
    }                                                                                                           \
    CA_UNIQUE_NAME(mem, SEED);                                                                                  \
})
/**
 * Pushes <code>elements</code> at the end of <code>array</code>, possibly making <code>array</code> grow.<br>
 * Typical usage:
 * @code
 *  void *mem = caarrpush(MyStruct, &my_struct_array, ( (MyStruct[]){ (MyStruct){...},... } ), N);
 *  if (mem) {
 *      free(mem);
 *      error("Realloc failed. my_string_array may be corrupted");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*] the array to append to
 * @param elements[in] [element_type*] the elements to append at the end of <code>array</code>
 * @param element_count[in] [integral] the number of elements in <code>elements</code> to append
 * @return [NULLABLE] [void*] the memory that must be freed in case reallocation failed.<br>
 *  It is advised to drop completely <code>array</code> when such a non-NULL pointer is returned.
 */
#define caarrpush_all(element_type, array, elements, element_count) \
private_arRpusH_alL(element_type, array, elements, element_count, __COUNTER__)


#define private_arRpoP(element_type, array, SEED) ({                                        \
    __auto_type CA_UNIQUE_NAME(arr, SEED) = (array);                                        \
     caarrget(element_type, CA_UNIQUE_NAME(arr, SEED), --CA_UNIQUE_NAME(arr, SEED)->count); \
})
/**
 * Pops the last element of <code>array</code> and returns it.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_array.count == 0) {
 *      error("Trying to pop from an empty array. That should never happen");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct foo = caarrpop(MyStruct, &my_struct_array);
 *  ...
 * @endcode
 *
 * @pre <code>array-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*]
 *          the array to pop an element from. Must be non-empty, i.e. <code>array-&gt;count &gt; 0</code>.
 * @return [element_type] the popped element
 */
#define caarrpop(element_type, array) private_arRpoP(element_type, array, __COUNTER__)


/**
 * Clears <code>array</code>, i.e. sets its <code>count</code> to 0, effectively emptying the buffer.
 * @param array[in,out] [UnboundArray*] the array to clear.
 */
void caarrrst(UnboundArray *array);





/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundArray by +50%.
 * @param array[in,out] the array to grow
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDarraY(UnboundArray *array);


/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundArray by max(+50%, +(new_el_count * array->element_size)).
 * @param array[in,out] the array to grow
 * @param new_el_count the minimum number of new elements the array will be able to hold after growing
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDarraYamounT(UnboundArray *array, size_t new_el_count);











/**
 * Represents a ring buffer of fixed capacity.
 *
 * <br><br><br>
 * For any valid BoundRingBuffer <code>b</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>b.capacity_minus_one &gt;= 0</code></li>
 *      <li>@code forall i; 0 &lt;= i &lt; (b.capacity_minus_one - b.count) -&gt;
 *          b.elements[(b.head + i) % (b.capacity_minus_one + 1)] = undefined@endcode</li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>The implementation does <b>not</b> support 0-sized ring buffer.
 *          Instead, calling #ca_bound_ringbuffer_static_of(type, required_capacity) -&gt; BoundRingBuffer
 *          or #ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) -&gt; BoundRingBuffer
 *          will force a non-null size.</li>
 *      <li>Elements should <b>never</b> be accessed manually.
 *          Do rely on the proposed methods such as #cabuffoffer(element_type, buffer, element) -&gt; bool and
 *          #cabuffpoll(element_type, buffer) -&gt; element instead.</li>
 * </ol>
 */
typedef struct BoundRingBuffer {
    /**
     * The underlying array of elements. Meant to be const.
     */
    void *elements;
    /**
     * The capacity, i.e. the number of elements <code>elements</code> is able to hold, minus one.<br>
     * Used to speed up modulo operation under the assumption that the capacity is a power of two. Meant to be const.
     */
    uint32_t capacity_minus_one;
    /**
     * The ring buffer's head, i.e. its writing end.
     * @invariant @code 0 &lt;= head &lt;= capacity_minus_one@endcode
     */
    uint32_t head;
    /**
     * The ring buffer's tail, i.e. its reading end.
     * @invariant @code 0 &lt;= tail &lt;= capacity_minus_one@endcode
     */
    uint32_t tail;
    /**
     * The ring buffer's element count, i.e. the number of elements "between" <code>tail</code> and <code>head</code>.
     * @invariant @code 0 &lt;= count &lt;= (capacity_minus_one + 1)@endcode
     */
    uint32_t count;
} BoundRingBuffer;





/**
 * Statically (array declaration) creates a ringbuffer
 * with as capacity <code>required_capacity</code> rounded up to the next power of two.
 * @param type the type of the elements the ringbuffer will be holding
 * @param required_capacity[in] [integral type] the minimum capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 *              <h3>IMPORTANT!</h3><br>Must <b>not</b> bear side-effects (macro expansion oblige)
 * @return [BoundRingBuffer] the resulting ringbuffer
 */
#define ca_bound_ringbuffer_static_of(type, required_capacity) ((BoundRingBuffer){ \
        .elements = (type[required_capacity < 2 ? 1 :                              \
                    1 << ( 32 - __builtin_clz(required_capacity - 1) )]){},        \
        .capacity_minus_one = (required_capacity < 2 ? 1 :                         \
                    1 << ( 32 - __builtin_clz(required_capacity - 1) )) - 1        \
})


#define private_bounDringbuffeRdynamiCoF(type, required_capacity, alloc_func, SEED) ({ \
    int CA_UNIQUE_NAME(cap, SEED) = (int)(required_capacity);                          \
    CA_UNIQUE_NAME(cap, SEED) = CA_UNIQUE_NAME(cap, SEED) < 2 ? 1 :                    \
                    1 << ( 32 - __builtin_clz(CA_UNIQUE_NAME(cap, SEED) - 1) );        \
    (BoundRingBuffer) {                                                                \
        .elements = alloc_func(sizeof(type) * CA_UNIQUE_NAME(cap, SEED)),              \
        .capacity_minus_one = CA_UNIQUE_NAME(cap, SEED) - 1                            \
    };                                                                                 \
})
/**
 * Creates a ring buffer with as capacity <code>required_capacity</code> rounded up to the next power of two.<br>
 * The user should NULL-check this ring buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_ringbuffer_free(buffer, free_func) when they are done with the ringbuffer.<br>
 * Typical usage:
 * @code
 *  BoundRingBuffer my_struct_ringbuffer = ca_bound_ringbuffer_dynamic_of(
 *      MyStruct, A_BIG_NUMBER, malloc
 *  );
 *  if ( !my_struct_ringbuffer.elements ) {
 *      error("Ringbuffer allocation failed.");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param type the type of the elements the ringbuffer will be holding
 * @param required_capacity[in] [integral type] the minimum capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 * @param alloc_func[in] [void* (size_t)] a macro/function name for allocating memory
 * @return [BoundRingBuffer] the resulting ringbuffer
 */
#define ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) \
private_bounDringbuffeRdynamiCoF(type, required_capacity, alloc_func, __COUNTER__)


/**
 * Frees a ring buffer's elements. Should only be used for ring buffers allocated dynamically using
 * #ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) -&gt; BoundRingBuffer.
 * @param buffer[in] [BoundRingBuffer] the ringbuffer to free
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory
 */
#define ca_bound_ringbuffer_free(buffer, free_func) free_func((buffer).elements)





#define private_bufFiSfulL(buffer, SEED) ({                                                \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                      \
    CA_UNIQUE_NAME(buf, SEED)->count == CA_UNIQUE_NAME(buf, SEED)->capacity_minus_one + 1; \
})
/**
 * Checks whether <code>buffer</code> is full or not.
 * @param buffer[in] [BoundRingBuffer*] the buffer to scan
 * @return whether the buffer is full or not
 */
#define cabuffisfull(buffer) private_bufFiSfulL(buffer, __COUNTER__)


#define private_bufFoffeR(element_type, buffer, element, SEED) ({             \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                         \
    bool CA_UNIQUE_NAME(is_full, SEED) = false;                               \
    if ( !cabuffisfull(CA_UNIQUE_NAME(buf, SEED)) ) {                         \
        cabuffoffer_unsafe(element_type, CA_UNIQUE_NAME(buf, SEED), element); \
        CA_UNIQUE_NAME(is_full, SEED) = true;                                 \
    }                                                                         \
    CA_UNIQUE_NAME(is_full, SEED);                                            \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Typical usage:
 * @code
 *  if ( !cabuffoffer(MyStruct, &my_struct_buffer, ( (MyStruct){...} )) ) {
 *      // handle my_struct_buffer full
 *      ...
 *  }
 *  ...
 * @endcode
 *
 * For an unsafe version with 1 less branch, see #cabuffoffer_unsafe(element_type, buffer, element).
 *
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @return [bool] whether <code>buffer</code> had enough space for appending <code>element</code>
 * @see #cabuffoffer_unsafe(element_type, buffer, element)
 */
#define cabuffoffer(element_type, buffer, element) private_bufFoffeR(element_type, buffer, element, __COUNTER__)


#define private_bufFoffeR_unsafE(element_type, buffer, element, SEED) ({                             \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                                \
    ((element_type*)CA_UNIQUE_NAME(buf, SEED)->elements)[CA_UNIQUE_NAME(buf, SEED)->head] = element; \
    CA_UNIQUE_NAME(buf, SEED)->head = (CA_UNIQUE_NAME(buf, SEED)->head + 1) &                        \
                                       CA_UNIQUE_NAME(buf, SEED)->capacity_minus_one;                \
    CA_UNIQUE_NAME(buf, SEED)->count++;                                                              \
    {}                                                                                               \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Unsafe version of #cabuffoffer(element_type, buffer, element) -&gt; bool.<br>
 * Typical usage:
 * @code
 *  if ( cabuffisfull(&my_struct_buffer) ) {
 *      // handle my_struct_buffer full
 *      ...
 *  } else {
 *      cabuffoffer_unsafe(MyStruct, &my_struct_buffer, ( (MyStruct){...} ));
 *      ...
 *  }
 * @endcode
 *
 * @pre <code>¬cabuffisfull(buffer)</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cabuffoffer(element_type, buffer, element) -&gt; bool
 */
#define cabuffoffer_unsafe(element_type, buffer, element) \
private_bufFoffeR_unsafE(element_type, buffer, element, __COUNTER__)


#define private_bufFoffeRoverwritE(element_type, buffer, element, SEED) ({                           \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                                \
    ((element_type*)CA_UNIQUE_NAME(buf, SEED)->elements)[CA_UNIQUE_NAME(buf, SEED)->head] = element; \
    CA_UNIQUE_NAME(buf, SEED)->head = (CA_UNIQUE_NAME(buf, SEED)->head + 1) &                        \
                                       CA_UNIQUE_NAME(buf, SEED)->capacity_minus_one;                \
    if (cabuffisfull(CA_UNIQUE_NAME(buf, SEED)))                                                     \
        CA_UNIQUE_NAME(buf, SEED)->tail = CA_UNIQUE_NAME(buf, SEED)->head;                           \
    else CA_UNIQUE_NAME(buf, SEED)->count++;                                                         \
    {}                                                                                               \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head, possibly overwriting not-yet-polled elements.<br>
 * Variant of #cabuffoffer(element_type, buffer, element) -&gt; bool that allows overwriting.<br>
 *
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cabuffoffer(element_type, buffer, element) -&gt; bool
 */
#define cabuffoffer_overwrite(element_type, buffer, element) \
private_bufFoffeRoverwritE(element_type, buffer, element, __COUNTER__)


#define private_bufFpolL(element_type, buffer, SEED) ({                                        \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                          \
    element_type CA_UNIQUE_NAME(res, SEED) =                                                   \
        ((element_type*)CA_UNIQUE_NAME(buf, SEED)->elements)[CA_UNIQUE_NAME(buf, SEED)->tail]; \
    CA_UNIQUE_NAME(buf, SEED)->tail = (CA_UNIQUE_NAME(buf, SEED)->tail + 1) &                  \
                                       CA_UNIQUE_NAME(buf, SEED)->capacity_minus_one;          \
    CA_UNIQUE_NAME(buf, SEED)->count--;                                                        \
    CA_UNIQUE_NAME(res, SEED);                                                                 \
})
/**
 * Polls a ring buffer, i.e. reads the element at its tail then increments the latter.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cabuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to be polled
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see BoundRingBuffer
 */
#define cabuffpoll(element_type, buffer) private_bufFpolL(element_type, buffer, __COUNTER__)


#define private_bufFpeeK(element_type, buffer, SEED) ({                                    \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                      \
    ((element_type*)CA_UNIQUE_NAME(buf, SEED)->elements)[CA_UNIQUE_NAME(buf, SEED)->tail]; \
})
/**
 * Take a peek at a ring buffer, i.e. reads the element at its tail.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cabuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to peek from
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see BoundRingBuffer
 */
#define cabuffpeek(element_type, buffer) private_bufFpeeK(element_type, buffer, __COUNTER__)


/**
 * Resets a ring buffer, i.e. sets <code>buffer-&gt;count</code> to 0.
 * @param buffer[in,out] the buffer to be reset
 */
void cabuffrst(BoundRingBuffer *buffer);











/**
 * Represents a ring buffer of dynamic capacity.
 *
 * <br><br><br>
 * For any valid UnboundRingBuffer <code>b</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>b.capacity_minus_one &gt;= 0</code></li>
 *      <li>@code forall i; 0 &lt;= i &lt; (b.capacity_minus_one - b.count) →
 *          b.elements[(b.head + i) % (b.capacity_minus_one + 1)] = undefined@endcode</li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>The implementation does <b>not</b> support 0-sized ring buffer.
 *          Instead, calling #ca_unbound_ringbuffer__of(type, required_capacity) -&gt; UnboundRingBuffer
 *          will force a non-null size.</li>
 *      <li>Elements should <b>never</b> be accessed manually.
 *          Do rely on the proposed methods such as #cabuffoffer(element_type, buffer, element) -&gt; bool and
 *          #cabuffpoll(element_type, buffer) instead.</li>
 * </ol>
 */
typedef struct UnboundRingBuffer {
    /**
     * The underlying array of elements. Meant to be const.
     */
    void *elements;
    /**
     * The size of one element in <code>elements</code>.
     */
    size_t element_size;
    /**
     * A pointer to a realloc-like function for reallocating memory.
     * @return NULL upon reallocation failure, non-NULL memory otherwise
     */
    void* (*reallocFuncPtr)(void*, size_t);
    /**
     * The capacity, i.e. the number of elements <code>elements</code> is able to hold, minus one.<br>
     * Used to speed up modulo operation under the assumption that the capacity is a power of two. Meant to be const.
     */
    uint32_t capacity_minus_one;
    /**
     * The ring buffer's head, i.e. its writing end.
     * @invariant @code 0 &lt;= head &lt;= capacity_minus_one@endcode
     */
    uint32_t head;
    /**
     * The ring buffer's tail, i.e. its reading end.
     * @invariant @code 0 &lt;= tail &lt;= capacity_minus_one@endcode
     */
    uint32_t tail;
    /**
     * The ring buffer's element count, i.e. the number of elements "between" <code>tail</code> and <code>head</code>.
     * @invariant @code 0 &lt;= count &lt;= (capacity_minus_one + 1)@endcode
     */
    uint32_t count;
} UnboundRingBuffer;






#define private_unbounDringbuffeRoF(type, initial_capacity, alloc_func, realloc_func_ptr, SEED) ({ \
    int CA_UNIQUE_NAME(cap, SEED) = (int)(initial_capacity);                                       \
    CA_UNIQUE_NAME(cap, SEED) = CA_UNIQUE_NAME(cap, SEED) < 2 ? 1 :                                \
                1 << ( 32 - __builtin_clz(CA_UNIQUE_NAME(cap, SEED) - 1) );                        \
    (UnboundRingBuffer) {                                                                          \
        .element_size = sizeof(type),                                                              \
        .elements = alloc_func(sizeof(type) * CA_UNIQUE_NAME(cap, SEED)),                          \
        .capacity_minus_one = CA_UNIQUE_NAME(cap, SEED) - 1,                                       \
        .reallocFuncPtr = realloc_func_ptr                                                         \
    };                                                                                             \
})
/**
 * Creates a ringbuffer with as capacity <code>initial_capacity</code> rounded up to the next power of two.<br>
 * The user should NULL-check this ring buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_unbound_ringbuffer_free(buffer, free_func) when they are done with the ringbuffer.<br>
 * Typical usage:
 * @code
 *  UnboundRingBuffer my_struct_ringbuffer = ca_unbound_ringbuffer_of(
 *      MyStruct, A_BIG_NUMBER, malloc, realloc
 *  );
 *  if ( !my_struct_ringbuffer.elements ) {
 *      error("Ringbuffer allocation failed.");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param type the type of the elements the ringbuffer will be holding
 * @param initial_capacity[in] [integral type] the initial capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 * @param alloc_func[in] [void* (size_t)] a macro/function name for allocating memory
 * @param realloc_func[in] [void* (void*,size_t)] a pointer to a function for reallocating memory.
 *                  Must return non-NULL memory upon reallocation failure.
 * @return [UnboundRingBuffer] the resulting ringbuffer
 */
#define ca_unbound_ringbuffer_of(type, initial_capacity, alloc_func, realloc_func_ptr) \
private_unbounDringbuffeRoF(type, initial_capacity, alloc_func, realloc_func_ptr, __COUNTER__)


/**
 * Frees a ring buffer's elements. Should only be used for ring buffers allocated dynamically using
 * #ca_unbound_ringbuffer_of(type, initial_capacity, alloc_func, realloc_func_ptr) -&gt; UnboundRingBuffer.
 * @param buffer[in] [UnboundRingBuffer] the ringbuffer to free
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory
 */
#define ca_unbound_ringbuffer_free(buffer, free_func) free_func((buffer).elements)





/**
 * Checks whether <code>buffer</code> is full or not.
 * @param buffer[in] [UnboundRingBuffer*] the buffer to scan
 * @return whether the buffer is full or not
 */
#define cadbuffisfull(buffer) cabuffisfull(buffer)


#define private_dbufFoffeR(element_type, buffer, element, SEED) ({                                   \
    __auto_type CA_UNIQUE_NAME(buf, SEED) = (buffer);                                                \
    const void* CA_UNIQUE_NAME(mem, SEED) = cadbuffisfull(CA_UNIQUE_NAME(buf, SEED)) ?               \
        private_groWunbounDringbuffeR(CA_UNIQUE_NAME(buf, SEED)) : NULL;                             \
    ((element_type*)CA_UNIQUE_NAME(buf, SEED)->elements)[CA_UNIQUE_NAME(buf, SEED)->head] = element; \
    CA_UNIQUE_NAME(buf, SEED)->head = (CA_UNIQUE_NAME(buf, SEED)->head + 1) &                        \
                                       CA_UNIQUE_NAME(buf, SEED)->capacity_minus_one;                \
    CA_UNIQUE_NAME(buf, SEED)->count++;                                                              \
    CA_UNIQUE_NAME(mem, SEED);                                                                       \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Typical usage:
 * @code
 *  void* mem = cadbuffoffer(MyStruct, &my_struct_buffer, ( (MyStruct){...} ));
 *  if (mem) {
 *      error("Failed to reallocate UnboundRingBuffer");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @return [out][void*] if reallocation failed, the memory to be freed, NULL otherwise
 */
#define cadbuffoffer(element_type, buffer, element) private_dbufFoffeR(element_type, buffer, element, __COUNTER__)


/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head, possibly overwriting not-yet-polled elements.<br>
 * Variant of #cadbuffoffer(element_type, buffer, element) -&gt; void* that allows overwriting.<br>
 *
 * @pre <code>¬cadbuffisfull(buffer)</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cadbuffoffer(element_type, buffer, element) -&gt; void*
 */
#define cadbuffoffer_overwrite(element_type, buffer, element) cabuffoffer_overwrite(element_type, buffer, element)


/**
 * Polls a ring buffer, i.e. reads the element at its tail then increments the latter.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cadbuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to be polled
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see UnboundRingBuffer
 */
#define cadbuffpoll(element_type, buffer) cabuffpoll(element_type, buffer)


/**
 * Take a peek at a ring buffer, i.e. reads the element at its tail.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to peek at empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cadbuffpeek(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in] [UnboundRingBuffer*] the buffer to peek from
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see UnboundRingBuffer
 */
#define cadbuffpeek(element_type, buffer) cabuffpeek(element_type, buffer)


/**
 * Resets a ring buffer, i.e. sets <code>buffer-&gt;count</code> to 0.
 * @param buffer[in,out] the buffer to be reset
 */
void cadbuffrst(UnboundRingBuffer *buffer);


/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundRingBuffer by +100%.
 * @param buffer[in,out] the array to grow
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDringbuffeR(UnboundRingBuffer *buffer);


#endif //CARIG_CAARRAY_H