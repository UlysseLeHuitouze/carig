//
// Created by Ulysse Le Huitouze on 25/03/2024.
//
#include "carand.h"
#include "camath.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>

#define BENCHMARK_TURNS 100000


static void runVec();

static CARandom rng;
static uint64_t seed;



int main() {
    carand_setseed(&rng, seed = time(NULL));

    printf("int32x2    int32x4    int64x2    int64x4    float32x2  float32x4  "
           "float64x2  float64x4  vector normalization\n");
    runVec();
    puts("\n");
}



static ivec2_t genIVec2() {
    int32_t x = (int32_t)carandi(&rng);
    int32_t y = (int32_t)carandi(&rng);
    return ivec2(x, y);
}

static ivec4_t genIVec4() {
    int32_t x = (int32_t)carandi(&rng);
    int32_t y = (int32_t)carandi(&rng);
    int32_t z = (int32_t)carandi(&rng);
    int32_t w = (int32_t)carandi(&rng);
    return ivec4(x, y, z, w);
}

static lvec2_t genLVec2() {
    int64_t x = (int64_t)carandl(&rng);
    int64_t y = (int64_t)carandl(&rng);
    return lvec2(x, y);
}

static lvec4_t genLVec4() {
    int64_t x = (int64_t)carandl(&rng);
    int64_t y = (int64_t)carandl(&rng);
    int64_t z = (int64_t)carandl(&rng);
    int64_t w = (int64_t)carandl(&rng);
    return lvec4(x, y, z, w);
}

static vec2_t genVec2() {
    float x = carandf_bound(&rng, 10000.f);
    float y = carandf_bound(&rng, 10000.f);
    return vec2(x, y);
}

static vec4_t genVec4() {
    float x = carandf_bound(&rng, 10000.f);
    float y = carandf_bound(&rng, 10000.f);
    float z = carandf_bound(&rng, 10000.f);
    float w = carandf_bound(&rng, 10000.f);
    return vec4(x, y, z, w);
}

static dvec2_t genDVec2() {
    double x = carandd_bound(&rng, 10000);
    double y = carandd_bound(&rng, 10000);
    return dvec2(x, y);
}
static dvec4_t genDVec4() {
    double x = carandd_bound(&rng, 10000);
    double y = carandd_bound(&rng, 10000);
    double z = carandd_bound(&rng, 10000);
    double w = carandd_bound(&rng, 10000);
    return dvec4(x, y, z, w);
}

static void runVec() {

    FILE *dummy = fopen("/dev/null", "r");
    uint64_t total;
    int digits;


    // ivec2_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        ivec2_t u = genIVec2();
        ivec2_t v = genIVec2();
        ivec2_t w = genIVec2();
        int32_t z;
        int32_t h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%d%d%d%d%d", (int)z, (int)h, (int)u[0], (int)v[0], (int)w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // ivec4_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        ivec4_t u = genIVec4();
        ivec4_t v = genIVec4();
        ivec4_t w = genIVec4();
        int32_t z;
        int32_t h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
            v = caveccross(w, u);
            w = caveccross(u, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%d%d%d%d%d", (int)z, (int)h, (int)u[0], (int)v[0], (int)w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // lvec2_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        lvec2_t u = genLVec2();
        lvec2_t v = genLVec2();
        lvec2_t w = genLVec2();
        int64_t z;
        int64_t h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%d%d%d%d%d", (int)z, (int)h, (int)u[0], (int)v[0], (int)w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // lvec4_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        lvec4_t u = genLVec4();
        lvec4_t v = genLVec4();
        lvec4_t w = genLVec4();
        int64_t z;
        int64_t h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
            v = caveccross(w, u);
            w = caveccross(u, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%d%d%d%d%d", (int)z, (int)h, (int)u[0], (int)v[0], (int)w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);
    
    
    

    // vec2_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        vec2_t u = genVec2();
        vec2_t v = genVec2();
        vec2_t w = genVec2();
        float z;
        float h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%f%f%f%f%f", z, h, u[0], v[0], w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // vec4_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        vec4_t u = genVec4();
        vec4_t v = genVec4();
        vec4_t w = genVec4();
        float z;
        float h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
            v = caveccross(w, u);
            v = caveccross(u, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%f%f%f%f%f", z, h, u[0], v[0], w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // dvec2_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        dvec2_t u = genDVec2();
        dvec2_t v = genDVec2();
        dvec2_t w = genDVec2();
        double z;
        double h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%f%f%f%f%f", z, h, u[0], v[0], w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);



    // dvec4_t

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        dvec4_t u = genDVec4();
        dvec4_t v = genDVec4();
        dvec4_t w = genDVec4();
        double z;
        double h;

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            z = cavecdot(u, v);
            h = cavecsum(u);
            u = cavecmix(u, w, u);
            w = cavecmix(w, v, v);
            v = caveccross(w, u);
            v = caveccross(u, v);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%f%f%f%f%f", z, h, u[0], v[0], w[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);





    // vector normalization

    total = 0;
    for (int _ = 0; _ < 100; _++) {
        vec2_t a = genVec2();
        vec4_t b = genVec4();
        dvec2_t c = genDVec2();
        dvec4_t d = genDVec4();

        clock_t begin = clock();
        for (int i = 0; i < BENCHMARK_TURNS; i++) {
            a = cavecnormalize(a);
            b = cavecnormalize(b);
            c = cavecnormalize(c);
            d = cavecnormalize(d);
        }
        clock_t end = clock();
        total += end - begin;
        fprintf(dummy, "%f%f%f%f", a[0], b[0], c[0], d[0]);
    }
    printf("%" PRIu64, total);
    digits = (int)log10f((float)total) + 1;
    for (int i = 0; i < 11 - digits; i++)
        putc(' ', stdout);


    fclose(dummy);
}
