//
// Created by Ulysse Le Huitouze on 27/03/2024.
//
#include <stdio.h>
#include <stdlib.h>

int main() {

    loop:
    printf("Re-run benchmarks? (y|n):");
    fflush(stdout);
    int rerun;
    for (rerun = getchar();
         rerun != 'y' && rerun != 'n' && rerun != 'Y' && rerun != 'N';
         getchar(), rerun = getchar()) {
        printf("Invalid answer. Enter either 'y' or 'n':");
        fflush(stdout);
    }

    switch (rerun) {
        case 'y':
        case 'Y':
            system("cd benchmark && make benchmark");
            getchar();
            goto loop;
    }

    printf("Which mode would you like to build carig math module in?\n"
           "\t1. Intrinsic-free\n"
           "\t2. Intrinsic\n"
           "Enter a number (1|2):");
    fflush(stdout);
    int mode;
    for (getchar(), mode = getchar(); mode != '1' && mode != '2'; getchar(), mode = getchar()) {
        printf("Invalid number. Enter either '1' or '2':");
        fflush(stdout);
    }

    printf("Would you like to always enable intrinsics for vector normalization? (y|n):");
    fflush(stdout);
    int norm;
    for (getchar(), norm = getchar();
            norm != 'y' && norm != 'n' && norm != 'Y' && norm != 'N';
            getchar(), norm = getchar()) {
        printf("Invalid answer. Enter either 'y' or 'n':");
        fflush(stdout);
    }

    char cmd[] = "MATH_MODE=3 DISABLE_INTRINSICS_VECNORMALIZE=0 make build";
    cmd[10] = mode;
    switch (norm) {
        case 'n':
        case 'N':
            cmd[44] = '1';
            break;
        default:
            break;
    }
    system(cmd);
}
