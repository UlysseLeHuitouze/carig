
INC=include
HEADERS=$(INC)/caarray.h $(INC)/calogging.h $(INC)/camacro.h $(INC)/camap.h $(INC)/camath.h $(INC)/carand.h $(INC)/castring.h \
		$(INC)/carig.h
SRC=src
SOURCES=$(SRC)/caarray.c $(SRC)/calogging.c $(SRC)/camap.c $(SRC)/camath.c $(SRC)/carand.c $(SRC)/castring.c

FLAGS=-I$(INC) -Wall -Wextra -O2
MATH_FLAGS=-DCAMATH_ENABLE_INTRINSICS
ifeq ($(MATH_MODE), 1)
MATH_FLAGS=
endif

ifeq ($(DISABLE_INTRINSICS_VECNORMALIZE), 0)
MATH_FLAGS:=-DCAMATH_ENABLE_INTRINSICS_VECNORMALIZE $(MATH_FLAGS)
endif
ifndef DISABLE_INTRINSICS_VECNORMALIZE
MATH_FLAGS:=-DCAMATH_ENABLE_INTRINSICS_VECNORMALIZE $(MATH_FLAGS)
endif

all: build

benchmark:
	@cd benchmark && make

build: bin_folder all_static

install: opt_folder
	mv bin/libcarig.a /opt/carig/lib
	cp $(HEADERS) /opt/carig/include

uninstall:
	rm -rf /opt/carig

clean:
	rm bin/*

opt_folder:
	mkdir -p /opt/carig/lib /opt/carig/include

bin_folder:
	mkdir -p bin

all_static: array logging map rand string math
	ar rcs bin/libcarig.a bin/caarray.o bin/calogging.o bin/camap.o bin/carand.o bin/castring.o bin/camath.o

array: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -o bin/caarray.o $(SRC)/caarray.c

logging: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -o bin/calogging.o $(SRC)/calogging.c

map: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -o bin/camap.o $(SRC)/camap.c

rand: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -o bin/carand.o $(SRC)/carand.c

string: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -o bin/castring.o $(SRC)/castring.c

math: $(HEADERS) $(SOURCES)
	$(CC) -c $(FLAGS) -march=native $(MATH_FLAGS) -o bin/camath.o $(SRC)/camath.c

.PHONY: all benchmark build install uninstall clean opt_folder bin_folder all_static array logging map rand string math