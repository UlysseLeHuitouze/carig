# carig

## Abstract

~~A toy library for my toy projects.~~

A few utilities for C.

Philosophy: Bloat-less, Robust, 30% Comments

Implications: Each .c file (module) is independent of the others, Property-based tested (without shrinking),
Each .h file starts with an abstract of the API.

Written in GNU-C17 (extensions used are mostly Statement Expressions, Function Attributes and some built-in functions).

Features:

- String abstraction
- Hash Map abstraction
- Hash Set abstraction
- Growable Array abstraction
- Ring Buffer abstraction
- Pseudo-random Number generator abstraction
- Logging (no threading) abstraction
- Vector abstraction

Work in progress:

- Heavily challenge PRNG

## Installation

carig can be installed either with the recommended settings, or by benchmarking manually to customize said settings.

#### With the recommended settings:

Simply run the following commands:

```bash
git clone https://gitlab.com/UlysseLeHuitouze/carig.git
cd carig
make && sudo make install
```

#### Benchmarking manually

As of now, benchmarking is only available for carig math module. Benchmarking it will display benchmarking results
for two different variants of carig math module: The intrinsic-free mode and the intrinsic mode.
After re-running the benchmarks if desired, the users will be prompted
to choose one of the two mode. Additionally, the users will be asked if they want to enable intrinsics for
vector normalization regardless of the previous mode (Indeed, compilers are not allowed to optimize calls to `sqrt`).

To benchmark and then install, run the following commands:

```bash
git clone https://gitlab.com/UlysseLeHuitouze/carig.git
cd carig
make benchmark && sudo make install
```

Note that the recommended settings (when launching `make && sudo make install`) are to use intrinsic mode.

Choosing either option will create the headers and
the archive library in `/opt/carig/include` and `/opt/carig/lib` respectively.

## Usage

Compile with the following options in order to use carig and be able to do `#include "carig.h"`:

```
-I/opt/carig/include -lm -L/opt/carig/lib -lcarig
```

### <!> Important <!>

carig math module's API involves numerous architecture-dependent types and
does not feature platform-independent layer yet. Therefore, any code making use of `camath.h` API should be compiled
with the `-march=native` option.

## Uninstallation

To uninstall carig, run the following command from the same carig directory than for the **Installation**:

```bash
make uninstall
```

This will delete the entire `/opt/carig` folder.