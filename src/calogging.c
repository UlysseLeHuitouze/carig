//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "calogging.h"
#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <stdlib.h>







#ifdef CALOGGING_NO_ABORT_ERROR
#define CHECK_TRUE(EXPR)                                        \
    if (!(EXPR)) {                                              \
        fprintf(stderr, "failed to log a message. continuing"); \
        return;                                                 \
    }
#else
#define CHECK_TRUE(EXPR)                                   \
    if (!(EXPR)) {                                         \
        fprintf(stderr, "failed to log a message. fatal"); \
        exit(1);                                           \
    }
#endif



static FILE *info_stream;
static FILE *warn_error_stream;






bool caloginit(const char *filename) {
    if (!filename) {
        info_stream = stdout;
        warn_error_stream = stderr;
        return true;
    }
    return (info_stream = warn_error_stream = fopen(filename, "a")) != NULL;
}


bool calogclose() {
    if (info_stream == stdout)
        return true;
    return !fclose(info_stream);
}


void calogwriteraw(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    vfprintf(info_stream, msg, args);
    va_end(args);
}


void private_info(const char *msg, ...) {
    time_t now;
    time(&now);
    va_list args;
    va_start(args, msg);
    CHECK_TRUE(
            fputs(ctime(&now), info_stream) >= 0 &&
            vfprintf(info_stream, msg, args) >= 0 &&
            fputc('\n', info_stream) >= 0
    )
    va_end(args);
}

void private_error(const char *msg, ...) {
    time_t now;
    time(&now);
    va_list args;
    va_start(args, msg);
    CHECK_TRUE(
            fputs(ctime(&now), warn_error_stream) >= 0 &&
            vfprintf(warn_error_stream, msg, args) >= 0 &&
            fputc('\n', warn_error_stream) >= 0
    )
    va_end(args);
}
