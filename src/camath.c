//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "camath.h"





void caperspective_lh(
        mat4_t dst, double fov, uint32_t width, uint32_t height, double z_near, double z_far) {

    double r_fov = 1.0 / tan(0.5 * caradians(fov));
    double r_z_delta = z_far / (z_far - z_near);

    dst[0][0] = (float)(r_fov * ((double) height / (double) width));
    dst[0][1] = dst[0][2] = dst[0][3] = 0;

    dst[1][1] = (float)r_fov;
    dst[1][0] = dst[1][2] = dst[1][3] = 0;

    dst[2][2] = (float)r_z_delta;
    dst[2][3] = 1;
    dst[2][0] = dst[2][1] = 0;

    dst[3][2] = - (float)(r_z_delta * z_near);
    dst[3][0] = dst[3][1] = dst[3][3] = 0;

}

void caperspective_rh(
        mat4_t dst, double fov, uint32_t width, uint32_t height, double z_near, double z_far) {

    double r_fov = 1.0 / tan(0.5 * caradians(fov));
    double r_z_delta = z_far / (z_far - z_near);

    dst[0][0] = (float)(r_fov * ((double) height / (double) width));
    dst[0][1] = dst[0][2] = dst[0][3] = 0;

    dst[1][1] = (float)r_fov;
    dst[1][0] = dst[1][2] = dst[1][3] = 0;

    dst[2][2] = (float)r_z_delta;
    dst[2][3] = -1;
    dst[2][0] = dst[2][1] = 0;

    dst[3][2] = (float)(r_z_delta * z_near);
    dst[3][0] = dst[3][1] = dst[3][3] = 0;

}


void caperspective_unsafe(mat4_t dst, double reverse_tan_half_fov, double reverse_aspect_ratio) {
    dst[0][0] = (float)(reverse_tan_half_fov * reverse_aspect_ratio);
    dst[1][1] = (float)reverse_tan_half_fov;
}


void calook_at_lh(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir) {

    vec4_t look_dir = cavecnormalize(cam_look - cam_pos);
    vec4_t right_vec = cavecnormalize(caveccross(up_dir, look_dir));
    vec4_t up = caveccross(look_dir, right_vec);

    dst[0][0] = right_vec[0];
    dst[0][1] = up[0];
    dst[0][2] = look_dir[0];
    dst[0][3] = 0;

    dst[1][0] = right_vec[1];
    dst[1][1] = up[1];
    dst[1][2] = look_dir[1];
    dst[1][3] = 0;

    dst[2][0] = right_vec[2];
    dst[2][1] = up[2];
    dst[2][2] = look_dir[2];
    dst[2][3] = 0;

    dst[3][0] = -cavecdot(right_vec, cam_pos);
    dst[3][1] = -cavecdot(up, cam_pos);
    dst[3][2] = -cavecdot(look_dir, cam_pos);
    dst[3][3] = 1;
}


void calook_at_lh_unsafe(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir) {

    vec4_t look_dir = cavecnormalize(cam_look - cam_pos);
    vec4_t right_vec = cavecnormalize(caveccross(up_dir, look_dir));
    vec4_t up = caveccross(look_dir, right_vec);

    dst[0][0] = right_vec[0];
    dst[0][1] = up[0];
    dst[0][2] = look_dir[0];

    dst[1][0] = right_vec[1];
    dst[1][1] = up[1];
    dst[1][2] = look_dir[1];

    dst[2][0] = right_vec[2];
    dst[2][1] = up[2];
    dst[2][2] = look_dir[2];

    dst[3][0] = -cavecdot(right_vec, cam_pos);
    dst[3][1] = -cavecdot(up, cam_pos);
    dst[3][2] = -cavecdot(look_dir, cam_pos);
}


void calook_at_rh(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir) {

    vec4_t look_dir = cavecnormalize(cam_look - cam_pos);
    vec4_t right_vec = cavecnormalize(caveccross(look_dir, up_dir));
    vec4_t up = caveccross(right_vec, look_dir);

    dst[0][0] = right_vec[0];
    dst[0][1] = up[0];
    dst[0][2] = -look_dir[0];
    dst[0][3] = 0;

    dst[1][0] = right_vec[1];
    dst[1][1] = up[1];
    dst[1][2] = -look_dir[1];
    dst[1][3] = 0;

    dst[2][0] = right_vec[2];
    dst[2][1] = up[2];
    dst[2][2] = -look_dir[2];
    dst[2][3] = 0;

    dst[3][0] = -cavecdot(right_vec, cam_pos);
    dst[3][1] = -cavecdot(up, cam_pos);
    dst[3][2] = cavecdot(look_dir, cam_pos);
    dst[3][3] = 1;
}


void calook_at_rh_unsafe(mat4_t dst, vec4_t cam_pos, vec4_t cam_look, vec4_t up_dir) {

    vec4_t look_dir = cavecnormalize(cam_look - cam_pos);
    vec4_t right_vec = cavecnormalize(caveccross(look_dir, up_dir));
    vec4_t up = caveccross(right_vec, look_dir);

    dst[0][0] = right_vec[0];
    dst[0][1] = up[0];
    dst[0][2] = -look_dir[0];

    dst[1][0] = right_vec[1];
    dst[1][1] = up[1];
    dst[1][2] = -look_dir[1];

    dst[2][0] = right_vec[2];
    dst[2][1] = up[2];
    dst[2][2] = -look_dir[2];

    dst[3][0] = -cavecdot(right_vec, cam_pos);
    dst[3][1] = -cavecdot(up, cam_pos);
    dst[3][2] = cavecdot(look_dir, cam_pos);
}
