//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "carand.h"
#include "math.h"







#define NEXT_SEED(seed) ((seed * 25214903917ULL + 11ULL) & 0x0000ffffffffffffULL)

void carand_setseed(CARandom *rand, uint64_t seed) {
    rand->seed = (seed ^ 25214903917ULL) & 0x0000ffffffffffffULL;
}


uint32_t carandi_bound(CARandom *rand, uint32_t ceiling) {

#define NEXT_INT_SHIFT_RIGHT_ONE (( rand->seed = NEXT_SEED(rand->seed) ) >> 17)

    uint64_t res = NEXT_INT_SHIFT_RIGHT_ONE;
    const uint64_t ceiling_minus_one = ceiling - 1;

    if (!( ceiling & ceiling_minus_one ))
        res = (ceiling * res) >> 31;
    else
        for (uint64_t i = res;
            (int64_t)(i - (res = i % ceiling) + ceiling_minus_one) < 0;
            i = NEXT_INT_SHIFT_RIGHT_ONE
                    );

#undef NEXT_INT_SHIFT_RIGHT_ONE

    return res;
}


uint32_t carandi(CARandom *rand) {
    return ( rand->seed = NEXT_SEED(rand->seed) ) >> 16;
}


uint64_t carandl_bound(CARandom *rand, uint64_t ceiling) {

#define NEXT_LONG_SHIFT_RIGHT_ONE_BIS(SEED) ({                                        \
    uint64_t CA_UNIQUE_NAME(msb, SEED) = NEXT_SEED(rand->seed);                          \
    uint64_t CA_UNIQUE_NAME(lsb, SEED) = rand->seed = NEXT_SEED(CA_UNIQUE_NAME(msb, SEED)); \
                                                                                      \
    ( (CA_UNIQUE_NAME(msb, SEED) & 0x0000ffffffff0000ULL) << 15 ) |                      \
    ( (CA_UNIQUE_NAME(lsb, SEED) & 0x0000ffffffff0000ULL) >> 17 );                       \
})

#define NEXT_LONG_SHIFT_RIGHT_ONE NEXT_LONG_SHIFT_RIGHT_ONE_BIS(__COUNTER__)

    uint64_t res = NEXT_LONG_SHIFT_RIGHT_ONE;

    const uint64_t ceiling_minus_one = ceiling - 1;

    if (!( ceiling & ceiling_minus_one ))
        res &= ceiling_minus_one;
    else
        for (uint64_t i = res;
            (int64_t)(i - (res = i % ceiling) + ceiling_minus_one) < 0;
            i = NEXT_LONG_SHIFT_RIGHT_ONE
                    );

#undef NEXT_LONG_SHIFT_RIGHT_ONE
#undef NEXT_LONG_SHIFT_RIGHT_ONE_BIS

    return res;
}


uint64_t carandl(CARandom *rand) {
    uint64_t msb = NEXT_SEED(rand->seed);
    uint64_t lsb = rand->seed = NEXT_SEED(msb);

    return ( (msb & 0x0000ffffffff0000ULL) << 16 ) | ( (lsb & 0x0000ffffffff0000ULL) >> 16 );
}


bool carandbool(CARandom *rand) {
    return ( rand->seed = NEXT_SEED(rand->seed) ) >> 47;
}


float carandf(CARandom *rand) {
    return 0.000000059604644775390625f * (float)(uint32_t)( ( rand->seed = NEXT_SEED(rand->seed) ) >> 24 );
}


float carandf_bound(CARandom *rand, float ceiling) {
    float res = carandf(rand) * ceiling;
    if (res >= ceiling)
        res = nextafterf(ceiling, -INFINITY);

    return res;
}


double carandd(CARandom *rand) {
    uint64_t msb = NEXT_SEED(rand->seed);
    uint64_t lsb = rand->seed = NEXT_SEED(msb);

    return  0.00000000000000011102230246251565404236316680908203125 * (double)(
            ( (msb & 0xffffffffffc00000ULL) << 5 ) +
            ( (lsb >> 21) & 0x00000000ffffffffULL )
    );
}


double carandd_bound(CARandom *rand, double ceiling) {
    double res = carandd(rand) * ceiling;
    if (res >= ceiling)
        res = nextafter(ceiling, -INFINITY);

    return res;
}


double carandgaussian(CARandom *rand, double mean, double deviation) {
    double res;
    const double deviate = rand->gaussian_deviate;
    if (isnan(deviate)) {

        double v1, v2, distance_squared;

        do {
            v1 = 2.0 * carandd(rand) - 1;
            v2 = 2.0 * carandd(rand) - 1;
            distance_squared = v1 * v1 + v2 * v2;
        } while( !distance_squared || distance_squared >= 1);

        double polar = sqrt(-2 * log(distance_squared) / distance_squared);
        rand->gaussian_deviate = v2 * polar;

        res = mean + v1 * polar * deviation;

    } else {
        res = mean + deviation * deviate;
        rand->gaussian_deviate = nan("");
    }
    return res;
}
