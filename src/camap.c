//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include <string.h>
#include <stdalign.h>
#include "camap.h"




#ifndef CAMAP_CALLOC
#   include <stdlib.h>
#   define CAMAP_CALLOC calloc
#endif

#ifndef CAMAP_FREE
#   include <stdlib.h>
#   define CAMAP_FREE free
#endif

// || Integral set & map


static uint32_t browse_integer(const uint32_t *elements, const uint32_t element_count, uint32_t element){
    uint32_t i = element % element_count;
    for (uint32_t el = elements[i]; el && el != element;) {
        if (++i == element_count)
            i = 0;
        el = elements[i];
    }
    return i;
}



// integral map




BoundIntMap ca_bound_intmap_dynamic_of(size_t element_count) {
#define ALIGN_REQ (alignof(void*)/alignof(uint32_t))
    size_t cap = 2 * element_count + 1;
    size_t keySizeAndPadding = sizeof(uint32_t) * ({
        size_t mod = cap % ALIGN_REQ;
        mod != 0 ? cap + ALIGN_REQ - mod : cap;
    });
    void *mem = CAMAP_CALLOC(1, keySizeAndPadding + sizeof(void*) * cap);
    return (BoundIntMap) {
            .keys = mem,
            .values = (void**)((char*)mem + keySizeAndPadding),
            .capacity = cap
    };
#undef ALIGN_REQ
}


void ca_bound_intmap_free(BoundIntMap *map) {
    CAMAP_FREE(map->keys);
}


void* caimapput(BoundIntMap *map, uint32_t key, void *value) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    void *old = map->values[i];
    map->keys[i] = key;
    map->values[i] = value;
    map->count += old == NULL;
    return old;
}


void* caimapget(BoundIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    return map->values[i];  // thanks to strong constraints, we bypass null-checking map->keys[i]. See BoundHashMap doc
}


void* caimaprmv(BoundIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    void *old = map->values[i];
    map->keys[i] = 0;
    map->values[i] = NULL;
    map->count -= old != NULL;
    return old;
}


void caimaprst(BoundIntMap *map) {
    map->count = 0;
    memset(map->keys, 0, sizeof(*map->keys) * map->capacity);
    memset(map->values, 0, sizeof(*map->values) * map->capacity);
}


void caimaparr(BoundIntMap *map, uint32_t *key_dst, void **value_dst) {
    int c = 0;
    for (size_t i = 0; i < map->capacity; i++) {
        uint32_t key = map->keys[i];
        if (key) {
            key_dst[c] = key - 1;
            value_dst[c++] = map->values[i];
        }
    }
}



// integral to integral map


static uint32_t browse_iimap(BoundIntIntMap *map, const uint32_t element){
    uint32_t i = 2 * (element % map->capacity);
    for (uint32_t el = map->mem[i]; el && el != element;) {
        i += 2;
        if (i == 2 * map->capacity)
            i = 0;
        el = map->mem[i];
    }
    return i;
}


BoundIntIntMap ca_bound_intintmap_dynamic_of(size_t element_count) {
    size_t cap = 2 * element_count + 1;
    return (BoundIntIntMap) {
        .mem = CAMAP_CALLOC(2 * cap, sizeof(uint32_t)),
        .capacity = cap
    };
}


void ca_bound_intintmap_free(BoundIntIntMap *map) {
    CAMAP_FREE(map->mem);
}


uint32_t caiimapput(BoundIntIntMap *map, uint32_t key, uint32_t value) {
    key++;

    uint32_t i = browse_iimap(map, key);

    uint32_t old = map->mem[i + 1];
    map->mem[i] = key;
    map->mem[i + 1] = value + 1;
    map->count += old == 0;
    return old - 1;
}


uint32_t caiimapget(BoundIntIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_iimap(map, key);

    // thanks to strong constraints, map->values[i] is well-defined. See BoundHashMap doc
    // so, we don't need to 0-check map->keys[i]
    return map->mem[i + 1] - 1;
}


uint32_t caiimaprmv(BoundIntIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_iimap(map, key);

    uint32_t old = map->mem[i + 1];
    map->mem[i] = 0;
    map->mem[i + 1] = 0;
    map->count -= old != 0;
    return old - 1;
}


void caiimaprst(BoundIntIntMap *map) {
    map->count = 0;
    memset(map->mem, 0, sizeof(*map->mem) * 2 * map->capacity);
}


void caiimaparr(BoundIntIntMap *map, uint32_t *restrict key_dst, uint32_t *restrict value_dst) {
    int c = 0;
    for (size_t i = 0; i < map->capacity; i++) {
        uint32_t key = map->mem[2 * i];
        if (key) {
            key_dst[c] = key - 1;
            value_dst[c++] = map->mem[2 * i + 1] - 1;
        }
    }
}




// integral set



BoundIntSet ca_bound_intset_dynamic_of(size_t element_count) {
    return (BoundIntSet) {
        .elements = CAMAP_CALLOC(2 * element_count + 1, sizeof(uint32_t)),
        .capacity = 2 * element_count + 1
    };
}


void ca_bound_intset_free(BoundIntSet *set) {
    CAMAP_FREE(set->elements);
}


bool caisetput(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);

    bool had = set->elements[i];
    set->elements[i] = element;
    set->count += !had;
    return had;
}


bool caisethas(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);
    return set->elements[i];
}


bool caisetrmv(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);

    bool had = set->elements[i];
    set->elements[i] = 0;
    set->count -= had;
    return had;
}


void caisetrst(BoundIntSet *set) {
    set->count = 0;
    memset(set->elements, 0, sizeof(*set->elements) * set->capacity);
}


void caisetarr(BoundIntSet *set, uint32_t *dst) {
    int c = 0;
    for (size_t i = 0; i < set->capacity; i++) {
        uint32_t el = set->elements[i];
        if (el)
            dst[c++] = el - 1;
    }
}





// || Generic set & map


static uint32_t browse(
        void **elements,
        const uint32_t element_count,
        const void *element,
        const uint32_t hash,
        int (*compareFunctionPtr)(const void*, const void*))
{
    uint32_t i = hash % element_count;
    for (void *el = elements[i]; el != NULL && compareFunctionPtr(el, element) != 0;) {
        if (++i == element_count)
            i = 0;
        el = elements[i];
    }
    return i;
}



// generic map


static uint32_t browse_hmap(BoundHashMap *map, const uint32_t hash, const void *element)
{
    uint32_t i = 2 * (hash % map->capacity);
    for (void *el = map->mem[i]; el != NULL && map->compareFunctionPtr(el, element) != 0;) {
        i += 2;
        if (i == 2 * map->capacity)
            i = 0;
        el = map->mem[i];
    }
    return i;
}


BoundHashMap ca_bound_hashmap_dynamic_of(
        size_t element_count,
        int (*compare_func_ptr)(const void*, const void*)
) {
    size_t cap = 2 * element_count + 1;
    return (BoundHashMap) {
        .mem = CAMAP_CALLOC(2 * cap, sizeof(void*)),
        .compareFunctionPtr = compare_func_ptr,
        .capacity = cap
    };
}


void ca_bound_hashmap_free(BoundHashMap *map) {
    CAMAP_FREE(map->mem);
}


void* cahmapput(BoundHashMap *map, const uint32_t key_hash, void *key, void *value) {
    uint32_t i = browse_hmap(map, key_hash, key);

    void *old = map->mem[i + 1];
    map->mem[i] = key;
    map->mem[i + 1] = value;
    map->count += old == NULL;
    return old;
}


void* cahmapget(BoundHashMap *map, const uint32_t key_hash, void *key) {
    uint32_t i = browse_hmap(map, key_hash, key);

    return map->mem[i + 1]; // thanks to strong constraints, we bypass null-checking map->keys[i]. See BoundHashMap doc
}


void* cahmaprmv(BoundHashMap *map, const uint32_t key_hash, void *key) {
    uint32_t i = browse_hmap(map, key_hash, key);

    void *old = map->mem[i + 1];
    map->mem[i] = NULL;
    map->mem[i + 1] = NULL;
    map->count -= old != NULL;
    return old;
}


void cahmaprst(BoundHashMap *map) {
    map->count = 0;
    memset(map->mem, 0, sizeof(*map->mem) * 2 * map->capacity);
}


void cahmaparr(BoundHashMap *map, void **key_dst, void **value_dst) {
    int c = 0;
    for (size_t i = 0; i < map->capacity; i++) {
        void *key = map->mem[2 * i];
        if (key) {
            key_dst[c] = key;
            value_dst[c++] = map->mem[2 * i + 1];
        }
    }
}




// generic set



BoundHashSet ca_bound_hashset_dynamic_of(
        size_t element_count,
        int (*compare_func_ptr)(const void*, const void*)
) {
    return (BoundHashSet) {
        .elements = CAMAP_CALLOC(2 * element_count + 1, sizeof(void*)),
        .compareFunctionPtr = compare_func_ptr,
        .capacity = 2 * element_count + 1
    };
}


void ca_bound_hashset_free(BoundHashSet *set) {
    CAMAP_FREE(set->elements);
}



bool cahsetput(BoundHashSet *set, const uint32_t element_hash, void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);

    bool had = set->elements[i];
    set->elements[i] = element;
    set->count += !had;
    return had;
}


bool cahsethas(BoundHashSet *set, const uint32_t element_hash, const void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);
    return set->elements[i];
}


bool cahsetrmv(BoundHashSet *set, uint32_t element_hash, const void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);

    bool had = set->elements[i];
    set->elements[i] = NULL;
    set->count -= had;
    return had;
}


void cahsetrst(BoundHashSet *set) {
    set->count = 0;
    memset(set->elements, 0, sizeof(*set->elements) * set->capacity);
}


void cahsetarr(BoundHashSet *set, void **dst) {
    int c = 0;
    for (size_t i = 0; i < set->capacity; i++) {
        void *el = set->elements[i];
        if (el)
            dst[c++] = el;
    }
}
