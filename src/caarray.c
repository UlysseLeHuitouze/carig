//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "caarray.h"
#include <string.h>





void* private_groWunbounDarraY(UnboundArray *array) {
    size_t cap_factor = array->capacity >> 1;
    size_t new_cap = array->capacity + cap_factor + (!cap_factor);

    void *mem = array->reallocFuncPtr(array->elements, new_cap * array->element_size);
    if (mem == NULL)
        return array->elements;

    array->elements = mem;
    array->capacity = new_cap;
    return NULL;
}


void* private_groWunbounDarraYamounT(UnboundArray *array, size_t new_el_count) {
    size_t cap_factor = array->capacity >> 1;
    size_t cap1 = array->capacity + cap_factor + (!cap_factor);
    size_t cap2 = array->capacity + new_el_count;
    size_t newcap = MAX(cap1, cap2);

    void* mem = array->reallocFuncPtr(array->elements, newcap * array->element_size);
    if (mem == NULL)
        return array->elements;

    array->elements = mem;
    array->capacity = newcap;
    return NULL;
}


void* private_groWunbounDringbuffeR(UnboundRingBuffer *buffer) {
    size_t cap = buffer->capacity_minus_one + 1;
    size_t new_cap = 2 * cap;
    void *mem = buffer->reallocFuncPtr(buffer->elements, new_cap * buffer->element_size);
    if (mem == NULL)
        return buffer->elements;

    if (buffer->count != 0 && buffer->head <= buffer->tail)
        memcpy(
                (char*)mem + buffer->element_size * cap,
                mem,
                buffer->head * buffer->element_size
        );

    buffer->head += cap;
    buffer->elements = mem;
    buffer->capacity_minus_one = new_cap - 1;
    return NULL;
}


void caarrrst(UnboundArray *array) {
    array->count = 0;
}


void cabuffrst(BoundRingBuffer *buffer) {
    buffer->count = 0;
    buffer->tail = buffer->head;
}


void cadbuffrst(UnboundRingBuffer *buffer) {
    buffer->count = 0;
    buffer->tail = buffer->head;
}
