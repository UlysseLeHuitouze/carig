//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "castring.h"
#include <math.h>




#ifndef CASTRING_MALLOC
#   include <stdlib.h>
#   define CASTRING_MALLOC malloc
#endif

#ifndef CASTRING_REALLOC
#   include <stdlib.h>
#   define CASTRING_REALLOC realloc
#endif

#ifndef CASTRING_FREE
#   include <stdlib.h>
#   define CASTRING_FREE free
#endif



char *ca_str_to_CStr(string *s) {
    s->chars[s->len] = 0;
    return s->chars;
}


uint32_t castrhash(size_t begin, string *s) {
    uint32_t hash = 0;
    for (size_t i = begin; i < s->len; i++)
        hash = 31 * hash + s->chars[i];

    return hash;
}

uint32_t castrhash_unsafe(size_t begin, const char *s) {
    uint32_t hash = 0;
    char c;
    s += begin;
    while ((c = *s++))
        hash = 31 * hash + c;
    return hash;
}


void castrconcat(string *restrict result, string *restrict s1, string *restrict s2) {
    result->len = s1->len + s2->len;
    memcpy(result->chars, s1->chars, sizeof(*s1->chars) * s1->len);
    memcpy(result->chars + s1->len, s2->chars, sizeof(*s2->chars) * s2->len);
    result->chars[result->len] = 0;
}


int castrcmp(string *s1, string *s2) {
    return strncmp(s1->chars, s2->chars, MAX(s1->len, s2->len));
}

const char* castrsubstr(string *restrict s, string *restrict sub) {
    return sub->len ? strstr(s->chars, sub->chars) : s->chars;
}



static bool growBuffer(StringBuffer *buffer, size_t new_el_count) {
    size_t cap_factor = buffer->capacity >> 1;
    size_t cap1 = buffer->capacity + cap_factor + (!cap_factor);
    size_t cap2 = buffer->capacity + new_el_count;
    size_t newcap = MAX(cap1, cap2);

    void* mem = CASTRING_REALLOC(buffer->elements, sizeof(char) * newcap);
    if (mem == NULL)
        CASTRING_FREE(buffer->elements);

    buffer->elements = mem;
    buffer->capacity = newcap;
    return mem != NULL;
}



StringBuffer ca_string_buffer_of(size_t initialCapacity) {
    return (StringBuffer){
        .elements = CASTRING_MALLOC(sizeof(char) * initialCapacity),
        .capacity = initialCapacity
    };
}


void ca_string_buffer_free(StringBuffer *buffer) {
    CASTRING_FREE(buffer->elements);
}


char castrbufpop(StringBuffer *buffer) {
    return buffer->elements[--buffer->count];
}


bool castrbufpush(StringBuffer *buffer, char c) {
    bool success = true;
    if (buffer->count >= buffer->capacity)
        success = growBuffer(buffer, 1);
    buffer->elements[buffer->count++] = c;
    return success;
}


bool castrbufpush_all(StringBuffer *buffer, string *restrict s) {
    bool success = true;
    if (buffer->count + s->len >= buffer->capacity)
        success = growBuffer(buffer, s->len);
    memcpy(buffer->elements + buffer->count, s->chars, sizeof(char) * s->len);
    buffer->count += s->len;
    return success;
}


bool castrbufpush_int(StringBuffer *buffer, uint32_t i) {
    char c[10];

    int j = 0;
    for (; i && j < 10; j++) {
        c[9 - j] = (char)('0' + i % 10);
        i /= 10;
    }
    return castrbufpush_all(buffer, &(string){.chars = c + (10 - j), .len = j});
}


bool castrbufpush_bool(StringBuffer *buffer, bool b) {
    string s = {
            .chars = b ? "true" : "false",
            .len = 5 - b
    };
    return castrbufpush_all(buffer, &s);
}


#define FLOAT_MAX_DIGIT 32
bool castrbufpush_float(StringBuffer *buffer, float f, int frac_digits) {

    if (isnan(f))
        return castrbufpush_all(buffer, &(string){
            .chars = "NaN",
            .len = 3
        });
    else if (isinf(f)) {
        bool neg = signbit(f);
        return castrbufpush_all(buffer, &(string) {
                .chars = neg ? "-INF" : "INF",
                .len = 3 + neg
        });
    } else {
        frac_digits = MIN(frac_digits, FLOAT_MAX_DIGIT);

        if (signbit(f) && !castrbufpush(buffer, '-'))
            return false;


        float int_partf = 0;
        long frac_part =  labs((long)lrintf( modff(f, &int_partf) * powf(10, (float)frac_digits) ));
        long int_part = labs((long)int_partf);

        char c[FLOAT_MAX_DIGIT];


        // integral part of  f

        int i = 0;
        for (; int_part && i < FLOAT_MAX_DIGIT; i++) {
            c[FLOAT_MAX_DIGIT - 1 - i] = (char)('0' + int_part % 10);
            int_part /= 10;
        }


        if (
                !castrbufpush_all(buffer, &(string){.chars = c + (FLOAT_MAX_DIGIT - i), .len = i}) ||
                !castrbufpush(buffer, '.')
        )
            return false;


        // fractional part of f

        i = 0;
        for (; frac_part && i < frac_digits; i++) {
            c[FLOAT_MAX_DIGIT - 1 - i] = (char)('0' + frac_part % 10);
            frac_part /= 10;
        }

        for (; i < frac_digits; i++)
            c[FLOAT_MAX_DIGIT - 1 - i] = '0';


        return castrbufpush_all(buffer, &(string){.chars = c + (FLOAT_MAX_DIGIT - i), .len = i});
    }
}
