//
// Created by Ulysse Le Huitouze on 17/03/2024.
//

#include "camath.h"
#include "carand.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <inttypes.h>
#undef assert

#ifndef TEST_MATH_TURNS
#define TEST_MATH_TURNS 10000
#endif

#define assert(COND, MSG, ...) ({ \
    if (!(COND)) {                \
        printf(MSG "\n--- SEED %" PRIu64 "---\n", ##__VA_ARGS__, seed); \
        fflush(stdout);           \
        exit(1);                  \
    }                             \
    {}                            \
})

#define dummy()


static float abs_f(float f) {
    return f > 0 ? f : -f;
}

static double abs_d(double d) {
    return d > 0 ? d : -d;
}

static bool approxEqual_f(float a, float b) {
    return b == 0 ? a == 0 : abs_f((a - b) / b) < 0.05f;
}

static bool approxEqual_d(double a, double b) {
    return b == 0 ? a == 0 : abs_d((a - b) / b) < 0.0001;
}

static void vecTest();


static CARandom rng;
static uint64_t seed;

int main() {
    carand_setseed(&rng, seed = time(NULL));
    
    puts("Testing vectors...\n");
    vecTest();
    
    puts("Success!\n");

    return 0;
}



static void vecTest() {

    // ivec2_t
    puts("[ivec2_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        int32_t x1 = (int32_t)carandi(&rng);
        int32_t y1 = (int32_t)carandi(&rng);

        int32_t x2 = (int32_t)carandi(&rng);
        int32_t y2 = (int32_t)carandi(&rng);

        int32_t d1 = cavecdot(ivec2(x1, y1), ivec2(x2, y2));
        int32_t d2 = x1 * x2 + y1 * y2;
        assert(d1 == d2, "Faulty cavecdot. Expected %d, got %d", d1, d2);

        int32_t s1 = cavecsum(ivec2(x1, y1));
        int32_t s2 = x1 + y1;
        assert(s1 == s2, "Faulty cavecsum. Expected %d, got %d", s1, s2);

        int32_t x3 = (int32_t)carandi(&rng);
        int32_t y3 = (int32_t)carandi(&rng);

        ivec2_t m1 = cavecmix(ivec2(x1, y1), ivec2(x2, y2), ivec2(x3, y3));
        assert(
                m1[0] == x1 * (1 - x3) + x2 * x3 &&
                m1[1] == y1 * (1 - y3) + y2 * y3,
                "Faulty cavecmix");
    }


    // ivec4_t
    puts("[ivec4_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        int32_t x1 = (int32_t)carandi(&rng);
        int32_t y1 = (int32_t)carandi(&rng);
        int32_t z1 = (int32_t)carandi(&rng);
        int32_t w1 = (int32_t)carandi(&rng);

        int32_t x2 = (int32_t)carandi(&rng);
        int32_t y2 = (int32_t)carandi(&rng);
        int32_t z2 = (int32_t)carandi(&rng);
        int32_t w2 = (int32_t)carandi(&rng);

        int32_t d1 = cavecdot(ivec4(x1, y1, z1, w1), ivec4(x2, y2, z2, w2));
        int32_t d2 = x1 * x2 + y1 * y2 + z1 * z2 + w1 * w2;
        assert(d1 == d2, "Faulty cavecdot. Expected %d, got %d", d1, d2);

        int32_t s1 = cavecsum(ivec4(x1, y1, z1, w1));
        int32_t s2 = x1 + y1 + z1 + w1;
        assert(s1 == s2, "Faulty cavecsum. Expected %d, got %d", s1, s2);

        ivec4_t c1 = caveccross(ivec4(x1, y1, z1, w1), ivec4(x2, y2, z2, w2));
        assert(
                c1[0] == y1 * z2 - z1 * y2 &&
                c1[1] == z1 * x2 - x1 * z2 &&
                c1[2] == x1 * y2 - y1 * x2,
                "Faulty caveccross");

        int32_t x3 = (int32_t)carandi(&rng);
        int32_t y3 = (int32_t)carandi(&rng);
        int32_t z3 = (int32_t)carandi(&rng);
        int32_t w3 = (int32_t)carandi(&rng);

        ivec4_t m1 = cavecmix(ivec4(x1, y1, z1, w1), ivec4(x2, y2, z2, w2), ivec4(x3, y3, z3, w3));
        assert(
                m1[0] == x1 * (1 - x3) + x2 * x3 &&
                m1[1] == y1 * (1 - y3) + y2 * y3 &&
                m1[2] == z1 * (1 - z3) + z2 * z3 &&
                m1[3] == w1 * (1 - w3) + w2 * w3,
                "Faulty cavecmix");
    }



    // lvec2_t
    puts("[lvec2_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        int64_t x1 = (int64_t)carandl(&rng);
        int64_t y1 = (int64_t)carandl(&rng);

        int64_t x2 = (int64_t)carandl(&rng);
        int64_t y2 = (int64_t)carandl(&rng);

        int64_t d1 = cavecdot(lvec2(x1, y1), lvec2(x2, y2));
        int64_t d2 = x1 * x2 + y1 * y2;
        assert(d1 == d2, "Faulty cavecdot. Expected %" PRIi64 ", got %" PRIi64, d1, d2);

        int64_t s1 = cavecsum(lvec2(x1, y1));
        int64_t s2 = x1 + y1;
        assert(s1 == s2, "Faulty cavecsum. Expected %" PRIi64 ", got %" PRIi64, s1, s2);

        int64_t x3 = (int64_t)carandl(&rng);
        int64_t y3 = (int64_t)carandl(&rng);

        lvec2_t m1 = cavecmix(lvec2(x1, y1), lvec2(x2, y2), lvec2(x3, y3));
        assert(
                m1[0] == x1 * (1 - x3) + x2 * x3 &&
                m1[1] == y1 * (1 - y3) + y2 * y3,
                "Faulty cavecmix");
    }


    // lvec4_t
    puts("[lvec4_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        int64_t x1 = (int64_t)carandl(&rng);
        int64_t y1 = (int64_t)carandl(&rng);
        int64_t z1 = (int64_t)carandl(&rng);
        int64_t w1 = (int64_t)carandl(&rng);

        int64_t x2 = (int64_t)carandl(&rng);
        int64_t y2 = (int64_t)carandl(&rng);
        int64_t z2 = (int64_t)carandl(&rng);
        int64_t w2 = (int64_t)carandl(&rng);

        int64_t d1 = cavecdot(lvec4(x1, y1, z1, w1), lvec4(x2, y2, z2, w2));
        int64_t d2 = x1 * x2 + y1 * y2 + z1 * z2 + w1 * w2;
        assert(d1 == d2, "Faulty cavecdot. Expected %" PRIi64 ", got %" PRIi64, d1, d2);

        int64_t s1 = cavecsum(lvec4(x1, y1, z1, w1));
        int64_t s2 = x1 + y1 + z1 + w1;
        assert(s1 == s2, "Faulty cavecsum. Expected %" PRIi64 ", got %" PRIi64, s1, s2);

        lvec4_t c1 = caveccross(lvec4(x1, y1, z1, w1), lvec4(x2, y2, z2, w2));
        assert(
                c1[0] == y1 * z2 - z1 * y2 &&
                c1[1] == z1 * x2 - x1 * z2 &&
                c1[2] == x1 * y2 - y1 * x2,
                "Faulty caveccross");

        int64_t x3 = (int64_t)carandl(&rng);
        int64_t y3 = (int64_t)carandl(&rng);
        int64_t z3 = (int64_t)carandl(&rng);
        int64_t w3 = (int64_t)carandl(&rng);

        lvec4_t m1 = cavecmix(lvec4(x1, y1, z1, w1), lvec4(x2, y2, z2, w2), lvec4(x3, y3, z3, w3));
        assert(
                m1[0] == x1 * (1 - x3) + x2 * x3 &&
                m1[1] == y1 * (1 - y3) + y2 * y3 &&
                m1[2] == z1 * (1 - z3) + z2 * z3 &&
                m1[3] == w1 * (1 - w3) + w2 * w3,
                "Faulty cavecmix");
    }


    // vec2_t
    puts("[vec2_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        float x1 = carandf_bound(&rng, 1000000.f);
        float y1 = carandf_bound(&rng, 1000000.f);

        float x2 = carandf_bound(&rng, 1000000.f);
        float y2 = carandf_bound(&rng, 1000000.f);

        float d1 = cavecdot(vec2(x1, y1), vec2(x2, y2));
        float d2 = x1 * x2 + y1 * y2;
        assert(approxEqual_f(d1, d2), "Faulty cavecdot. Expected %f, got %f", d1, d2);

        float s1 = cavecsum(vec2(x1, y1));
        float s2 = x1 + y1;
        assert(approxEqual_f(s1, s2), "Faulty cavecsum. Expected %f, got %f", s1, s2);

        float x3 = carandf_bound(&rng, 1000000.f);
        float y3 = carandf_bound(&rng, 1000000.f);

        vec2_t m1 = cavecmix(vec2(x1, y1), vec2(x2, y2), vec2(x3, y3));
        assert(approxEqual_f(m1[0], x1 * (1 - x3) + x2 * x3) && approxEqual_f(m1[1], y1 * (1 - y3) + y2 * y3),
               "Faulty cavecmix");

        vec2_t n1 = cavecnormalize(vec2(x1, y1));
        float nn1 = cavecdot(n1, n1);
        assert(approxEqual_f(nn1, 1.f), "Faulty cavecnormalize");
    }


    // vec4_t
    puts("[vec4_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        float x1 = carandf_bound(&rng, 1000000.f);
        float y1 = carandf_bound(&rng, 1000000.f);
        float z1 = carandf_bound(&rng, 1000000.f);
        float w1 = carandf_bound(&rng, 1000000.f);

        float x2 = carandf_bound(&rng, 1000000.f);
        float y2 = carandf_bound(&rng, 1000000.f);
        float z2 = carandf_bound(&rng, 1000000.f);
        float w2 = carandf_bound(&rng, 1000000.f);

        float d1 = cavecdot(vec4(x1, y1, z1, w1), vec4(x2, y2, z2, w2));
        float d2 = x1 * x2 + y1 * y2 + z1 * z2 + w1 * w2;
        assert(approxEqual_f(d1, d2), "Faulty cavecdot implementation. Expected %f, got %f", d1, d2);

        float s1 = cavecsum(ivec4(x1, y1, z1, w1));
        float s2 = x1 + y1 + z1 + w1;
        assert(approxEqual_f(s1, s2), "Faulty cavecsum. Expected %f, got %f", s1, s2);

        vec4_t c1 = caveccross(vec4(x1, y1, z1, w1), vec4(x2, y2, z2, w2));
        assert(
                approxEqual_f(c1[0], y1 * z2 - z1 * y2) &&
                approxEqual_f(c1[1], z1 * x2 - x1 * z2) &&
                approxEqual_f(c1[2], x1 * y2 - y1 * x2),
                "Faulty caveccross");

        float x3 = carandf_bound(&rng, 1000000.f);
        float y3 = carandf_bound(&rng, 1000000.f);
        float z3 = carandf_bound(&rng, 1000000.f);
        float w3 = carandf_bound(&rng, 1000000.f);

        vec4_t m1 = cavecmix(vec4(x1, y1, z1, w1), vec4(x2, y2, z2, w2), vec4(x3, y3, z3, w3));
        assert(
                approxEqual_f(m1[0], x1 * (1 - x3) + x2 * x3) &&
                approxEqual_f(m1[1], y1 * (1 - y3) + y2 * y3) &&
                approxEqual_f(m1[2], z1 * (1 - z3) + z2 * z3) &&
                approxEqual_f(m1[3], w1 * (1 - w3) + w2 * w3),
                "Faulty cavecmix");

        vec4_t n1 = cavecnormalize(vec4(x1, y1, z1, w1));
        float nn1 = cavecdot(n1, n1);
        assert(approxEqual_f(nn1, 1.f), "Faulty cavecnormalize");
    }


    // dvec2_t
    puts("[dvec2_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        double x1 = carandd_bound(&rng, 1000000.f);
        double y1 = carandd_bound(&rng, 1000000.f);

        double x2 = carandd_bound(&rng, 1000000.f);
        double y2 = carandd_bound(&rng, 1000000.f);

        double d1 = cavecdot(dvec2(x1, y1), dvec2(x2, y2));
        double d2 = x1 * x2 + y1 * y2;
        assert(approxEqual_d(d1, d2), "Faulty cavecdot implementation. Expected %f, got %f", d1, d2);

        double s1 = cavecsum(vec2(x1, y1));
        double s2 = x1 + y1;
        assert(approxEqual_d(s1, s2), "Faulty cavecsum. Expected %f, got %f", s1, s2);

        double x3 = carandd_bound(&rng, 1000000.f);
        double y3 = carandd_bound(&rng, 1000000.f);

        dvec2_t m1 = cavecmix(dvec2(x1, y1), dvec2(x2, y2), dvec2(x3, y3));
        assert(approxEqual_d(m1[0], x1 * (1 - x3) + x2 * x3) && approxEqual_d(m1[1], y1 * (1 - y3) + y2 * y3),
               "Faulty cavecmix");

        dvec2_t n1 = cavecnormalize(dvec2(x1, y1));
        double nn1 = cavecdot(n1, n1);
        assert(nn1 == 0 ? false : nn1 > 0.99 && nn1 < 1.01, "Faulty cavecnormalize");
    }


    // dvec4_t
    puts("[dvec4_t]\n");
    for (int _ = 0; _ < TEST_MATH_TURNS; _++) {
        double x1 = carandd_bound(&rng, 1000000.f);
        double y1 = carandd_bound(&rng, 1000000.f);
        double z1 = carandd_bound(&rng, 1000000.f);
        double w1 = carandd_bound(&rng, 1000000.f);

        double x2 = carandd_bound(&rng, 1000000.f);
        double y2 = carandd_bound(&rng, 1000000.f);
        double z2 = carandd_bound(&rng, 1000000.f);
        double w2 = carandd_bound(&rng, 1000000.f);

        double d1 = cavecdot(dvec4(x1, y1, z1, w1), dvec4(x2, y2, z2, w2));
        double d2 = x1 * x2 + y1 * y2 + z1 * z2 + w1 * w2;
        assert(approxEqual_d(d1, d2), "Faulty cavecdot implementation. Expected %f, got %f", d1, d2);

        double s1 = cavecsum(ivec4(x1, y1, z1, w1));
        double s2 = x1 + y1 + z1 + w1;
        assert(approxEqual_d(s1, s2), "Faulty cavecsum. Expected %f, got %f", s1, s2);

        dvec4_t c1 = caveccross(dvec4(x1, y1, z1, w1), dvec4(x2, y2, z2, w2));
        assert(
                approxEqual_d(c1[0], y1 * z2 - z1 * y2) &&
                approxEqual_d(c1[1], z1 * x2 - x1 * z2) &&
                approxEqual_d(c1[2], x1 * y2 - y1 * x2),
                "Faulty caveccross");

        double x3 = carandd_bound(&rng, 1000000.f);
        double y3 = carandd_bound(&rng, 1000000.f);
        double z3 = carandd_bound(&rng, 1000000.f);
        double w3 = carandd_bound(&rng, 1000000.f);

        dvec4_t m1 = cavecmix(dvec4(x1, y1, z1, w1), dvec4(x2, y2, z2, w2), dvec4(x3, y3, z3, w3));
        assert(
                approxEqual_d(m1[0], x1 * (1 - x3) + x2 * x3) &&
                approxEqual_d(m1[1], y1 * (1 - y3) + y2 * y3) &&
                approxEqual_d(m1[2], z1 * (1 - z3) + z2 * z3) &&
                approxEqual_d(m1[3], w1 * (1 - w3) + w2 * w3),
                "Faulty cavecmix");

        dvec4_t n1 = cavecnormalize(dvec4(x1, y1, z1, w1));
        double nn1 = cavecdot(n1, n1);
        assert(nn1 == 0 ? false : nn1 > 0.99 && nn1 < 1.01, "Faulty cavecnormalize");
    }
}
