//
// Created by Ulysse Le Huitouze on 09/03/2024.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include "castring.h"
#include "carand.h"


#ifndef TEST_STRING_TURNS
#define TEST_STRING_TURNS 10000
#endif

#define assert(COND, MSG, ...) ({ \
    if (!(COND)) {                \
        printf(MSG "\n--- SEED %" PRIu64 "---\n", ##__VA_ARGS__, seed); \
        fflush(stdout);           \
        exit(1);                  \
    }                             \
    {}                            \
})

static void stringTest();
static void stringBufferTest();

static CARandom rng;
static uint64_t seed;


int main() {
    carand_setseed(&rng, seed = time(NULL));

    puts("Testing string...\n");
    stringTest();

    puts("Testing StringBuffer...\n");
    stringBufferTest();

    puts("Success!\n");
}






static void stringTest() {
#define STACK_SIZE 10

    for (int _ = 0; _ < TEST_STRING_TURNS; _++) {
        char mem[STACK_SIZE][50];
        int sp = 0;

#define RAND_STRING() ({ \
    if (sp >= STACK_SIZE) { \
        puts("overflow\n"); \
        exit(1);\
    }                    \
                         \
    int _strlen = (int)carandi_bound(&rng, 50); \
    for (int i = 0; i < _strlen; i++)      \
        mem[sp][i] = (char)((int)carandi_bound(&rng, 60) + 'A');\
    mem[sp][_strlen] = 0;                   \
    mem[sp++];\
})


        // lemma 1: forall s i. 0 <= i < strlen(s) -> castrhash(i, ca_str_of(s)) = castrhash_unsafe(i, s)
        {
            char *s = RAND_STRING();
            uint32_t i = carandi_bound(&rng, strlen(s));
            string tmp = ca_str_of(s);
            uint32_t h1 = castrhash(i, &tmp);
            uint32_t h2 = castrhash_unsafe(i, s);
            assert(h1 == h2,
                   "lemma 1: forall s i. 0 <= i < strlen(s) ->"
                   " castrhash(i, ca_str_of(s)) = castrhash_unsafe(i, s) falsified for s: %s", s);
            sp--;
        }

        // lemma 2: forall s1 s2 i j. 0 <= i < len s1 ^ 0 <= j < len s2 ->
        // (castrconcat s1 s2)[i] = s1[i] ^ (castrconcat s1 s2)[(len s1) + j] = s2[j]
        {
            string s1 = ca_str_of(RAND_STRING());
            string s2 = ca_str_of(RAND_STRING());

            string tmp = {
                    .len = s1.len + s2.len,
                    .chars = (char[100]){}
            };

            castrconcat(&tmp, &s1, &s2);
            for (size_t i = 0; i < s1.len; i++) {
                assert(tmp.chars[i] == s1.chars[i],
                       "lemma 2: forall s1 s2 i j. 0 <= i < len s1 ^ 0 <= j < len s2 -> "
                       "(castrconcat s1 s2)[i] = s1[i] ^ (castrconcat s1 s2)[(len s1) + j] = s2[j] falsified"
                       " for s1: \"%s\", s2: \"%s\", s1 concat s2: \"%s\"",
                       ca_str_to_CStr(&s1), ca_str_to_CStr(&s2), ca_str_to_CStr(&tmp));
            }

            for (size_t j = 0; j < s2.len; j++) {
                assert(tmp.chars[s1.len + j] == s2.chars[j],
                       "lemma 2: forall s1 s2 i j. 0 <= i < len s1 ^ 0 <= j < len s2 -> "
                       "(castrconcat s1 s2)[i] = s1[i] ^ (castrconcat s1 s2)[(len s1) + j] = s2[j] falsified"
                       " for s1: \"%s\", s2: \"%s\", s1 concat s2: \"%s\"",
                       ca_str_to_CStr(&s1), ca_str_to_CStr(&s2), ca_str_to_CStr(&tmp));
            }
            sp -= 2;
        }


        // lemma 3: forall s1 s2. s1 = s2 -> castrcmp s1 s2 = 0
        {
            string s = ca_str_of(RAND_STRING());
            // no aliasing problem since strcmp only ever reads
            assert(castrcmp(&s, &s) == 0, "lemma 3: forall s1 s2. s1 = s2 -> castrcmp s1 s2 = 0 falsified");

            sp--;
        }


        // lemma 4: forall s1 s2. castrsubstr s1 s2 /= NULL -> castrsubstr s1 s2 = s2
        {
            string s1 = ca_str_of(RAND_STRING());
            if (sp >= STACK_SIZE) {
                puts("overflow\n");
                exit(1);
            }
            string s2 = {.chars = mem[sp]};

            bool forceSub = carandi_bound(&rng, 4) == 0;
            if (forceSub) {
                uint32_t i = carandi_bound(&rng, s1.len);
                uint32_t len = carandi_bound(&rng, s1.len - i + 1);
                s2.chars[len] = 0;
                s2.len = len;
                memcpy(s2.chars, s1.chars + i, sizeof(char) * len);
            } else {
                uint32_t len = carandi_bound(&rng, 6);
                for (uint32_t i = 0; i < len; i++)
                    s2.chars[i] = (char)((int)carandi_bound(&rng, 60) + 'A');
                s2.chars[len] = 0;
                s2.len = len;
            }

            const char *ptr = castrsubstr(&s1, &s2);


            assert(!forceSub || ptr, "lemma 4: forall s1 s2. castrsubstr s1 s2 /= NULL ->"
                                     " castrsubstr s1 s2 = s2 falsified for s1: \"%s\" s2: \"%s\"",
                   ca_str_to_CStr(&s1), ca_str_to_CStr(&s2));

            if (ptr) {
                bool success = true;
                for (size_t i = 0; success && i < s2.len; i++)
                    success = ptr[i] == s2.chars[i];

                assert(success, "lemma 4: forall s1 s2. castrsubstr s1 s2 /= NULL ->"
                                " castrsubstr s1 s2 = s2 falsified for s1: \"%s\" s2: \"%s\" s2len %zu",
                       ca_str_to_CStr(&s1), ca_str_to_CStr(&s2), s2.len);
            }

        }
    }
}



#ifndef TEST_STRING_BUFFER_CAPACITY
#define TEST_STRING_BUFFER_CAPACITY 100
#endif



static void stringBufferTest() {

#define RAND_CHAR (char)carandi_bound(&rng, 256)
    for (int _ = 0; _ < TEST_STRING_TURNS; _++) {

        uint32_t cap = carandi_bound(&rng, TEST_STRING_BUFFER_CAPACITY);
        StringBuffer buffer = ca_string_buffer_of(cap);

        uint32_t fillInit = carandi_bound(&rng, TEST_STRING_BUFFER_CAPACITY * 2);
        for (uint32_t __ = 0; __ < fillInit; __++)
            castrbufpush(&buffer, RAND_CHAR);

        // lemma 1: forall buf c. c = pop (push buf c)
        {
            char c = RAND_CHAR;
            castrbufpush(&buffer, c);
            char c1 = castrbufpop(&buffer);
            assert(c == c1, "lemma 1: forall buf c. c = pop (push buf c) falsified");
        }

        // def pop_all n b = pop_all (n - 1) (pop b)
        // def pop_all 0 b = b
        // lemma 2: forall buf s i. 0 <= i < len s -> pop (pop_all i (push_all buf s)) = s[(len s) - 1 - i]
        {
            char s[100];
            uint32_t len = carandi_bound(&rng, 100);

            if (len) {
                for (uint32_t i = 0; i < len; i++)
                    s[i] = MAX(RAND_CHAR, 1);

                s[len] = 0;

                castrbufpush_all(&buffer, &(string){.chars = s, .len = len});


                uint32_t i = carandi_bound(&rng, len);

                bool _exc = !buffer.count;

                for (uint32_t ___ = 0; !_exc && ___ < i; ___++) {
                    castrbufpop(&buffer);
                    _exc = !buffer.count;
                }

                assert(!_exc,
                       "lemma 2: forall buf s i. 0 <= i < len s ->"
                       " pop (pop_all i (push_all buf s)) = s[(len s) - 1 - i] falsified");

                char c = castrbufpop(&buffer);
                assert(c == s[len - 1 - i],
                       "lemma 2: forall buf s i. 0 <= i < len s ->"
                       " pop (pop_all i (push_all buf s)) = s[(len s) - 1 - i] falsified");
            }
        }

    }

}