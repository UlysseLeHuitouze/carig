//
// Created by Ulysse Le Huitouze on 04/01/2024.
//

#include "carand.h"
#include "caarray.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#ifndef TEST_ARRAY_TURNS
#define TEST_ARRAY_TURNS 10000
#endif

#define assert(COND, DUMP_STATE, MSG, ...) ({ \
    if (!(COND)) {                            \
        DUMP_STATE();                         \
        printf(MSG "\n--- SEED %" PRIu64 "---\n", ##__VA_ARGS__, seed); \
        fflush(stdout);                       \
        exit(1);                              \
    }                                         \
    {}                                        \
})

#define dummy()


static void unboundArrayTest();

static void ringbufferTest();

static void unboundRingbufferTest();

static CARandom rng;
static uint64_t seed;

int main() {
    carand_setseed(&rng, seed = time(NULL));

    puts("Testing UnboundArray...\n");
    unboundArrayTest();

    puts("Testing BoundRingBuffer...\n");
    ringbufferTest();

    puts("Testing UnboundRingBuffer...\n");
    unboundRingbufferTest();

    puts("Success!\n");
    return 0;
}


// forall array a. a = pop (push array a)
#define lemma1(type, array, a, comp_func, dump_state) ({ \
    type _a = (a);                                       \
    caarrpush(type, &array, _a);                         \
    type _b = caarrpop(type, &array);                    \
    assert(comp_func(_a, _b), dump_state, "lemma 1: forall array a. a = pop (push array a) falsified"); \
    {}                                                   \
})

// forall array a i. 0 <= i < count array -> (get (set array i a) i) = a
#define lemma2(type, array, a, comp_func, dump_state) ({ \
    type _a = (a);                                       \
    int _i = carandi_bound(&rng, array.count);           \
    caarrset(type, &array, _i, _a);                      \
    type _b = caarrget(type, &array, _i);                \
    assert(comp_func(_a, _b), dump_state,                \
        "lemma 2: forall array a i. 0 <= i < (count array) -> (get (set array i a) i) = a falsified" \
        " for i=%d and (count array)=%zu", _i, array.count);                                         \
    {}                                                   \
})

// forall array. (count array) > 0 -> (get array ((count array) - 1)) = pop array
#define lemma3(type, array, comp_func, dump_state) ({ \
    if (array.count) {                                \
        type _a = caarrget(type, &array, array.count - 1); \
        type _b = caarrpop(type, &array);             \
        assert(comp_func(_a, _b), dump_state,         \
            "lemma 3: forall array. (count array) > 0 -> (get array ((count array) - 1)) = pop array falsified"); \
    }                                                 \
    {}                                                \
})

// forall array a. get (push array a) (count array) = a
#define lemma4(type, array, a, comp_func, dump_state) ({ \
    type _a = (a);                                       \
    caarrpush(type, &array, _a);                         \
    type _b = caarrget(type, &array, array.count - 1);   \
    assert(comp_func(_a, _b), dump_state, "lemma 4: forall array a. get (push array a) (count array) = a falsified"); \
    {}                                                   \
})

// lemma 5: forall array arr i. 0 <= i < len arr -> get (push_all array arr) (count array + i) = arr[i]
#define lemma5(type, array, a, comp_func, dump_state) ({ \
    uint32_t _arr_len = carandi_bound(&rng, MAX(TEST_UNBOUND_ARRAY_CAPACITY - 3, 1)); \
    if (_arr_len) {                                      \
        type *_arr = malloc(sizeof(type) * _arr_len);    \
        for (uint32_t i = 0; i < _arr_len; i++) {        \
            _arr[i] = (a);                               \
        }                                                \
                                                         \
        size_t _old_count = array.count;                 \
        caarrpush_all(type, &array, _arr, _arr_len);     \
        bool _success = true;                            \
        for (uint32_t i = 0; _success && i < _arr_len; i++) {                         \
             type _b = caarrget(type, &array, _old_count + i);                        \
            _success = comp_func(_b, _arr[i]);           \
        }                                                \
        assert(_success, dump_state, "lemma 5: forall array arr i. 0 <= i < len arr ->" \
        " get (push_all array arr) (count array + i) = arr[i] falsified");            \
    }                                                    \
})

#ifndef TEST_UNBOUND_ARRAY_CAPACITY
#define TEST_UNBOUND_ARRAY_CAPACITY 100
#endif

static void unboundArrayTest() {
#define doTests(type, cmp_func, dump_state, random_func, pretest_callback) ({ \
    for (int _ = 0; _ < TEST_ARRAY_TURNS; _++) {                              \
        int cap = carandi_bound(&rng, TEST_UNBOUND_ARRAY_CAPACITY);           \
        UnboundArray array = ca_unbound_array_of(type, cap, malloc, realloc); \
        pretest_callback();                                                   \
                                                                              \
        int fillInit = carandi_bound(&rng, TEST_UNBOUND_ARRAY_CAPACITY * 2);  \
        for (int __ = 0; __ < fillInit; __++)                                 \
            caarrpush(type, &array, random_func());                           \
                                                                              \
        lemma1(type, array, random_func(), cmp_func, dump_state);             \
        lemma2(type, array, random_func(), cmp_func, dump_state);             \
        lemma3(type, array, cmp_func, dump_state);                            \
        lemma4(type, array, random_func(), cmp_func, dump_state);             \
        lemma5(type, array, random_func(), cmp_func, dump_state);             \
        ca_unbound_array_free(array, free);                                   \
    }                                                                         \
    {}                                                                        \
})

    // int type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Int type] count: %zu, capacity: %zu\n{", array.count, array.capacity); \
    for (uint32_t i = 0; i < array.count; i++) \
        printf("%d, ", caarrget(int, &array, i)); \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandi(&rng)

        doTests(int, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // float type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Float type] count: %zu, capacity: %zu\n{", array.count, array.capacity);  \
    for (uint32_t i = 0; i < array.count; i++) \
        printf("%f, ", caarrget(float, &array, i)); \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandf(&rng)

        doTests(float, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x.c == y.c && x.d == y.d && x.i == y.i)
#define DUMP_STATE() ({ \
    printf("[Struct type] count: %zu, capacity: %zu\n{", array.count, array.capacity); \
    for (uint32_t i = 0; i < array.count; i++) {                                       \
        S s = caarrget(S, &array, i);                                                  \
        printf("{c='%c', d=%f, i=%d}, ", s.c, s.d, s.i);                               \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ((S){ \
    .c = carandi_bound(&rng, 256), \
    .d = carandd(&rng),     \
    .i = carandi(&rng)      \
})

        doTests(S, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // pointer to struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x->c == y->c && x->d == y->d && x->i == y->i)
#define DUMP_STATE() ({ \
    printf("[Pointer to Struct type] count: %zu, capacity: %zu\n{", array.count, array.capacity); \
    for (uint32_t i = 0; i < array.count; i++) {                                                  \
        S* s = caarrget(S*, &array, i);                                                           \
        printf("{c='%c', d=%f, i=%d}, ", s->c, s->d, s->i);                                       \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ({ \
    if (sp >= TEST_UNBOUND_ARRAY_CAPACITY * 3) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    mem[sp].c = carandi_bound(&rng, 256);       \
    mem[sp].d = carandd(&rng);                  \
    mem[sp].i = carandi(&rng);                  \
    mem + sp++;          \
})
#define PRETEST_CLEAR() sp = 0
        S mem[TEST_UNBOUND_ARRAY_CAPACITY * 3];
        int sp;


        doTests(S*, CMP, DUMP_STATE, RANDOM_FUNC, PRETEST_CLEAR);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }
#undef doTests
}

#undef lemma5
#undef lemma4
#undef lemma3
#undef lemma2
#undef lemma1




// forall buf. (tail buf + count buf) % (capacity buf) == head buf
#define lemma1(type, buf, dump_state) assert(((buf.tail + buf.count) & buf.capacity_minus_one) == buf.head, \
    dump_state, "lemma 1: forall buf. (tail buf + count buf) %% (capacity buf) == head buf falsified")

// forall buf. 0 <= tail buf < capacity buf ^ 0 <= head buf < capacity buf ^ 0 <= count buf <= capacity buf
#define lemma2(type, buf, dump_state) assert( \
    buf.tail <= buf.capacity_minus_one && buf.head <= buf.capacity_minus_one && \
    buf.count <= buf.capacity_minus_one + 1, dump_state,                        \
    "lemma 2: forall buf. 0 <= tail buf < capacity buf ^ 0 <= head buf < capacity buf ^" \
    " 0 <= count buf <= capacity buf falsified")

// forall buf. count buf > 0 -> peek buf = poll buf
#define lemma3(type, buf, comp_func, dump_state) ({ \
    if (buf.count) {                                \
        type _a = cabuffpeek(type, &buf);           \
        type _b = cabuffpoll(type, &buf);           \
        assert(comp_func(_a, _b), dump_state, "lemma 3: forall buf. peek buf = poll buf falsified"); \
    }                                               \
    {}                                              \
})

// def offer_all buf x:xs = offer_all (offer buf x) xs
// def offer_all buf []   = buf
// forall buf arr. count buf > 0 ^ length arr < (capacity buf) - (count buf) -> poll buf = poll (offer_all buf arr)
#define lemma4(type, buf, a, comp_func, dump_state) ({ \
    if (buf.count == 0)                                \
        cabuffoffer_unsafe(type, &buf, a);             \
                                                       \
    type _a = cabuffpeek(type, &buf);                  \
    unsigned int _arr_length = carandi_bound(&rng, buf.capacity_minus_one + 1 - buf.count); \
    for (unsigned int ___ = 0; ___ < _arr_length; ___++)                                    \
        cabuffoffer_unsafe(type, &buf, a);             \
                                                       \
    type _b = cabuffpeek(type, &buf);                  \
    assert(comp_func(_a, _b), dump_state, "lemma 4: forall buf arr. count buf > 0 ^"        \
    " length arr < (capacity buf) - (count buf) -> poll buf = poll (offer_all buf arr) falsified"); \
    {}                                                 \
})

// def offer_overwrite_all buf x:xs = offer_all (offer_overwrite buf x) xs
// def offer_overwrite_all buf []   = buf
// def poll_n_times n buf = poll_n_times (n - 1) (poll buf)
// def poll_n_times 0 buf = buf
// forall buf a b arr. length arr = (capacity buf) - 1 ->
//      poll (offer_overwrite_all (offer_overwrite (offer_overwrite buf a) b) arr) = b
#define lemma5(type, buf, a, comp_func, dump_state) ({ \
    type _a = a;                                       \
    type _b = a;                                       \
    cabuffoffer_overwrite(type, &buf, _a);             \
    cabuffoffer_overwrite(type, &buf, _b);             \
                                                       \
    for (unsigned int ___ = 0; ___ < buf.capacity_minus_one; ___++) \
        cabuffoffer_overwrite(type, &buf, a);          \
                                                       \
    type _c = cabuffpeek(type, &buf);                  \
    assert(comp_func(_c, _b), dump_state, "lemma 5: forall buf a b arr. length arr = (capacity buf) - 1 ->" \
    " poll (offer_overwrite_all (offer_overwrite (offer_overwrite buf a) b) arr) = b falsified");           \
    {}                                                 \
})

// forall buf a. count buf = 0 -> poll (offer buf a) = a ^ count (poll (offer buf a)) = 0
#define lemma6(type, buf, a, comp_func, dump_state) ({ \
    cabuffrst(&buf);                                   \
    type _a = (a);                                     \
    bool _trve = cabuffoffer(type, &buf, _a);          \
    type _b = cabuffpoll(type, &buf);                  \
    assert(_trve && comp_func(_a, _b) && buf.count == 0, dump_state, \
        "lemma 6: forall buf a. count buf = 0 -> poll (offer buf a) = a ^" \
        " count (poll (offer buf a)) = 0 falsified");  \
    {}                                                 \
})


#ifndef TEST_RINGBUFFER_CAPACITY
#define TEST_RINGBUFFER_CAPACITY 100
#endif

static void ringbufferTest() {
#define doTests(type, cmp_func, dump_state, random_func, pretest_callback) ({ \
    for (int _ = 0; _ < TEST_ARRAY_TURNS; _++) {                              \
        pretest_callback();                                                   \
        int cap = carandi_bound(&rng, TEST_RINGBUFFER_CAPACITY);              \
        bool automatic = carandbool(&rng);                                    \
        BoundRingBuffer buf = automatic ?                                     \
            ca_bound_ringbuffer_static_of(type, TEST_RINGBUFFER_CAPACITY) :   \
            ca_bound_ringbuffer_dynamic_of(type, cap, malloc);                \
                                                                              \
        int fillInit = carandi_bound(&rng, automatic ? TEST_RINGBUFFER_CAPACITY : cap); \
        for (int __ = 0; __ < fillInit; __++)                                 \
            cabuffoffer_unsafe(type, &buf, random_func());                    \
                                                                              \
        lemma1(type, buf, dump_state);                                        \
        lemma2(type, buf, dump_state);                                        \
        lemma3(type, buf, cmp_func, dump_state);                              \
        lemma4(type, buf, random_func(), cmp_func, dump_state);               \
        pretest_callback();                                                   \
        lemma5(type, buf, random_func(), cmp_func, dump_state);               \
        pretest_callback();                                                   \
        lemma6(type, buf, random_func(), cmp_func, dump_state);               \
        if (!automatic)                                                       \
            ca_bound_ringbuffer_free(buf, free);                              \
    }                                                                         \
    {}                                                                        \
})

    // int type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Int type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);  \
    while (buf.count > 0)                                            \
        printf("%d, ", cabuffpoll(int, &buf));                       \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandi(&rng)

        doTests(int, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // float type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Float type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);    \
    while (buf.count > 0)                                              \
        printf("%f, ", cabuffpoll(float, &buf));                       \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandf(&rng)

        doTests(float, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x.c == y.c && x.d == y.d && x.i == y.i)
#define DUMP_STATE() ({ \
    printf("[Struct type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);     \
    while (buf.count > 0) {                                             \
        S s = cabuffpoll(S, &buf);                                      \
        printf("{c='%c', d=%f, i=%d}, ", s.c, s.d, s.i);                \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ((S){ \
    .c = carandi_bound(&rng, 256), \
    .d = carandd(&rng),     \
    .i = carandi(&rng)      \
})

        doTests(S, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }


    // pointer to struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x->c == y->c && x->d == y->d && x->i == y->i)
#define DUMP_STATE() ({ \
    printf("[Pointer to Struct type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);                \
    while (buf.count > 0) {                                                        \
        S* s = cabuffpoll(S*, &buf);                                               \
        printf("{c='%c', d=%f, i=%d}, ", s->c, s->d, s->i);                        \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ({ \
    if (sp >= TEST_RINGBUFFER_CAPACITY * 2) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    mem[sp].c = carandi_bound(&rng, 256); \
    mem[sp].d = carandd(&rng);            \
    mem[sp].i = carandi(&rng);            \
    mem + sp++;          \
})
#define PRETEST_CLEAR() sp = 0
        S mem[TEST_RINGBUFFER_CAPACITY * 2];
        int sp;


        doTests(S*, CMP, DUMP_STATE, RANDOM_FUNC, PRETEST_CLEAR);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }
#undef doTests
}

#undef lemma6
#undef lemma5
#undef lemma4
#undef lemma3
#undef lemma2
#undef lemma1




// forall buf. (tail buf + count buf) % (capacity buf) == head buf
#define lemma1(type, buf, dump_state) assert(((buf.tail + buf.count) & buf.capacity_minus_one) == buf.head, \
    dump_state, "lemma 1: forall buf. (tail buf + count buf) %% (capacity buf) == head buf falsified")

// forall buf. 0 <= tail buf < capacity buf ^ 0 <= head buf < capacity buf ^ 0 <= count buf <= capacity buf
#define lemma2(type, buf, dump_state) assert( \
    buf.tail <= buf.capacity_minus_one && buf.head <= buf.capacity_minus_one && \
    buf.count <= buf.capacity_minus_one + 1, dump_state,                        \
    "lemma 2: forall buf. 0 <= tail buf < capacity buf ^ 0 <= head buf < capacity buf ^" \
    " 0 <= count buf <= capacity buf falsified")

// forall buf. count buf > 0 -> peek buf = poll buf
#define lemma3(type, buf, comp_func, dump_state) ({ \
    if (buf.count) {                                \
        type _a = cadbuffpeek(type, &buf);          \
        type _b = cadbuffpoll(type, &buf);          \
        assert(comp_func(_a, _b), dump_state, "lemma 3: forall buf. peek buf = poll buf falsified"); \
    }                                               \
    {}                                              \
})

// def offer_all buf x:xs = offer_all (offer buf x) xs
// def offer_all buf []   = buf
// forall buf arr. count buf > 0 ^ length arr < (capacity buf) - (count buf) -> poll buf = poll (offer_all buf arr)
#define lemma4(type, buf, a, comp_func, dump_state) ({ \
    if (buf.count == 0)                                \
        cadbuffoffer_overwrite(type, &buf, a);         \
                                                       \
    type _a = cadbuffpeek(type, &buf);                 \
    unsigned int _arr_length = carandi_bound(&rng, buf.capacity_minus_one + 1 - buf.count); \
    for (unsigned int ___ = 0; ___ < _arr_length; ___++)                                    \
        cadbuffoffer_overwrite(type, &buf, a);         \
                                                       \
    type _b = cadbuffpeek(type, &buf);                 \
    assert(comp_func(_a, _b), dump_state, "lemma 4: forall buf arr. count buf > 0 ^"        \
    " length arr < (capacity buf) - (count buf) -> poll buf = poll (offer_all buf arr) falsified"); \
    {}                                                 \
})

// def offer_overwrite_all buf x:xs = offer_all (offer_overwrite buf x) xs
// def offer_overwrite_all buf []   = buf
// def poll_n_times n buf = poll_n_times (n - 1) (poll buf)
// def poll_n_times 0 buf = buf
// forall buf a b arr. length arr = (capacity buf) - 1 ->
//      poll (offer_overwrite_all (offer_overwrite (offer_overwrite buf a) b) arr) = b
#define lemma5(type, buf, a, comp_func, dump_state) ({ \
    type _a = a;                                       \
    type _b = a;                                       \
    cadbuffoffer_overwrite(type, &buf, _a);            \
    cadbuffoffer_overwrite(type, &buf, _b);            \
                                                       \
    for (unsigned int ___ = 0; ___ < buf.capacity_minus_one; ___++) \
        cadbuffoffer_overwrite(type, &buf, a);         \
                                                       \
    type _c = cadbuffpeek(type, &buf);                 \
    assert(comp_func(_c, _b), dump_state, "lemma 5: forall buf a b arr. length arr = (capacity buf) - 1 ->" \
    " poll (offer_overwrite_all (offer_overwrite (offer_overwrite buf a) b) arr) = b falsified");           \
    {}                                                 \
})

// forall buf a. count buf = 0 -> poll (offer buf a) = a ^ count (poll (offer buf a)) = 0
#define lemma6(type, buf, a, comp_func, dump_state) ({ \
    cadbuffrst(&buf);                                  \
    type _a = (a);                                     \
    cadbuffoffer(type, &buf, _a);                      \
    type _b = cadbuffpoll(type, &buf);                 \
    assert(comp_func(_a, _b) && buf.count == 0, dump_state, \
        "lemma 6: forall buf a. count buf = 0 -> poll (offer buf a) = a ^" \
        " count (poll (offer buf a)) = 0 falsified");  \
    {}                                                 \
})

// def offer_all buf x:xs = offer_all (offer buf x) xs
// def offer_all buf []   = buf
// def poll_n_times n buf = poll_n_times (n - 1) (poll buf)
// def poll_n_times 0 buf = buf
// forall buf arr i j. 0 <= i < j < length arr ->
//      exists x y. y - x = j - i ^
//      poll (poll_n_times x (offer_all buf arr)) = arr[i] ^ poll (poll_n_times y (offer_all buf arr)) = arr[j]
#define lemma7(type, buf, a, comp_func, dump_state) ({ \
    int _arr_len = carandi_bound(&rng, TEST_RINGBUFFER_CAPACITY * 10); \
    int _j = carandi_bound(&rng, _arr_len);            \
    int _i = carandi_bound(&rng, _j);                  \
    if (_j) {                                          \
        type _a;                                       \
        type _b;                                       \
        for (int ___ = 0; ___ < _arr_len; ___++) {     \
            type _tmp = (a);                           \
            if (___ == _i)                             \
                _a = _tmp;                             \
            else if (___ == _j)                        \
                _b = _tmp;                             \
            cadbuffoffer(type, &buf, _tmp);            \
        }                                              \
                                                       \
        uint32_t _tail_backup;                         \
        int _diff = _j - _i;                           \
        assert(_diff >= 0, dump_state, "internal error"); \
        bool _exc = false;                             \
        bool _success = false;                         \
        while (buf.count && !_success && !_exc) {      \
                                                       \
            type _tmp;                                 \
            do {                                       \
                _tmp = cadbuffpoll(type, &buf);        \
                _exc = !buf.count;                     \
            } while (!_exc && !comp_func(_tmp, _a));   \
            _tail_backup = buf.tail;                   \
                                                       \
            for (int ___ = 1; !_exc && ___ < _diff; ___++) { \
                _exc = !buf.count;                     \
                cadbuffpoll(type, &buf);               \
            }                                          \
                                                       \
            if (buf.count)                             \
                _tmp = cadbuffpoll(type, &buf);        \
            else                                       \
                _exc = true;                           \
                                                       \
            if (!_exc && comp_func(_tmp, _b))          \
                _success = true;                       \
            else {                                     \
                                                       \
                buf.count += buf.tail - _tail_backup;  \
                buf.tail = _tail_backup;               \
            }                                          \
        }                                              \
                                                       \
        assert(!_exc && _success, dump_state,          \
            "lemma 7: forall buf arr i j. 0 <= i < j < length arr ->"  \
            " exists x y. y - x = j - i ^ poll (poll_n_times x (offer_all buf arr))" \
            " = arr[i] ^ poll (poll_n_times y (offer_all buf arr)) = arr[j]");       \
    }                                                  \
    {}                                                 \
})

#ifndef TEST_UNBOUND_RINGBUFFER_CAPACITY
#define TEST_UNBOUND_RINGBUFFER_CAPACITY 100
#endif

static void unboundRingbufferTest() {
#define doTests(type, cmp_func, dump_state, random_func, pretest_callback) ({ \
    for (int _ = 0; _ < TEST_ARRAY_TURNS; _++) {                              \
        pretest_callback();                                                   \
        int cap = carandi_bound(&rng, TEST_UNBOUND_RINGBUFFER_CAPACITY);      \
        UnboundRingBuffer buf = ca_unbound_ringbuffer_of(type, cap, malloc, realloc); \
                                                                              \
        int fillInit = carandi_bound(&rng, TEST_UNBOUND_RINGBUFFER_CAPACITY * 2);     \
        for (int __ = 0; __ < fillInit; __++)                                 \
            cadbuffoffer(type, &buf, random_func());                          \
                                                                              \
        lemma1(type, buf, dump_state);                                        \
        lemma2(type, buf, dump_state);                                        \
        lemma3(type, buf, cmp_func, dump_state);                              \
        lemma4(type, buf, random_func(), cmp_func, dump_state);               \
        pretest_callback();                                                   \
        lemma5(type, buf, random_func(), cmp_func, dump_state);               \
        pretest_callback();                                                   \
        lemma7(type, buf, random_func(), cmp_func, dump_state);               \
        pretest_callback();                                                   \
        lemma6(type, buf, random_func(), cmp_func, dump_state);               \
        ca_unbound_ringbuffer_free(buf, free);                                \
    }                                                                         \
    {}                                                                        \
})

    // int type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Int type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);  \
    while (buf.count > 0)                                            \
        printf("%d, ", cabuffpoll(int, &buf));                       \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandi(&rng)

        doTests(int, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // float type
    {
#define CMP(x, y) (x == y)
#define DUMP_STATE() ({ \
    printf("[Float type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);    \
    while (buf.count > 0)                                              \
        printf("%f, ", cabuffpoll(float, &buf));                       \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandf(&rng)

        doTests(float, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }

    // struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x.c == y.c && x.d == y.d && x.i == y.i)
#define DUMP_STATE() ({ \
    printf("[Struct type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);     \
    while (buf.count > 0) {                                             \
        S s = cabuffpoll(S, &buf);                                      \
        printf("{c='%c', d=%f, i=%d}, ", s.c, s.d, s.i);                \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ((S){ \
    .c = carandi_bound(&rng, 256), \
    .d = carandd(&rng),     \
    .i = carandi(&rng)      \
})

        doTests(S, CMP, DUMP_STATE, RANDOM_FUNC, dummy);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }


    // pointer to struct type
    {
        typedef struct S {
            char c;
            double d;
            int i;
        } S;
#define CMP(x, y) (x->c == y->c && x->d == y->d && x->i == y->i)
#define DUMP_STATE() ({ \
    printf("[Pointer to Struct type] capacity: %d count: %d head: %d tail: %d\n{", \
        buf.capacity_minus_one + 1, buf.count, buf.head, buf.tail);                \
    while (buf.count > 0) {                                                        \
        S* s = cabuffpoll(S*, &buf);                                               \
        printf("{c='%c', d=%f, i=%d}, ", s->c, s->d, s->i);                        \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ({ \
    if (sp >= TEST_RINGBUFFER_CAPACITY * 10) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    mem[sp].c = carandi_bound(&rng, 256); \
    mem[sp].d = carandd(&rng);            \
    mem[sp].i = carandi(&rng);            \
    mem + sp++;          \
})
#define PRETEST_CLEAR() sp = 0
        S mem[TEST_RINGBUFFER_CAPACITY * 10];
        int sp;


        doTests(S*, CMP, DUMP_STATE, RANDOM_FUNC, PRETEST_CLEAR);

#undef RANDOM_FUNC
#undef DUMP_STATE
#undef CMP
    }
#undef doTests
}

#undef lemma7
#undef lemma6
#undef lemma5
#undef lemma4
#undef lemma3
#undef lemma2
#undef lemma1