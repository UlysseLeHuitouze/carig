//
// Created by Ulysse Le Huitouze on 04/01/2024.
//

#include "carand.h"
#include "camap.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#ifndef TEST_MAP_TURNS
#define TEST_MAP_TURNS 10000
#endif

#define assert(COND, DUMP_STATE, MSG, ...) ({ \
    if (!(COND)) {                            \
        DUMP_STATE();                         \
        printf(MSG "\n--- SEED %" PRIu64 "---\n", ##__VA_ARGS__, seed); \
        fflush(stdout);                       \
        exit(1);                              \
    }                                         \
    {}                                        \
})

#define dummy()

typedef struct S {
    char c;
    double d;
    int i;
} S;

static void setTest();

static void mapTest();

static CARandom rng;
static uint64_t seed;

int main() {
    carand_setseed(&rng, seed = time(NULL));


    setTest();

    mapTest();


    puts("Success!\n");
    return 0;
}


// forall set a. has (put set a) a
#define lemma1(type, has_func, put_func, a, dump_state) ({ \
    type _a = (a);                                         \
    put_func(_a);                                          \
    bool _has = has_func(_a);                              \
    assert(_has, dump_state, "lemma1: forall set a. has (put set a) a falsified"); \
    {}                                                     \
})

// forall set a b. ¬(a = b) -> has set a = has (put set b) a
#define lemma2(type, has_func, put_func, a, comp_func, dump_state) ({ \
    type _a = (a);                                                    \
    type _b = (a);                                                    \
    if (!comp_func(_a, _b)) {                                         \
        bool _has = has_func(_a);                                     \
        put_func(_b);                                                 \
        bool _has2 = has_func(_a);                                    \
        assert(_has == _has2, dump_state,                             \
            "lemma 2: forall set a b. ¬(a = b) -> has set a = has (put set b) a"); \
    }                                                                 \
    {}                                                                \
})

// forall set a. ¬has (rmv set a) a
#define lemma3(type, has_func, rmv_func, a, dump_state) ({ \
    type _a = (a);                                         \
    rmv_func(_a);                                          \
    bool _has = has_func(_a);                              \
    assert(!_has, dump_state, "lemma 3: forall set a. ¬has (rmv set a) a falsified");\
    {}                                                       \
})

// forall set a. ¬has (rst_func set) a
#define lemma4(type, has_func, rst_func, a, dump_state) ({ \
    type _a = (a);                                         \
    rst_func();                                            \
    bool _has = has_func(_a);                              \
    assert(!_has, dump_state, "lemma 4: forall set a. ¬has (rst_func set) a falsified");\
})

#ifndef TEST_SET_CAPACITY
#define TEST_SET_CAPACITY 100
#endif

static int Scmp(S *x, S *y) {
    return x->c - y->c + (int)(x->d - y->d) + x->i - y->i;
}


static void setTest() {
#define doTests(type, has_func, put_func, rmv_func, rst_func, \
        random_func, comp_func, dump_state, set_create, set_free) ({ \
    for (int _ = 0; _ < TEST_MAP_TURNS; _++) {                \
        set_create();                                         \
        lemma1(type, has_func, put_func, random_func(), dump_state); \
        lemma2(type, has_func, put_func, random_func(), comp_func, dump_state); \
        lemma3(type, has_func, rmv_func, random_func(), dump_state); \
        lemma4(type, has_func, rst_func, random_func(), dump_state); \
        set_free();                                           \
    }                                                         \
})



    puts("Testing BoundHashSet...\n");
    // hash sets
    {
        S mem[TEST_SET_CAPACITY];
        int sp;

#define SET_CREATE() \
    sp = 0;          \
    bool automatic = carandbool(&rng); \
    uint32_t cap = carandi_bound(&rng, TEST_SET_CAPACITY - 5) + 5; \
    BoundHashSet set = automatic ?     \
            ca_bound_hashset_static_of(TEST_SET_CAPACITY, (int (*)(const void *, const void *))Scmp) : \
            ca_bound_hashset_dynamic_of(cap, (int (*)(const void *, const void *))Scmp);               \
                     \
    uint32_t fillInit = carandi_bound(&rng, automatic ? TEST_SET_CAPACITY - 5 : cap - 5);              \
    for (uint32_t __ = 0; __ < fillInit; __++) {                   \
        __auto_type _wxc = RANDOM_FUNC();                          \
        PUT_FUNC(_wxc);                \
    }


#define SET_FREE() if (!automatic) ca_bound_hashset_free(&set)
#define HASH(x) ((uint32_t)(x->c + x->d * x->i))
#define PUT_FUNC(x) cahsetput(&set, HASH(x), x)
#define HAS_FUNC(x) cahsethas(&set, HASH(x), x)
#define RMV_FUNC(x) cahsetrmv(&set, HASH(x), x)
#define RST_FUNC(x) cahsetrst(&set)
#define DUMP_STATE() ({ \
    printf("[Struct type]\n{"); \
    for (size_t j = 0; j < set.capacity; j++) {                         \
        S* s = set.elements[j];                                         \
        if (s)          \
            printf("{c='%c', d=%f, i=%d}, ", s->c, s->d, s->i);         \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() ({ \
    if (sp >= TEST_SET_CAPACITY) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    mem[sp].c = carandi_bound(&rng, 256); \
    mem[sp].d = carandd(&rng);            \
    mem[sp].i = carandi(&rng);            \
    mem + sp++;          \
})
#define CMP(x, y) (x->c == y->c && x->d == y->d && x->i == y->i)


        doTests(S*, HAS_FUNC, PUT_FUNC, RMV_FUNC, RST_FUNC, RANDOM_FUNC, CMP, DUMP_STATE, SET_CREATE, SET_FREE);


#undef CMP
#undef RANDOM_FUNC
#undef DUMP_STATE
#undef RST_FUNC
#undef RMV_FUNC
#undef HAS_FUNC
#undef PUT_FUNC
#undef HASH
#undef SET_FREE
#undef SET_CREATE
    }

    puts("Testing BoundIntSet...\n");
    // int sets
    {
#define SET_CREATE() \
    bool automatic = carandbool(&rng); \
    uint32_t cap = carandi_bound(&rng, TEST_SET_CAPACITY - 4) + 4; \
    BoundIntSet set = automatic ?     \
            ca_bound_intset_static_of(TEST_SET_CAPACITY) :  \
            ca_bound_intset_dynamic_of(cap);        \
                     \
    uint32_t fillInit = carandi_bound(&rng, automatic ? TEST_SET_CAPACITY - 4 : cap - 4); \
    for (uint32_t __ = 0; __ < fillInit; __++) {                   \
        __auto_type _wxc = RANDOM_FUNC();                          \
        PUT_FUNC(_wxc);                \
    }
#define SET_FREE() if (!automatic) ca_bound_intset_free(&set)
#define PUT_FUNC(x) caisetput(&set, x)
#define HAS_FUNC(x) caisethas(&set, x)
#define RMV_FUNC(x) caisetrmv(&set, x)
#define RST_FUNC(x) caisetrst(&set)
#define DUMP_STATE() ({ \
    printf("[int type]\n{"); \
    for (size_t j = 0; j < set.capacity; j++) {                         \
        uint32_t i = set.elements[j];                                         \
        if (i)          \
            printf("%d, ", i + 1);         \
    }                   \
    puts("}\n");        \
    {}                  \
})
#define RANDOM_FUNC() carandi_bound(&rng, UINT32_MAX)
#define CMP(x, y) (x == y)

        doTests(uint32_t, HAS_FUNC, PUT_FUNC, RMV_FUNC, RST_FUNC, RANDOM_FUNC, CMP, DUMP_STATE, SET_CREATE, SET_FREE);

#undef CMP
#undef RANDOM_FUNC
#undef DUMP_STATE
#undef RST_FUNC
#undef RMV_FUNC
#undef HAS_FUNC
#undef PUT_FUNC
#undef SET_FREE
#undef SET_CREATE
    }
#undef doTests
}
#undef lemma1
#undef lemma2
#undef lemma3
#undef lemma4





static bool strEquals(const char *s1, const char *s2) {
    while (*s1 && *s2 && *s1 == *s2) {
        s1++;
        s2++;
    }
    return !*s1 && !*s2;
}

static uint32_t strHash(const char *s) {
    uint32_t h = 0;
    while (*s)
        h = 31 * h + *s++;
    return h;
}


#ifndef TEST_MAP_CAPACITY
#define TEST_MAP_CAPACITY 100
#endif

// forall map k v. get (put map k v) k = v
#define lemma1(typek, typev, get_func, put_func, k, v, cmp_values, dump_state) ({ \
    typek _k = (k);                                                                       \
    typev _v = (v);                                                                     \
                                                                                                 \
    put_func(_k, _v);                                                                       \
    typev _v1 = get_func(_k);                                                                       \
    assert(cmp_values(_v, _v1), dump_state, "lemma 1: forall map k v. get (put map k v) k = v falsified");\
    {}                                                                              \
})


// forall map k k1 v1. ¬(k = k1) -> get map k = get (put map k1 v1) k
#define lemma2(typek, typev, get_func, put_func, k, v, cmp_keys, cmp_values, dump_state) ({ \
    typek _k = (k);                                                                         \
    typek _k1 = (k);                                                                        \
    typev _v1 = (v);                                                                        \
    if (!cmp_keys(_k, _k1)) {                                                               \
        typev _tmp = get_func(_k);                                                          \
        put_func(_k1, _v1);                                                                 \
        typev _tmp2 = get_func(_k);                                                         \
        assert(cmp_values(_tmp, _tmp2), dump_state,                                         \
            "lemma 2: forall map k k1 v1. ¬(k = k1) -> get map k = get (put map k1 v1) k falsified"); \
    }                                                                                       \
})


// forall map k. ¬has (rmv map k) k
#define lemma3(typek, has_func, rmv_func, k, dump_state) ({ \
    typek _k = (k);                                         \
    rmv_func(_k);                                           \
    bool _b = has_func(_k);                                 \
    assert(!_b, dump_state, "lemma 3: forall map k. ¬has (rmv map k) k falsified");\
    {}                                                      \
})


// forall map k. ¬has (rst map) k
#define lemma4(typek, has_func, rst_func, k, dump_state) ({ \
    rst_func();                                             \
    typek _k = (k);                                         \
    bool _b = has_func(_k);                                 \
    assert(!_b, dump_state, "lemma 4: forall map k. ¬has (rst map) k falsified");\
    {}                                                      \
})


// forall map k v. get map k = rmv map k = put map k v
#define lemma5(typek, typev, get_func, rmv_func, put_func, k, v, cmp_values, dump_state) ({ \
    typek _k = (k);                                                                         \
    typev _v = (v);                                                                         \
    typev _v1 = get_func(_k);                                                               \
    typev _v2 = rmv_func(_k);                                                               \
    put_func(_k, _v2);                                                                      \
    typev _v3 = put_func(_k, _v);                                                           \
    bool _b = (cmp_values(_v1, _v2)) && (cmp_values(_v1, _v3));                             \
    assert(_b, dump_state, "lemma 5: forall map k v. get map k = rmv map k = put map k v falsified"); \
    {}                                                                                      \
})


static void mapTest() {
#define doTests(typek, typev, rand_keys, rand_values, cmp_keys, cmp_values, dump_state, \
get_func, put_func, has_func, rmv_func, rst_func, map_create, map_free) ({              \
    for (int _ = 0; _ < TEST_MAP_TURNS; _++) {                                          \
        map_create();                                                                   \
        lemma1(typek, typev, get_func, put_func, rand_keys(), rand_values(), cmp_values, dump_state); \
        lemma2(typek, typev, get_func, put_func, rand_keys(), rand_values(), cmp_keys, cmp_values, dump_state); \
        lemma3(typek, has_func, rmv_func, rand_keys(), dump_state);                     \
        lemma4(typek, has_func, rst_func, rand_keys(), dump_state);                     \
        lemma5(typek, typev, get_func, rmv_func, put_func, rand_keys(), rand_values(), cmp_values, dump_state); \
        map_free();                                                                     \
    }                                                                                   \
})

    puts("Testing BoundHashMap...\n");
    // hash map char* to S*
    {

        S memv[TEST_MAP_CAPACITY];
        char memk[TEST_MAP_CAPACITY][50];
        int spv, spk;

#define MAP_CREATE() \
    spv = spk = 0;                 \
    bool automatic = carandbool(&rng); \
    uint32_t cap = carandi_bound(&rng, TEST_MAP_CAPACITY - 6) + 6;                    \
    BoundHashMap map = automatic ?     \
        ca_bound_hashmap_static_of(TEST_MAP_CAPACITY, Scmp) :                 \
        ca_bound_hashmap_dynamic_of(cap, (int (*)(const void*, const void*))Scmp);                       \
                     \
    uint32_t fillInit = carandi_bound(&rng, automatic ? TEST_MAP_CAPACITY - 6 : cap - 6); \
    for (uint32_t __ = 0; __ < fillInit; __++) { \
        __auto_type k = RAND_KEYS();                                      \
        __auto_type v = RAND_VALUES();                                    \
        PUT_FUNC(k, v);\
    }


#define MAP_FREE() if (!automatic) ca_bound_hashmap_free(&map)
#define PUT_FUNC(k, v) cahmapput(&map, strHash(k), k, v)
#define GET_FUNC(k) cahmapget(&map, strHash(k), k)
#define HAS_FUNC(k) ((bool)GET_FUNC(k))
#define RMV_FUNC(k) cahmaprmv(&map, strHash(k), k)
#define RST_FUNC() cahmaprst(&map)
#define DUMP_STATE() dummy()
#define CMP(x, y) ((!x && !y) || (x->c == y->c && x->d == y->d && x->i == y->i))
#define RAND_KEYS() ({ \
    if (spk >= TEST_MAP_CAPACITY) { \
        puts("overflow");           \
        exit(1);       \
    }                  \
                       \
    int _strlen = (int)carandi_bound(&rng, 50); \
    for (int i = 0; i < _strlen; i++)      \
        memk[spk][i] = (char)((int)carandi_bound(&rng, 60) + 'A');\
    memk[spk][_strlen] = 0;                   \
    memk[spk++];\
})
#define RAND_VALUES() ({ \
    if (spv >= TEST_SET_CAPACITY) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    memv[spv].c = carandi_bound(&rng, 256); \
    memv[spv].d = carandd(&rng);            \
    memv[spv].i = carandi(&rng);            \
    memv + spv++;          \
})

        doTests(char*, S*, RAND_KEYS, RAND_VALUES, strEquals, CMP, DUMP_STATE,
                GET_FUNC, PUT_FUNC, HAS_FUNC, RMV_FUNC, RST_FUNC, MAP_CREATE, MAP_FREE);

#undef RAND_VALUES
#undef RAND_KEYS
#undef CMP
#undef DUMP_STATE
#undef RST_FUNC
#undef RMV_FUNC
#undef HAS_FUNC
#undef GET_FUNC
#undef PUT_FUNC
#undef MAP_FREE
#undef MAP_CREATE
    }


    puts("Testing BoundIntMap...\n");
    // int map uint32_t to S*
    {
        S mem[TEST_MAP_CAPACITY];
        int sp;

#define MAP_CREATE() \
    sp = 0;                 \
    bool automatic = carandbool(&rng); \
    uint32_t cap = carandi_bound(&rng, TEST_MAP_CAPACITY - 6) + 6;                    \
    BoundIntMap map = automatic ?     \
        ca_bound_intmap_static_of(TEST_MAP_CAPACITY) :                 \
        ca_bound_intmap_dynamic_of(cap);                       \
                     \
    uint32_t fillInit = carandi_bound(&rng, automatic ? TEST_MAP_CAPACITY - 6 : cap - 6); \
    for (uint32_t __ = 0; __ < fillInit; __++) { \
        __auto_type k = RAND_KEYS();                                      \
        __auto_type v = RAND_VALUES();                                    \
        PUT_FUNC(k, v);\
    }


#define MAP_FREE() if (!automatic) ca_bound_intmap_free(&map)
#define PUT_FUNC(k, v) caimapput(&map, k, v)
#define GET_FUNC(k) caimapget(&map, k)
#define HAS_FUNC(k) ((bool)GET_FUNC(k))
#define RMV_FUNC(k) caimaprmv(&map, k)
#define RST_FUNC() caimaprst(&map)
#define DUMP_STATE() dummy()
#define CMP_KEYS(x, y) (x == y)
#define CMP_VALUES(x, y) ((!x && !y) || (x->c == y->c && x->d == y->d && x->i == y->i))
#define RAND_KEYS() carandi_bound(&rng, UINT32_MAX)
#define RAND_VALUES() ({ \
    if (sp >= TEST_SET_CAPACITY) { \
        puts("overflow");\
        exit(1);         \
    }                    \
    mem[sp].c = carandi_bound(&rng, 256); \
    mem[sp].d = carandd(&rng);            \
    mem[sp].i = carandi(&rng);            \
    mem + sp++;          \
})

        doTests(uint32_t, S*, RAND_KEYS, RAND_VALUES, CMP_KEYS, CMP_VALUES, DUMP_STATE,
                GET_FUNC, PUT_FUNC, HAS_FUNC, RMV_FUNC, RST_FUNC, MAP_CREATE, MAP_FREE);

#undef RAND_VALUES
#undef RAND_KEYS
#undef CMP_VALUES
#undef CMP_KEYS
#undef DUMP_STATE
#undef RST_FUNC
#undef RMV_FUNC
#undef HAS_FUNC
#undef GET_FUNC
#undef PUT_FUNC
#undef MAP_FREE
#undef MAP_CREATE
    }



    puts("Testing BoundIntIntMap...\n");
    // int int map uint32_t to uint32_t
    {

#define MAP_CREATE() \
    bool automatic = carandbool(&rng); \
    uint32_t cap = carandi_bound(&rng, TEST_MAP_CAPACITY - 6) + 6;                    \
    BoundIntIntMap map = automatic ?     \
        ca_bound_intintmap_static_of(TEST_MAP_CAPACITY) :                 \
        ca_bound_intintmap_dynamic_of(cap);                       \
                     \
    uint32_t fillInit = carandi_bound(&rng, automatic ? TEST_MAP_CAPACITY - 6 : cap - 6); \
    for (uint32_t __ = 0; __ < fillInit; __++) { \
        __auto_type k = RANDOM_FUNC();                                      \
        __auto_type v = RANDOM_FUNC();                                    \
        PUT_FUNC(k, v);\
    }


#define MAP_FREE() if (!automatic) ca_bound_intintmap_free(&map)
#define PUT_FUNC(k, v) caiimapput(&map, k, v)
#define GET_FUNC(k) caiimapget(&map, k)
#define HAS_FUNC(k) (GET_FUNC(k) != UINT32_MAX)
#define RMV_FUNC(k) caiimaprmv(&map, k)
#define RST_FUNC() caiimaprst(&map)
#define DUMP_STATE() dummy()
#define CMP(x, y) (x == y)
#define RANDOM_FUNC() carandi_bound(&rng, UINT32_MAX)

        doTests(uint32_t, uint32_t, RANDOM_FUNC, RANDOM_FUNC, CMP, CMP, DUMP_STATE,
                GET_FUNC, PUT_FUNC, HAS_FUNC, RMV_FUNC, RST_FUNC, MAP_CREATE, MAP_FREE);

#undef RANDOM_FUNC
#undef CMP
#undef DUMP_STATE
#undef RST_FUNC
#undef RMV_FUNC
#undef HAS_FUNC
#undef GET_FUNC
#undef PUT_FUNC
#undef MAP_FREE
#undef MAP_CREATE
    }
}
